///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** @enum */
var SpriteEditMouseAction = {
	none: -1,
	default: 0,
	createSprite: 1,
	panCamera: 2,
	selectSprite: 3,
	dragSprite: 4,
	spriteResize_E: 5,
	spriteResize_S: 6,
	spriteResize_W: 7,
	spriteResize_N: 8,
	spriteResize_SE: 9,
	spriteResize_SW: 10,
	spriteResize_NW: 11,
	spriteResize_NE: 12
}

class SpriteSheetEditor{

	constructor(){

		/** @type {HTMLSelectElement} */
		this.control_sheetSelect = null;
		/** @type {HTMLSelectElement} */
		this.control_spriteSelect = null;
		
		/** @type {HTMLInputElement} */
		this.control_referencePath = null;
		/** @type {HTMLInputElement} */
		this.control_assetPackName = null;
		/** @type {HTMLInputElement} */
		this.control_importSheets = null;
		/** @type {HTMLInputElement} */
		this.control_addSheet = null;
		/** @type {HTMLInputElement} */
		this.control_removeSheet = null;
		/** @type {HTMLInputElement} */
		this.control_sheetKey = null;
		/** @type {HTMLInputElement} */
		this.control_sheetImagePath = null;
		/** @type {HTMLInputElement} */
		this.control_sheetApply = null;

		/** @type {HTMLInputElement} */
		this.control_spriteAutoGenerate = null;
		/** @type {HTMLInputElement} */
		this.control_addSprite = null;
		/** @type {HTMLInputElement} */
		this.control_removeSprite = null;
		/** @type {HTMLInputElement} */
		this.control_spriteKey = null;
		/** @type {HTMLInputElement} */
		this.control_spritePosition = null;
		/** @type {HTMLInputElement} */
		this.control_spriteSize = null;
		/** @type {HTMLInputElement} */
		this.control_spritePivot = null;
		/** @type {HTMLInputElement} */
		this.control_centerSpritePivot = null;
		/** @type {HTMLInputElement} */
		this.control_spriteApply = null;

		/** @type {HTMLInputElement} */
		this.control_export = null;
		/** @type {HTMLInputElement} */
		this.control_generateOutput = null;
		/** @type {HTMLTextAreaElement} */
		this.control_outputText = null;

		/** @type {HTMLCanvasElement} */
		this.canvas = null;
		/** @type {CanvasRenderingContext2D} */
		this.context = null;

		/** @type {Vec2} */
		this._cumMouseDelta = new Vec2();

		/** @type {Vec2} */
		this._canvasDragBegin = null;
		this._canvasMouseAction = -1;

		/** @type {Color} */
		this._spriteRectColor0 = new Color(150, 75, 150, 0.65);
		/** @type {Color} */
		this._spriteRectColor1 = new Color(255, 0, 255);
		/** @type {Color} */
		this._pivotColor0 =  new Color(150, 150, 75, 0.65);
		/** @type {Color} */
		this._pivotColor1 =  new Color(255, 255, 0);

		/** @type {Array<Vec2>} */
		this._resizePoints0 = null;
		/** @type {Array<Vec2>} */
		this._resizePoints1 = null;
		/** @type {Vec2} */
		this._resizeAxisMultiplier = null;

		/** @type {Boolean} */
		this._outTextSelected = false;

		/** @type {number} */
		this._sheetsCreated = 0;

		/** @type {Camera} */
		this._camera = null;
	}

	/** @type {Array<SpriteSheet>} */
	get allSpriteSheets(){

		// create a return array to populate with sprite sheets
		let r = [];

		// iterate through each sheet option
		let sel = this.control_sheetSelect.options;
		for(let i = 0; i < sel.length; i++){

			// add the spritesheet to the return array
			r.push(sel[i].sheet);
		}

		// return the return array
		return r;
	}

	/** @type {Array<SpriteSheet>} */
	get selectedSpriteSheets(){

		// create a return array to populate with sprite sheets
		let r = [];

		// iterate through each selection
		let sel = this.control_sheetSelect.selectedOptions;
		for(let i = 0; i < sel.length; i++){

			// add the spritesheet to the return array
			r.push(sel[i].sheet);
		}

		// return the return array
		return r;
	}

	/** @type {Array<Sprite>} */
	get selectedSprites(){

		let r = [];

		// iterate through each selected sprite option
		let ops = this.control_spriteSelect.selectedOptions;
		for(let i = 0; i < ops.length; i++){

			// add the option's sprite to the return array
			r.push(ops[i].sprite);
		}

		return r;
	}

	/** @type {Array<Number>} */
	get selectedSpriteIndices(){

		// find the indices of the already selected sprites
		let sel = [];
		let ops = this.control_spriteSelect.options;

		// iterate through each option in the sprite selection and add each selected index to sel
		for(let i = ops.length - 1; i >= 0; i--){
			if(ops[i].selected)
				sel.push(i);
		}

		return sel;
	}

	/** @type {String} */
	get referencePath(){
		return this.control_referencePath.value;
	}

	/**
	 * Finds all the elements based on their id strings specified in the html document
	 */
	FindAllElements(){
		
		this.canvas = document.getElementById("editor_canvas");
		this.canvas.style.cursor = "normal";

		this.control_sheetSelect = document.getElementById("editor_sheetSelect");
		this.control_spriteSelect = document.getElementById("editor_spriteSelect");

		this.control_referencePath = document.getElementById("editor_refFilePathText");
		this.control_referencePath.value = "../index.html";

		this.control_assetPackName = document.getElementById("editor_assetPackNameText");
		this.control_addSheet = document.getElementById("editor_addSheet");
		this.control_removeSheet = document.getElementById("editor_removeSheet");

		this.control_sheetKey = document.getElementById("editor_spriteSheetKeyText");
		this.control_sheetImagePath = document.getElementById("editor_filePathText");
		this.control_sheetApply = document.getElementById("editor_applySheet");
		
		this.control_spriteAutoGenerate = document.getElementById("editor_generateSprites");
		this.control_addSprite = document.getElementById("editor_addSprite");
		this.control_removeSprite = document.getElementById("editor_removeSprite");
		
		this.control_spriteKey = document.getElementById("editor_spriteKeyText");
		this.control_spritePosition = document.getElementById("editor_spritePosText");
		this.control_spriteSize = document.getElementById("editor_spriteSizeText");
		this.control_spritePivot = document.getElementById("editor_spritePivotText");
		this.control_centerSpritePivot = document.getElementById("editor_centerSpritePivot");
		this.control_spriteApply = document.getElementById("editor_applySprite");
		
		this.control_importSheets = document.getElementById("editor_importFile");
		this.control_export = document.getElementById("editor_exportFile");
		this.control_generateOutput = document.getElementById("editor_generateOutput");
		this.control_outputText = document.getElementById("editor_output");

		this._updateSheetControlsEnabled();
		this._updateSpriteControlsEnabled();
	}

	/** Add listeners to all the UI events */
	AddEventListeners(){

		let ths = this;
		
		// prevent the context menu from popping up on right click
		this.canvas.addEventListener('contextmenu', e => e.preventDefault());

		window.addEventListener('keyup', function(e){

			// delete key pressed
			if(e.keyCode == 46){

				// if sprite sheet selection is in focus, delete selected sprite sheets
				if(document.activeElement == ths.control_sheetSelect){
					ths._removeSpriteSheetPressed();
				}

				// if the sprite selection or doc body is in focus, delete the selected sprites
				else if(
					document.activeElement == ths.control_spriteSelect || 
					document.activeElement == document.body){
					ths._removeSpritePressed();
				}
			}
		});

		// add event listeners to hangle mouse drag functionality
		this.canvas.addEventListener('mousedown', function(e){
			ths._onCanvasMouseDown(e);
		});
		this.canvas.addEventListener('mouseup', function(e){
			ths._onCanvasMouseUp(e);
		});
		this.canvas.addEventListener('mouseleave', function(e){
			ths._handleDragEnd(e);
		});
		this.canvas.addEventListener('mousemove', function(e){
			ths._onCanvasMouseMove(e);
		});
		this.canvas.addEventListener('wheel', function(e){
			ths._onCanvasScroll(e);
		});

		this.control_importSheets.addEventListener('click', function(e){
			ths._importPressed();
		});

		this.control_sheetSelect.addEventListener('change', function(e){
			ths._onSheetSelectChange();
		});
		this.control_spriteSelect.addEventListener('change', function(e){
			ths._onSpriteSelectChange();
		});

		this.control_addSheet.addEventListener('click', function(e){
			ths._addSpriteSheetPressed();
		});
		this.control_removeSheet.addEventListener('click', function(e){
			ths._removeSpriteSheetPressed();
		});

		this.control_centerSpritePivot.addEventListener('click', function(e){
			ths._centerSpritePivotPressed();
		});

		this.control_sheetApply.addEventListener('click', function(e){
			ths._applySpriteSheetControlFields();
		});
		this.control_spriteApply.addEventListener('click', function(e){
			ths._applySpriteControlFields();
		})
		
		this.control_addSprite.addEventListener('click', function(e){
			ths._addSpritePressed();
		});
		this.control_removeSprite.addEventListener('click', function(e){
			ths._removeSpritePressed();
		});

		this.control_sheetKey.addEventListener('keypress', function(e){
			if(e.keyCode == 13) ths._applySpriteSheetControlFields(); 
		});
		this.control_sheetImagePath.addEventListener('keypress', function(e){
			if(e.keyCode == 13) ths._applySpriteSheetControlFields(); 
		});

		this.control_spriteKey.addEventListener('keypress', function(e){
			if(e.keyCode == 13) { 
				let sel = ths.selectedSpriteIndices;
				ths._applySpriteControlFields();
				ths._updateSpriteSelect(); 
				ths._setSelectedSpriteOptions(sel);
			}
		});
		this.control_spritePosition.addEventListener('keypress', function(e){
			if(e.keyCode == 13) ths._applySpriteControlFields(); 
		});
		this.control_spriteSize.addEventListener('keypress', function(e){
			if(e.keyCode == 13) ths._applySpriteControlFields(); 
		});
		this.control_spritePivot.addEventListener('keypress', function(e){
			if(e.keyCode == 13) ths._applySpriteControlFields(); 
		});

		this.control_generateOutput.addEventListener('click', function(e){
			ths._generateOutputPressed();
		});
		this.control_export.addEventListener('click', function(e){
			ths._exportPressed();
		});

		this.control_outputText.addEventListener('click', function(e){
			ths._onOutputTextClicked();
		});
	}

	/** Create all the internal objects inside the SpriteSheetEditor */
	InitializeFields(){

		// create the canvas context
		this.context = this.canvas.getContext("2d");

		// ceate the camera
		this._camera = new Camera(new Vec2(this.canvas.width, this.canvas.height));
	}

	/** Fully initialize the SpriteSheetEditor Object */
	Initialize(){
		this.FindAllElements();
		this.AddEventListeners();
		this.InitializeFields();
	}

	/** clear the editor canvas */
	ClearCanvas(){
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	/** reset the camera's orientation */
	ResetCamera(){

		// flag camera to be centered on spritesheet selection
		this._camera.position = null;

		// reset camera zoom factor
		this._camera.zoom = 1;
	}

	/** renders the selected spritesheets to the editor canvas */
	RenderCanvas(){

		let sheets = this.selectedSpriteSheets;
		let spacing = 10;
		let tpos = new Vec2();

		// clear the render and camera canvas
		this.ClearCanvas();
		this._camera.ClearCanvas();
		
		// if the camera's position is set to null, re-center it
		if(this._camera.position == null){
			
			// get the total viewport size by iterating through each spritesheet and adding 
			// their sizes together
			let vpSize = Vec2.zero;
			for(let i = 0; i < sheets.length; i++){
				vpSize.x = Math.max(vpSize.x, sheets[i].image.width);
				vpSize.y += sheets[i].image.height + spacing;
			}

			// set the camera position
			this._camera.position = vpSize.Times(0.5);
		}

		// iterate through each sprite sheet
		for(let i = 0; i < sheets.length; i++){

			// get the sheet reference and calculate where to draw it
			let sheet = sheets[i];

			// draw the spritesheet image at the specified position
			this._drawSpriteSheet(sheet, tpos.clone);
			tpos.y += sheet.image.height + spacing;
		}

		// render the camera view onto the canvas
		this.context.drawImage(this._camera.renderTarget, 0, 0);
	}

	/** 
	 * @returns {Object} 
	 * 
	 */
	GetJSONObject(){

		// create the base object
		let r = {};

		r.assetPack = this.control_assetPackName.value;

		// create the array of spriteSheets
		r.spriteSheets = [];
		let sheets = this.allSpriteSheets;
		for(let i = 0; i < sheets.length; i++){
			let jSheet = sheets[i].ToJSONObject();
			jSheet.source = sheets[i].source;
			r.spriteSheets.push(jSheet);
		}

		return r;
	}

	/** @private @param {Number} action */
	_setCanvasCursorFromAction(action){

			// determine the cursor that should display based on the action
			switch(action){
				case SpriteEditMouseAction.none:
					this.canvas.style.cursor = "default";
					break;
				case SpriteEditMouseAction.default:
					this.canvas.style.cursor = "default";
					break;
				case SpriteEditMouseAction.createSprite:
					this.canvas.style.cursor = "crosshair";
					break;
				case SpriteEditMouseAction.panCamera:
					this.canvas.style.cursor = "grab";
					break;
				case SpriteEditMouseAction.selectSprite:
					this.canvas.style.cursor = "default";
					break;
				case SpriteEditMouseAction.dragSprite:
					this.canvas.style.cursor = "move";
					break;
				case SpriteEditMouseAction.spriteResize_E: 
					this.canvas.style.cursor = "e-resize";
					break;
				case SpriteEditMouseAction.spriteResize_S:
					this.canvas.style.cursor = "s-resize";
					break;
				case SpriteEditMouseAction.spriteResize_W:
					this.canvas.style.cursor = "w-resize";
					break;
				case SpriteEditMouseAction.spriteResize_N:
					this.canvas.style.cursor = "n-resize";
					break;
				case SpriteEditMouseAction.spriteResize_SE:
					this.canvas.style.cursor = "se-resize";
					break;
				case SpriteEditMouseAction.spriteResize_SW:
					this.canvas.style.cursor = "sw-resize";
					break;
				case SpriteEditMouseAction.spriteResize_NW:
					this.canvas.style.cursor = "nw-resize";
					break;
				case SpriteEditMouseAction.spriteResize_NE:
					this.canvas.style.cursor = "ne-resize";
					break;
			}
	}

	/** @private @type {Number} @param {Vec2} pos */
	_getCanvasClickActionAtPos(pos){

		// return none if no sprite sheets are selected
		let sheetSel = this.selectedSpriteSheets;
		if(sheetSel.length <= 0)
			return 0;

		// get the mouse world position and a reference to the spritesheet
		let worldMouse = this._camera.ViewportToWorlPosition(pos);
		let sheet = sheetSel[0];

		// determine if world mouse is inside the sprite sheet bounds
		let inBounds = (
			worldMouse.x >= 0 && worldMouse.x <= sheet.image.width &&
			worldMouse.y >= 0 && worldMouse.y <= sheet.image.height
		);

		// if the world mouse is inside the sprite sheet bounds
		if(inBounds){

			// iterate through each sprite
			let spriteSel = this.selectedSprites;
			for(let i = sheet.sprites.length - 1; i >= 0; i--){

				// get sprite ref and determine if mouse is inside the sprite
				let sprite = sheet.sprites[i];
				let spriteSelected = spriteSel.indexOf(sprite) >= 0;

				if(spriteSelected){

					// declare variables to help determine which edge is grabbable at mouse pos
					let edgeSize = 2 / this._camera.zoom;
					let resizeDir = Vec2.zero;
					let spBounds = sprite.bounds;
					let spBoundsExt = spBounds.Expanded(edgeSize);

					// determine if mouse is in correct position to resize the sprite
					if(spBoundsExt.OverlapsPoint(worldMouse)){
						if(Math.abs(worldMouse.x - spBounds.right) <= edgeSize){
							resizeDir.x = 1;
						}
						else if (Math.abs(worldMouse.x - spBounds.left) <= edgeSize){
							resizeDir.x = -1;
						}
						if(Math.abs(worldMouse.y - spBounds.bottom) <= edgeSize){
							resizeDir.y = 1;
						}
						if(Math.abs(worldMouse.y - spBounds.top) <= edgeSize){
							resizeDir.y = -1;
						}
					}

					// right edge grab position
					if(resizeDir.x > 0){
						if(resizeDir.y > 0) return SpriteEditMouseAction.spriteResize_SE;
						if(resizeDir.y < 0) return SpriteEditMouseAction.spriteResize_NE; 
						return SpriteEditMouseAction.spriteResize_E;
					}
					// left edge grab position
					else if (resizeDir.x < 0){
						if(resizeDir.y > 0) return SpriteEditMouseAction.spriteResize_SW;
						if(resizeDir.y < 0) return SpriteEditMouseAction.spriteResize_NW;
						return SpriteEditMouseAction.spriteResize_W;
					}
					// bottom edge grab position
					if(resizeDir.y > 0){
						if(resizeDir.x > 0) return SpriteEditMouseAction.spriteResize_SE;
						if(resizeDir.x < 0) return SpriteEditMouseAction.spriteResize_SW;
						return SpriteEditMouseAction.spriteResize_S;
					}
					// top edge grab position
					else if (resizeDir.y < 0){
						if(resizeDir.x > 0) return SpriteEditMouseAction.spriteResize_NE;
						if(resizeDir.x < 0) return SpriteEditMouseAction.spriteResize_NW;
						return SpriteEditMouseAction.spriteResize_N;
					}
				}
			
				// if the world mouse is inside the sprite
				let inSprite = sprite.bounds.OverlapsPoint(worldMouse);
				if(inSprite){

					// if the sprite is already selected
					if(spriteSelected){

						// move sprite
						return SpriteEditMouseAction.dragSprite;
					}

					// if the sprite is not selected
					else
						// select sprite
						return SpriteEditMouseAction.selectSprite;
				}
			}

			// create sprite
			return SpriteEditMouseAction.createSprite;
		}

		// none
		return SpriteEditMouseAction.default;
	}

	/** @private */
	_importPressed(){

		// get self reference for callbacks
		let ths = this;

		// open dialogue for user to select a data file from disk
		IO.UploadDataFile(function(data){

			// create the asset pack from the data
			let pak = AssetPack.FromStringData(data, ths.referencePath);

			// parse the asset pack data when it's finished loading
			pak.OnFinishedLoading(function(){
				ths._parseImportedAssets(pak);
			});
		}, ".json");
	}

	/** @param {AssetPack} pack */
	_parseImportedAssets(pack){
		
		// iterate through each asset in the pack
		for(let i1 = 0; i1 < pack.selfAssets.length; i1++){

			// get a reference to currently iterated asset
			let asset = pack.selfAssets[i1];

			// skip iteration if it is not a sprite sheet asset
			if(!(asset instanceof SpriteSheetAsset)){
				continue;
			}

			// get a reference to the spritesheet that the asset contains
			/** @type {SpriteSheet} */
			let sheet = asset.globalInstance;
			sheet.key = asset.key;

			// set the sheet source to a local path
			sheet.source = IO.GetRelativeLocalPath(sheet.image.src, this.referencePath);

			// add spritesheet to editor
			this._addSpriteSheet(sheet);

			// iterate through each sprite asset inside the spritesheet asset
			for(let i2 = 0; i2 < asset.spriteAssets.length; i2++){

				// get a reference to the currently iterated sprite asset and it's contained sprite
				let spriteAsset = asset.spriteAssets[i2];
				let sprite = spriteAsset.globalInstance;

				// assign the sprite asset key to the instance
				sprite.key = spriteAsset.key;
				sprite.sheet = sheet;
			}
		}
	}

	/** @private @param {SpriteSheet} sheet @param {Vec2} pos */
	_drawSpriteSheet(sheet, pos){

		// don't draw non-loaded images
		if(sheet.image.width == 0 || sheet.image.height == 0)
			return;

		// draw the spritesheet image
		this._camera.DrawImage(sheet.image, pos.clone, Vec2.zero);
		let selSprites = this.selectedSprites;

		// iterate through each sprite and draw it
		for(let i = sheet.sprites.length - 1; i >= 0; i--){
			let sprite = sheet.sprites[i];
			
			// get the sprite rect to draw
			let trect = sprite.bounds.clone;
			let thickness = 1;
			trect.position = trect.position.Plus(pos);

			// determine if sprite is selected or not
			let isSel = selSprites.indexOf(sprite) >= 0;

			// draw the pivot pixel
			let pivSize = 2;
			let pRect = new Rect(
				trect.position.Plus(sprite.pivot).Minus(new Vec2(pivSize / this._camera.zoom / 2)), 
				Vec2.one.Times(pivSize / this._camera.zoom)
			);
			pRect.DrawFill(
				this._camera,
				isSel ? this._pivotColor1 : this._pivotColor0
			);

			// draw the sprite outline
			trect.DrawOutline(
				this._camera, 
				isSel ? this._spriteRectColor1 : this._spriteRectColor0, 
				thickness
			);
		}
	}

	/** @private @param {WheelEvent} e */
	_onCanvasScroll(e){

		// normalize the wheel delta
		let delta = Math.sign(e.deltaY);

		// if scroll down, zoom out
		if(delta > 0){
			this._camera.zoom *= 0.7071067811865475;
		}

		// if scroll up, zoom in
		else if(delta < 0){
			this._camera.zoom /= 0.7071067811865475;
		}

		// re render the canvas
		this.RenderCanvas();
	}

	/** @private @param {MouseEvent} e */
	_onCanvasMouseMove(e){

		// begin drag if necessary
		if(this._canvasMouseAction >= 0 && this._canvasDragBegin == null)
			this._handleDragStart(e);

		// if dragging, handle the drag action
		else if(this._canvasDragBegin != null){
			this._handleDragMove(e);
		}

		// if not dragging
		else {
			
			// determine what action would perform if the user clicked 
			let action = this._getCanvasClickActionAtPos(new Vec2(e.offsetX, e.offsetY));

			// change the cursor to reflect the action
			this._setCanvasCursorFromAction(action);
		}
	}

	/** @private @param {MouseEvent} e */
	_onCanvasMouseDown(e){
		
		this._canvasMouseAction = e.button;

		// if left click
		if(this._canvasMouseAction == 0){

			let canvasPos = new Vec2(e.offsetX, e.offsetY);

			// determine the action that should be performed by the click
			this._canvasMouseAction = 
				this._getCanvasClickActionAtPos(canvasPos);

			// select sprite at cursor if that was the action needed
			if(this._canvasMouseAction == SpriteEditMouseAction.selectSprite){
				this._setSelectedSpriteOptions([this._getSpriteIndexAtCanvasPos(canvasPos)]);
			}
		}
	}

	/** @private @param {MouseEvent} e */
	_onCanvasMouseUp(e){
		
		this._canvasMouseAction = -1;
		if(this._canvasDragBegin != null)
			this._handleDragEnd(e);
	}

	/** @private @param {MouseEvent} e */
	_handleDragStart(e){
		if(this._canvasDragBegin != null)
			return;
		
		this._cumMouseDelta = Vec2.zero;
		this._canvasDragBegin = new Vec2(e.offsetX - e.movementX, e.offsetY - e.movementY);

		switch(this._canvasMouseAction){

			case SpriteEditMouseAction.createSprite:
				this._dragStartAction_createSprite();
				break;
				
			// change cursor if panning camera
			case SpriteEditMouseAction.panCamera:
				this.canvas.style.cursor = "grabbing";
				break;
				
		}

		// if resizing
		if(this._canvasMouseAction >= SpriteEditMouseAction.spriteResize_E){
			this._dragStartAction_resizeSprite();
		}
	}

	/** @private @param {MouseEvent} e */
	_handleDragMove(e){

		// get the mouse delta
		let mDelta = new Vec2(e.movementX, e.movementY);

		// add to the cumulative world mouse delta
		this._cumMouseDelta = this._cumMouseDelta.Plus(mDelta.Times(1 / this._camera.zoom));

		// get the mouse world delta floored
		let flooredDelta = this._cumMouseDelta.floored;
		this._cumMouseDelta = this._cumMouseDelta.Minus(flooredDelta);

		// perform the proper mouse drag action
		switch(this._canvasMouseAction){
			case SpriteEditMouseAction.none: break;
			case SpriteEditMouseAction.default: break;
			case SpriteEditMouseAction.createSprite:
				this._dragMoveAction_resizeSprite(flooredDelta, this._resizeAxisMultiplier);
				this._centerSpritePivotPressed();
				break;
			case SpriteEditMouseAction.panCamera:
				this._dragMoveAction_panCamera(mDelta);
				break;
			case SpriteEditMouseAction.selectSprite:
				this._dragMoveAction_selectSprite(new Vec2(e.offsetX, e.offsetY));
				break;
			case SpriteEditMouseAction.dragSprite:
				this._dragMoveAction_dragSprite(flooredDelta);
				break;
			
			case SpriteEditMouseAction.spriteResize_E:
			case SpriteEditMouseAction.spriteResize_S:
			case SpriteEditMouseAction.spriteResize_W:
			case SpriteEditMouseAction.spriteResize_N:
			case SpriteEditMouseAction.spriteResize_SE:
			case SpriteEditMouseAction.spriteResize_SW:
			case SpriteEditMouseAction.spriteResize_NW:
			case SpriteEditMouseAction.spriteResize_NE:
				this._dragMoveAction_resizeSprite(flooredDelta, this._resizeAxisMultiplier);
				break;
		}
	}

	/** @private */
	_dragMoveAction_panCamera(delta){

		// pan camera and re render
		this._camera.position = this._camera.position.Minus(delta.Times(1 / this._camera.zoom));
		this.RenderCanvas();
	}

	/** @private */
	_dragMoveAction_selectSprite(pos){

		// get the current sprite index at the specified pos
		let ind = this._getSpriteIndexAtCanvasPos(pos);

		// do nothing if no sprite exists at pos
		if(ind < 0)
			return;

		// find the indices of the already selected sprites
		let sel = this.selectedSpriteIndices;

		// append the index of the sprite at the specified position to the sel array
		sel.push(ind);

		// set the selected sprites to include the all the indices in the sel array
		this._setSelectedSpriteOptions(sel);
	}

	/** @private */
	_dragMoveAction_dragSprite(delta){
		
		// return if no movement
		if(delta.x == 0 && delta.y == 0)
			return;

		// move all the selected sprites by the specified delta
		let sel = this.selectedSprites;
		for(let i = sel.length - 1; i >= 0; i--){
			sel[i].bounds.position = sel[i].bounds.position.Plus(delta);
		}

		// re render the canvas and update sprite fields
		this._updateSpriteFields();
		this.RenderCanvas();
	}

	/** @private @param {Vec2} delta @param {Vec2} axisMult */
	_dragMoveAction_resizeSprite(delta, axisMult){

		// if no movement, do nothing
		if(delta.x == 0 && delta.y == 0){
			return;
		}

		// lock the scale to the specified axes through multiplying by the axis multiplier
		let mov = delta.Scaled(axisMult);

		// iterate through each selected sprite
		let sel = this.selectedSprites;
		for(let i = sel.length - 1; i >= 0; i--){
			
			// drag the end resize point according to the delta
			this._resizePoints1[i] = this._resizePoints1[i].Plus(mov);

			// get reference to sprite
			let sprite = sel[i];

			// resize sprite bounds from resize points
			sprite.bounds = Rect.FromPoints(this._resizePoints0[i], this._resizePoints1[i]);
		}

		// re-render the canvas and refresh sprite fields
		this._updateSpriteFields();
		this.RenderCanvas();
	}

	/** @private */
	_dragStartAction_createSprite(){

		// get all selected spriteSheets
		let sheetSel = this.selectedSpriteSheets;

		// ensure that a sprite sheet is selected
		if(sheetSel.length != 1){
			return;
		}

		// get the starting position of the drag in world coordinates
		let pos = this._camera.ViewportToWorlPosition(this._canvasDragBegin).floored;

		// create the new sprite and add it to the spritesheet
		let sprite = new Sprite();
		sprite.bounds.position = pos;
		sprite.bounds.size = Vec2.one;
		sprite.sheet = sheetSel[0];
		sprite.key = sheetSel[0].key + "_" + sheetSel[0].sprites.length;
		sheetSel[0].sprites.push(sprite);

		// update the sprite select control
		this._updateSpriteSelect();

		// set the resize drag variables appropriately
		this._resizePoints0 = [pos.clone];
		this._resizePoints1 = [pos.Plus(Vec2.one)];
		this._resizeAxisMultiplier = Vec2.one;
		
		// select the most recently added option
		this.control_spriteSelect.selectedIndex = this.control_spriteSelect.childElementCount - 1;
		this.control_spriteSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */
	_dragStartAction_resizeSprite(){

		// find the anchor of each sprite rect to be start and endpoints
		let startAnchor = Vec2.zero;
		let endAnchor = Vec2.one;
		switch(this._canvasMouseAction){

			// if dragging the sprite's bottom or right side, set the start anchor to be the 
			// top left and the end anchor opposite to that
			case SpriteEditMouseAction.spriteResize_SE:
			case SpriteEditMouseAction.spriteResize_E:
			case SpriteEditMouseAction.spriteResize_S:
				startAnchor = Vec2.zero;
				endAnchor = Vec2.one;
				break;
			
			// if dragging the sprite's top or left side, set the start anchor to be the bottom
			// left and the end anchor opposite to that
			case SpriteEditMouseAction.spriteResize_NW:
			case SpriteEditMouseAction.spriteResize_W:
			case SpriteEditMouseAction.spriteResize_N:
				startAnchor = Vec2.one;
				endAnchor = Vec2.zero;
				break;

			// if dragging the sprite's bottom left corner, set that to be the end anchor and
			// the start andchor opposite to that
			case SpriteEditMouseAction.spriteResize_SW:
				startAnchor = new Vec2(1, 0)
				endAnchor = new Vec2(0, 1);
				break;

			// if dragging the sprite's top right corner, set that to be the end anchor and the
			// start anchor opposite to that
			case SpriteEditMouseAction.spriteResize_NE:
				startAnchor = new Vec2(0, 1);
				endAnchor = new Vec2(1, 0);
				break;
		}

		// if resizing vertically, lock horizontal axis
		if(this._canvasMouseAction == SpriteEditMouseAction.spriteResize_N || 
			this._canvasMouseAction == SpriteEditMouseAction.spriteResize_S ) {
			this._resizeAxisMultiplier = new Vec2(0, 1);
		}

		// if resizing horizontally, lock vertical axis
		else if(this._canvasMouseAction == SpriteEditMouseAction.spriteResize_E || 
			this._canvasMouseAction == SpriteEditMouseAction.spriteResize_W ) {
			this._resizeAxisMultiplier = new Vec2(1, 0);
		}

		// if resizeing diagonally, do not lock any axes
		else if(this._canvasMouseAction >= SpriteEditMouseAction.spriteResize_SE){
			this._resizeAxisMultiplier = Vec2.one;
		}

		// initialize the resize points to empty arrays
		this._resizePoints0 = [];
		this._resizePoints1 = [];

		// get the selected sprites and itereate through each of them
		let sel = this.selectedSprites;
		for(let i = 0; i < sel.length; i++){

			// initiealize the resize points based on the sprite boundary corners
			let sprite = sel[i];
			this._resizePoints0.push(sprite.bounds.position.Plus(sprite.size.Scaled(startAnchor)));
			this._resizePoints1.push(sprite.bounds.position.Plus(sprite.size.Scaled(endAnchor)));
		}
	}

	/** @private @param {MouseEvent} e */
	_handleDragEnd(e){

		this._canvasMouseAction = -1;
		this._canvasDragBegin = null;
		this.canvas.style.cursor = "default";
	}

	/** @private @type {Number} @param {Vec2} pos */
	_getSpriteIndexAtCanvasPos(pos){

		// return none if no sprite sheets are selected
		let sheetSel = this.selectedSpriteSheets;
		if(sheetSel.length <= 0)
			return -1;

		// get the world position of the canvas pos and a reference to the spritesheet
		let worldPos = this._camera.ViewportToWorlPosition(pos);
		let sheet = sheetSel[0];

		// determine if world mouse is inside the sprite sheet bounds
		let inBounds = (
			worldPos.x >= 0 && worldPos.x <= sheet.image.width &&
			worldPos.y >= 0 && worldPos.y <= sheet.image.height
		);

		if(!inBounds){
			return -1;
		}

		// iterate through each sprite
		for(let i = sheet.sprites.length - 1; i >= 0; i--){

			// return the index of the current iteration if the current sprite overlaps 
			// the world position
			if(sheet.sprites[i].bounds.OverlapsPoint(worldPos))
				return i;
		}

		return -1;
	}

	/** @private @param {Array<Number>} indices */
	_setSelectedSpriteOptions(indices){

		let changed = true;

		// iterate through each sprite selection option
		let ops = this.control_spriteSelect.options;
		for(let i = ops.length - 1; i >= 0; i --){

			let o = ops[i].selected;

			// select option if it's index is included in the indices parameter
			if(indices.indexOf(i) >= 0){
				ops[i].selected = true;
			}

			// otherwise deselect the option
			else {
				ops[i].selected = false;
			}

			// set 'changed' flag to true if any of the selected options were changed
			if(o != ops[i].selected)
				changed = true;
		}

		// dispatch a new event saying that the selection has changed
		if(changed)
			this.control_spriteSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */ 
	_addSpriteSheetPressed(){

		// create a new spritesheet object
		let sheet = new SpriteSheet(new Image(), []);
		sheet.key = "spriteSheet_" + this._sheetsCreated++;

		// add the sprite sheet object
		this._addSpriteSheet(sheet);

		// select the most recently added option
		this.control_sheetSelect.selectedIndex = this.control_sheetSelect.childElementCount - 1;
		this.control_sheetSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */
	_centerSpritePivotPressed(){

		// iterate through each sprite and center it's pivot
		let sel = this.selectedSprites;
		for(let i = sel.length - 1; i >= 0; i--){
			sel[i].pivot = sel[i].size.Times(0.5);
		}

		// re-render the canvas
		this._updateSpriteFields();
		this.RenderCanvas();
	}

	/** @private @param {SpriteSheet} sheet */
	_addSpriteSheet(sheet){

		// create the option element and add it to the spriteshee selection control
		let op = document.createElement("option");
		op.draggable = true;
		op.sheet = sheet;
		op.innerHTML = sheet.key;
		this.control_sheetSelect.appendChild(op);
	}
	
	/** @private */
	_generateOutputPressed(){
		let ob = this.GetJSONObject();
		this.control_outputText.innerHTML = JSON.stringify(ob, null, "\t");
		this.control_outputText.style.height = "0px";
		this.control_outputText.style.height = this.control_outputText.scrollHeight + 25 + "px";
	}

	/** @private */
	_exportPressed(){

		let filename = prompt("Enter the exported file name:");
		IO.DownloadDataFile(filename + ".json", JSON.stringify(this.GetJSONObject(), null, "\t"));
	}

	/** @private */ 
	_removeSpriteSheetPressed(){
		
		// get each selected option
		let sel = this.control_sheetSelect.selectedOptions;
		
		// iterate through each selected sheet
		for (let i = sel.length - 1; i >= 0; i--){
			this.control_sheetSelect.removeChild(sel[i]);
		}

		// deselect and dispatch selection change event
		this.control_sheetSelect.selectedIndex = -1;
		this.control_sheetSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */
	_updateSheetControlsEnabled(){

		let sel = this.selectedSpriteSheets;

		// if nothing is selected, disable the controls
		if(sel.length <= 0){
			this.control_sheetKey.disabled = true;
			this.control_sheetImagePath.disabled = true;
			this.control_sheetApply.disabled = true;
			this.control_removeSheet.disabled = true;
			this.control_spriteAutoGenerate.disabled = true;
		}

		// if something is seleceted, enable them
		else{
			this.control_sheetKey.disabled = false;
			this.control_sheetImagePath.disabled = false;
			this.control_sheetApply.disabled = false;
			this.control_removeSheet.disabled = false;
			this.control_spriteAutoGenerate.disabled = false;
		}
	}

	/** @private */
	_onSheetSelectChange(){

		// reset the camera and re-render the canvas
		this.ResetCamera();
		this.RenderCanvas();

		// update the sprite controls
		this._updateSpriteSelect();
		this._updateSheetControlsEnabled();
		this._updateSpriteControlsEnabled();

		// update the spritesheet fields
		this._updateSheetFields();
	}

	/** @private */
	_updateSheetFields(){
		
		// get each selected option
		let sel = this.control_sheetSelect.selectedOptions;
		
		// if nothing is selected, empty text fields and return
		if(sel.length <= 0){
			this.control_sheetKey.value = "";
			this.control_sheetImagePath.value = "";
			return;
		}
		
		// define variables to fill the spritesheet values with
		let cumKey = sel[0].sheet.key;
		let cumPath = sel[0].sheet.source;

		// iterate through each selected sheet
		for (let i = sel.length - 1; i >= 1; i--){
			let sheet = sel[i].sheet;

			// nullify the cumulative values if they do not match
			if(cumKey != sheet.key)
				cumKey = null;
			if(cumPath != sheet.src)
				cumPath = null;
		}

		// set the sheet key control input text value
		if(cumKey != null && cumKey.length > 0) this.control_sheetKey.value = cumKey;
		else this.control_sheetKey.value = "";

		// set the sheet image path control input text value
		if(cumPath != null && cumPath.length > 0) this.control_sheetImagePath.value = cumPath;
		else this.control_sheetImagePath.value = "";
	}

	/** @private */
	_onSpriteSelectChange(){
		this._updateSpriteControlsEnabled();
		this._updateSpriteFields();
		this.RenderCanvas();
	}

	/** @private */
	_onOutputTextClicked(){

		// do nothing if the text area already has focus
		if(this._outTextSelected)
			return;

		// get the currently selected stuff
		let selection = window.getSelection();

		// clear the selection
		selection.removeAllRanges();

		// create a range that can include the text in the text area
		let range = document.createRange();

		// set the range to include the output text and then select it
		range.selectNodeContents(this.control_outputText);
		selection.addRange(range);

		// set the output text selected flag to true so the use isn't firced to select everything 
		// if they've already clicked on the output text element
		this._outTextSelected = true;

		// when the text output element loses focus, unset the flag
		let ths = this;
		this.control_outputText.addEventListener('blur', function(e){
			ths._outTextSelected = false;
		});
	}

	/** @private */
	_applySpriteSheetControlFields(){
		
		// get all the selection options
		let sel = this.control_sheetSelect.selectedOptions;
		
		// if nothing is selected, empty text fields and return
		if(sel.length <= 0){
			this.control_sheetKey.value = "";
			this.control_sheetImagePath.value = "";
			return;
		}
		
		// iterate through each selected sheet
		let ths = this;
		for (let i = sel.length - 1; i >= 0; i--){
			let op = sel[i];
			let sheet = op.sheet;

			// change the sheet key
			sheet.key = this.control_sheetKey.value;
			op.innerHTML = sheet.key;

			// if the image path was changed, load the new image
			if(sheet.source != this.control_sheetImagePath.value){

				// create a new image object
				sheet.image = new Image();

				// update the canvas when image finishes loading
				sheet.image.addEventListener("load", function(e){
					console.log("loaded")
					ths.ResetCamera();
					ths.RenderCanvas();
				});
				
				// set the relative path
				sheet.source = this.control_sheetImagePath.value;

				// load the image at the specified path
				sheet.image.src = IO.GetRelativePath(
					this.control_sheetImagePath.value,
					this.control_referencePath.value
				);
			}
		}
	}

	/** @private */
	_applySpriteControlFields(){

		// get the selected sprites
		let sel = this.selectedSprites;

		// iterate through each selected sprite
		for(let i = sel.length - 1; i >= 0; i--){
			let sprite = sel[i];
			
			if(this.control_spriteKey.value.length > 0){
				sprite.key = this.control_spriteKey.value;
			}
			if(this.control_spritePosition.value.length > 0){
				let splstr = this.control_spritePosition.value.split(',')
				let pos = new Vec2(Number.parseInt(splstr[0]), Number.parseInt(splstr[1]));

				if(!Number.isNaN(pos.x) && !Number.isNaN(pos.y))
					sprite.bounds.position = pos.rounded;
				else
					this.control_spritePosition.value = "";
			}
			if(this.control_spriteSize.value.length > 0){
				let splstr = this.control_spriteSize.value.split(',')
				let size = new Vec2(Number.parseInt(splstr[0]), Number.parseInt(splstr[1]));
				
				if(!Number.isNaN(size.x) && !Number.isNaN(size.y))
					sprite.bounds.size = size.rounded;
				else
					this.control_spriteSize.value = "";
			}
			if(this.control_spritePivot.value.length > 0){
				let splstr = this.control_spritePivot.value.split(',')
				let pivot = new Vec2(Number.parseFloat(splstr[0]), Number.parseFloat(splstr[1]));
				
				if(!Number.isNaN(pivot.x) && !Number.isNaN(pivot.y)){
					sprite.pivot = pivot.Times(2).rounded.Times(0.5);
				}
				else
					this.control_spritePivot.value = "";
			}
		}

		// re render the canvas
		this.RenderCanvas();
	}

	/** @private */
	_addSpritePressed(){

		// get all selected spriteSheets
		let sheetSel = this.selectedSpriteSheets;

		// ensure that a sprite sheet is selected
		if(sheetSel.length != 1){
			return;
		}

		// create the new sprite and add it to the spritesheet
		let sprite = new Sprite();
		sprite.sheet = sheetSel[0];
		sprite.key = sheetSel[0].key + "_" + sheetSel[0].sprites.length;
		sheetSel[0].sprites.push(sprite);

		// update the sprite select control
		this._updateSpriteSelect();

		// select the most recently added option
		this.control_spriteSelect.selectedIndex = this.control_spriteSelect.childElementCount - 1;
		this.control_spriteSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */
	_removeSpritePressed(){
		
		// get all selected spriteSheets
		let sheetSel = this.selectedSpriteSheets;

		// ensure that a sprite sheet is selected
		if(sheetSel.length != 1){
			return;
		}

		// remove all the selected option elements
		let ops = this.control_spriteSelect.selectedOptions;
		for(let i = ops.length - 1; i >= 0; i--){

			// remove the sprite from the spritesheet
			let spriteIndex = sheetSel[0].sprites.indexOf(ops[i].sprite);
			sheetSel[0].sprites.splice(spriteIndex, 1);
			
			// remove the sprite option selectable element
			this.control_spriteSelect.removeChild(ops[i]);
		}

		// deselect
		this.control_spriteSelect.selectedIndex = -1;
		this.control_spriteSelect.dispatchEvent(new MouseEvent("change"));
	}

	/** @private */
	_updateSpriteControlsEnabled(){
		
		let sheetSel = this.selectedSpriteSheets;

		// if there is any number of sprite sheets selected other than 1, disable the controls
		if(sheetSel.length != 1){
			this.control_spriteSelect.disabled = true;
			this.control_addSprite.disabled = true;
			this.control_removeSprite.disabled = true;
			this.control_spriteKey.disabled = true;
			this.control_spritePosition.disabled = true;
			this.control_spriteSize.disabled = true;
			this.control_spritePivot.disabled = true;
			this.control_centerSpritePivot.disabled = true;
			this.control_spriteApply.disabled = true;
			return;
		}

		// if there is one selected, enable controls
		else{
			this.control_spriteSelect.disabled = false;
			this.control_addSprite.disabled = false;
		}

		let spriteSel = this.selectedSprites;

		// if there is more than 1 selected, enable some of them
		if(spriteSel.length <= 0){
			this.control_addSprite.disabled = false;
			this.control_removeSprite.disabled = true;
			this.control_spriteKey.disabled = true;
			this.control_spritePosition.disabled = true;
			this.control_spriteSize.disabled = true;
			this.control_spritePivot.disabled = true;
			this.control_centerSpritePivot.disabled = true;
			this.control_spriteApply.disabled = true;
		}
		else{
			this.control_addSprite.disabled = false;
			this.control_removeSprite.disabled = false;
			this.control_spriteKey.disabled = false;
			this.control_spritePosition.disabled = false;
			this.control_spriteSize.disabled = false;
			this.control_spritePivot.disabled = false;
			this.control_centerSpritePivot.disabled = false;
			this.control_spriteApply.disabled = false;
		}
	}

	/** @private */
	_updateSpriteFields(){

		// get the selected sprites
		let sprites = this.selectedSprites;

		// if there are none selected, set fields to empty and return
		if(sprites.length <= 0){
			this.control_spriteKey.value = "";
			this.control_spritePosition.value = "";
			this.control_spriteSize.value = "";
			this.control_spritePivot.value = "";
			return;
		}

		// declare values to compare and see if they can be displayed cumulatively for all 
		// sprites selected
		let cumKey = sprites[0].key;
		let cumPos = sprites[0].bounds.position;
		let cumSize = sprites[0].bounds.size;
		let cumPivot = sprites[0].pivot;

		// iterate through each sprite
		for(let i = sprites.length - 1; i >= 1; i--){
			let sprite = sprites[i];

			// check to see if the cumulative values match up
			if(cumKey != sprite.key) cumKey = null;
			if(cumPos !=null && !Vec2.Approximately(cumPos, sprite.bounds.position)) 
				cumPos = null;
			if(cumSize != null && !Vec2.Approximately(cumSize, sprite.bounds.size)) 
				cumSize = null;
			if(cumPivot != null && !Vec2.Approximately(cumPivot, sprite.pivot)) 
				cumPivot = null;
		}

		// assign the key values to their respective cumulative values if they match up
		this.control_spriteKey.value = cumKey != null ? cumKey : "";
		this.control_spritePosition.value = cumPos != null ? cumPos.x + "," + cumPos.y : "";
		this.control_spriteSize.value = cumSize != null ? cumSize.x + "," + cumSize.y : "";
		this.control_spritePivot.value = cumPivot != null ? cumPivot.x + "," + cumPivot.y : "";
	}

	/** @private */
	_updateSpriteSelect(){

		// if there is any number of sprite sheets selected other than 1
		let sheetSel = this.selectedSpriteSheets;
			
		// remove all the option elements
		let ops = this.control_spriteSelect.options;
		for(let i = ops.length - 1; i >= 0; i--){
			this.control_spriteSelect.removeChild(ops[i]);
		}

		// if there is any number of sprite sheets selected other than 1, do nothing further
		if(sheetSel.length != 1){
			return;
		}

		// iterate through each sprite
		let sprites = sheetSel[0].sprites;
		for(let i = 0; i < sprites.length; i++){

			// add the sprite to the select element
			this._addSpriteSelectOption(sprites[i]);
		}
	}

	/** @private @param {Sprite} sprite */
	_addSpriteSelectOption(sprite){

		// create the option element and add it to the select element
		let op = document.createElement("option");
		op.draggable = true;
		op.sprite = sprite;
		op.innerHTML = sprite.key;
		this.control_spriteSelect.appendChild(op);
	}
}