///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class HiveshipUIContainer extends UIContainer{

	constructor(){
		super();

		/** @type {UIWindow} */
		this.HUD = null;

		/** @type {Hiveship} */
		this.hiveship = null;

		/** @type {Collectible}*/
		this._reserveItem = null;

		/** @type {CanvasImageSource} */
		this._reserveItemCountGraphic = null;

		/** @type {Number} */
		this._reserveItemCount = 0;
	}

	/**
	 * @type {HiveshipUIContainer} creates a Hiveship ui container from a hiveship object
	 * @param {Hiveship} ship the ship that the ui container belongs to
	 * @param {Vec} resolution the resolution of the UI container
	 */
	static FromHiveship(ship, resolution){
		
		let hud = new HiveshipHUDOverlay();
		hud.hiveship = ship;
		
		let r = new HiveshipUIContainer();
		r.uiCam = new Camera(resolution);
		r.hiveship = ship;

		hud.OnAdded(r);
		r.HUD = hud;

		return r;
	}

	/** @type {Collectible} */
	get itemReserve(){

		// return null if there is nothing or an empty item in the reserve
		let r = this._reserveItem;
		if(r != undefined){
			if(r.amount <= 0)
				return null;
		}
		else
			return null;

		// if there is something in the reserve return it
		return r;
	}
	set itemReserve(value){
		this._reserveItem = value;

		// reset graphic if need be
		if(value == null){
			this._reserveItemCountGraphic = null;
		}
	}

	/** @override */
	DrawCursor(){
		super.DrawCursor();
		this.DrawItemReserve();
	}

	DrawItemReserve(){

		let itm = this.itemReserve;
		if(itm != null){
			this.uiCam.DrawSprite(
				itm.icon,
				this.cursorPosition.rounded.Plus(new Vec2(10))
			);

			if(itm.amount > 1){

				// if the item count is different, flag graphic for regeneration
				if(this._reserveItemCount != itm.amount){
					this._reserveItemCountGraphic = null
				}

				// if the item count graphic is null, generate it
				if(this._reserveItemCountGraphic == null){

					// generate item graphic from default font
					this._reserveItemCount = itm.amount;
					this._reserveItemCountGraphic = 
						SpriteFont.default.GenerateTextImage(itm.amount.toString(), 1, -1);
				}

				// draw the item count text graphic
				this.uiCam.DrawImage(
					this._reserveItemCountGraphic,
					this.cursorPosition.rounded.Plus(new Vec2(20))
				)
			}
		}
	}

	/** @override */
	Draw(ctx){

		// clear the ui canvas
		this.uiCam.ClearCanvas();

		// draw the HUD in the background of the UI
		this.HUD.Draw(this.uiCam);

		// iterate through each window backwards so the top window is drawn last
		for(let i = this._windows.length - 1; i >= 0; i--){

			// draw each window individually
			this._windows[i].Draw(this.uiCam);
		}

		// draw the cursor if necessary
		if(this._cursorEnabled){
			this.DrawCursor();
		}

		// render the camera image onto the specified target context
		ctx.drawImage(this.uiCam.renderTarget, 0, 0, ctx.canvas.width, ctx.canvas.height);
	}
}

/** 
 * A UI window designed spicifically to display a hiveship's HUD
 */
class HiveshipHUDOverlay extends UIOverlayWindow{

	constructor(){
		super();

		this.bgColor = Color.Transparent;

		/** @type {Hiveship} */
		this.hiveship = null;

		/** @type {CanvasImageSource} */
		this.text_droneLabel =  SpriteFont.default.GenerateTextImage("Drone:", 1, -1);
		/** @type {CanvasImageSource} */
		this.text_hiveshipLabel =  SpriteFont.default.GenerateTextImage("Hive:", 1, -1.01);
		/** @type {CanvasImageSource} */
		this.text_weakSignal =  SpriteFont.default.GenerateTextImage("Weak signal", 1, -1.0);
		/** @type {CanvasImageSource} */
		this.text_returnToShip =  SpriteFont.default.GenerateTextImage("Return to hiveship", 1, -1.0);
	}

	/**
	 * @param {Camera} cam 
	 */
	DrawHiveshipStats(cam){
		
		// store values in local variables for easier access
		let hive = this.hiveship;
		let canvasRect = cam.viewRect;

		// calculate rect for max health bar
		let maxHealth = new Rect(new Vec2(), new Vec2(90, 5));
		maxHealth.bottomRight = canvasRect.bottomRight.Plus(new Vec2(-5, -5));

		// calculate values needed to draw stat bars
		let healthperc = Math.max(0, hive.health / hive.maxHealth);
		let health = maxHealth.clone;
		health.size.x *= healthperc;
		
		// draw a flashing red health indicator if low health
		if(healthperc < 0.4 && (this.uiContainer.elapsedTime % 0.5 < 0.2)){
			let healthnot = health.clone;
			healthnot.left = health.right;
			healthnot.width = maxHealth.width - health.width;
			
			cam.FillRect(healthnot, "rgba(255, 0, 0, 0.5)");
		}

		// draw the hiveship health bar
		cam.FillRect(health, "rgba(255, 255, 0, 1)");
		cam.StrokeRect(maxHealth, "rgba(100, 100, 0, 0.75)", 1);
		
		// draw the drone repair bar if necessary
		if(this.hiveship != null && this.hiveship.drone != null){
			if(this.hiveship.drone.isRemoved && this.hiveship.health > 0){
			
				let camrect = this.camera.viewRect;
				let percent = 1 - (
					this.hiveship.droneFabricationTime / this.hiveship.droneFabricationDelay);

				this.camera.FillRect(
					new Rect(camrect.bottomLeft.Plus(
						new Vec2(4, -11)), 
						new Vec2(91, 6)), "#333"
					);
				this.camera.FillRect(
					new Rect(camrect.bottomLeft.Plus(
						new Vec2(5, -10)), 
						new Vec2(89 * percent, 4)), "#DDD"
					);
			}
		}
	}

	/**
	 * @param {Camera} cam 
	 */
	DrawDroneStats(cam){

		// store values in local variables for easier access
		let drone = this.hiveship.drone;
		let canvasRect = cam.viewRect;

		// calculate health bar rect
		let maxHealth = new Rect(new Vec2(), new Vec2(90, 5));
		maxHealth.bottomLeft = canvasRect.bottomLeft.Plus(new Vec2(5, -5));

		// calculate values needed for drawing stat bars
		let healthperc = drone.health / drone.maxHealth;
		let health = maxHealth.clone;
		health.size.x *= healthperc
		
		// draw flashing red health indicator if low health
		if(healthperc < 0.4 && (this.uiContainer.elapsedTime % 0.5 < 0.2)){

			let healthnot = health.clone;
			healthnot.left = health.right;
			healthnot.width = maxHealth.width - health.width - 1;
			cam.FillRect(healthnot, "rgba(255, 0, 0, 0.5)");
		}

		// draw health bar
		cam.FillRect(health, "rgba(0, 150, 150, 1)");
		cam.StrokeRect(maxHealth, "rgba(0, 50, 250, 0.75)", 1);

		// draw the ammo bar
		let tpos = canvasRect.bottomLeft.Plus(new Vec2(5, -17));
		cam.FillRect(new Rect(tpos, new Vec2(60 * drone.getTotalAmmoPercent(), 4)), "#333");

		// draw ammo bar for both weapons if available
		if(!!drone.currentWeapon && !!drone.altWeapon){
			cam.FillRect(new Rect(tpos, new Vec2(60 * drone.currentWeapon.ammoPercent, 1)), "#AAA");
			cam.FillRect(new Rect(tpos.Plus(new Vec2(0, 2)), new Vec2(60 * drone.altWeapon.ammoPercent, 1)), "#AAA");
		}

		// otherwise only draw the ammo bar for the weapon that exists
		else{
			let wep = drone.altWeapon || drone.currentWeapon;
			cam.FillRect(new Rect(tpos, new Vec2(60 * wep.ammoPercent, 3)), "#AAA");
		}

		// draw the ammo bar outline
		cam.StrokeRect(new Rect(tpos, new Vec2(60, 4)), "#333", 1);

		// calculate the values needed to draw the drone signal bars
		let signal = this.hiveship.droneSignal;
		let signalBars = Math.floor(signal * 7) + 1;

		// draw the drone signal bars
		tpos = canvasRect.bottomLeft.Plus(new Vec2(11, -21));
		for(let i = 5; i > 0; i--){
			
			let ipos = tpos.Plus(new Vec2(i * 3, 0));
			let h = i * 2;
			let rect = new Rect(ipos, new Vec2(2, h));
			rect.position.y -= h;

			// set the fill color to white if they are active signal bars, or grey if not
			let fillCol = i < signalBars ? "#DDD" : "#333";
			cam.FillRect(rect, fillCol);
		}

		// draw signal fuzz and warning message
		if(signal <= 0){

			// calculate the fuzz intensity based off drone distance
			let fuzz = (this.hiveship.droneDistance - this.hiveship.droneRange) / 200;
			
			// fill the screen with fuzz based on the alpha value
			cam.FillRect(canvasRect.clone, "rgba(0,0,0," + fuzz.toString() + ")");

			// draw flashing text that warns player
			if(this.uiContainer.elapsedTime % 0.5 >= 0.25){
				cam.DrawImage(this.text_weakSignal, canvasRect.center.Plus(new Vec2(0, -10)));
				cam.DrawImage(this.text_returnToShip, canvasRect.center.Plus(new Vec2(0, 5)));
			}
		}

		// draw the battery power of the drone
		cam.FillRect(new Rect(tpos.Plus(new Vec2(-6, -10)), new Vec2(6, 10)), "#DDD");
		cam.FillRect(new Rect(tpos.Plus(new Vec2(-4, -11)), new Vec2(2, 1)), "#DDD");
		cam.FillRect(new Rect(tpos.Plus(new Vec2(-5, -9)), new Vec2(4, 8)), "#222");
		cam.FillRect(new Rect(tpos.Plus(new Vec2(-4, -8)), new Vec2(2, 6)), "#DDD");
	}

	DrawWindowBackground(){

		// clear the window canvas
		this.camera.ClearCanvas();

		// update the hiveship radar canvas
		this.hiveship.DrawRadar();

		let viewRect = this.camera.viewRect;

		this.camera.DrawImage(this.text_hiveshipLabel, viewRect.bottomRight.Plus(new Vec2(-18, -16)));
		if(this.hiveship != null && !this.hiveship.isRemoved){
			this.DrawHiveshipStats(this.camera);
		}

		this.camera.DrawImage(this.text_droneLabel, viewRect.bottomLeft.Plus(new Vec2(18, -38)));
		if(this.hiveship.drone != null && !this.hiveship.drone.isRemoved){
			this.DrawDroneStats(this.camera);
		}

		// draw the radar in the top left corner
		this.camera.DrawImage(this.hiveship.radarCanvas, viewRect.topLeft, Vec2.zero);
	}
}