///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * a data structure that represents an interactive UI window
 */
class UIWindow extends UIElementContainer{

	constructor(){
		super();

		this._childElements = [];
		this.focusable = true;
		this.selfAnchor = new Vec2(0.5);

		/** @type {UIContainer} */
		this.uiContainer = null;

		/** @type {Camera} */
		this.camera = null;
	}

	/** @type {Boolean} returns true if the window has a container */
	get isOpen(){
		return this.uiContainer != null;
	}

	/** @type {HTMLCanvasElement} */
	get canvas(){ return this.camera.renderTarget; }

	/** @type {CanvasRenderingContext2D} */
	get context(){ return this.camera.renderContext; }

	/** @type {Vec2} returns the top left corner of this window */
	get position(){
		
		let uiRect = this.uiContainer.uiCam.viewRect;
		return uiRect.topLeft.Plus(
			uiRect.size
			.Scaled(this.parentAnchor)
			.Plus(this.positionOffset)
			//.Minus(new Vec2(this.padding))
		);

		// find the top left corner of the window
		// let topleft = this.parentAnchor.Minus(
		// 	new Vec2(
		// 		this.canvas.width * this.selfAnchor.x,
		// 		this.canvas.height * this.selfAnchor.y
		// 	));

		// return topleft;
	}

	get size(){
		return this.uiContainer.uiCam.resolution.Scaled(this.sizeAnchor).Plus(this.sizeOffset);
	}

	// /** @override @type {Rect} */
	// get bounds(){
	// 	return new Rect(Vec2.zero, this.camera.resolution);
	// }

	/**
	 * adds a UI element to the window
	 * @param {UIElement} uiElement 
	 */
	AddUIElement(uiElement){

		// add the element to the element array
		uiElement._parent = this;
		this._childElements.push(uiElement);
	}

	/** 
	 * called when the window is added to a ui container
	 * @param {UIContainer} uiContainer
	 */
	OnAdded(uiContainer){

		// set the ui container
		this.uiContainer = uiContainer;

		// get the resolution of the window
		let resolution = this.size;

		// create a new camera for the window
		this.camera = new Camera(resolution);

		// // initialize the canvas
		// this.canvas = document.createElement("canvas");
		// this.canvas.width = Math.ceil(this.uiContainer.uiCam.renderTarget.width * this.size.x);
		// this.canvas.height = Math.ceil(this.uiContainer.uiCam.renderTarget.height * this.size.y);

		// // initialize the rendering context
		// this.context = this.canvas.getContext("2d");
	}

	/**
	 * @type {Vec2} transforms a normalized position from 0 to 1, into a pixel position relative 
	 * to this window's top left corner
	 * @param {Vec2 normalizedPos}
	 */
	GetPositionFromNormalized(normalizedPos){

		// find the top left corner of the window
		let topleft = this.bounds.topLeft;

		// return the normalized position weighted by the canvas size plus the position of the 
		// window's top left corner
		return new Vec2(
			this.canvas.width * normalizedPos.x, 
			this.canvas.height * normalizedPos.y
			).Plus(topleft);
	}

	/** @virtual */
	DrawWindowBackground(){

		// fill the window with the semi-transparent black color
		this.context.fillStyle = "#00000088";
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	}

	/** 
	 * @virtual 
	 * @param {Camera} cam the context the window is to be rndered onto
	 * @param {Rect} bounds the window border bounds
	*/
	DrawWindowBorder(cam, bounds){ }

	/**
	 * draws all the UI elements inside the window
	 * @param {Camera} cam 
	 */
	Draw(cam){

		// clear the window canvas
		this.camera.ClearCanvas();

		// draw the window background
		this.DrawWindowBackground();

		// iterate through each element in the window
		let elems = this.GetChildElements();
		for(let i = 0; i < elems.length; i++){

			// draw each element individually
			elems[i].Draw(this.camera);
		}

		let topleft = this.bounds.topLeft;

		// find the top left corner of the window
		// let topleft = this.uiContainer.uiCam.viewRect.topLeft.Plus(
		// 	this.uiContainer.uiCam.resolution.Scaled(this.parentAnchor)
		// );
		// topleft = topleft.Minus(
		// 	new Vec2(
		// 		this.canvas.width * this.selfAnchor.x,
		// 		this.canvas.height * this.selfAnchor.y
		// ));

		// render the window canvas
		cam.DrawImage(this.canvas, topleft, Vec2.zero);

		// draw the window's border
		this.DrawWindowBorder(
			cam, 
			new Rect(
				topleft, 
				new Vec2(this.canvas.width, this.canvas.height)
			));
	}
}

/**
 * a UI window that is used to overlay the whole screen
 */
class UIOverlayWindow extends UIWindow{

	constructor(){
		super();

		this.parentAnchor = Vec2.zero;
		this.selfAnchor = Vec2.zero;
		this.sizeAnchor = new Vec2(1, 1);

		/** @type {Color} */
		this.bgColor = Color.TranslucentBlack;
	}

	/** @override */
	DrawWindowBackground(){
		this.context.fillStyle = this.bgColor.ToRGBA();
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	}
}

/**
 * 
 */
class UITitledWindow extends UIWindow{

	constructor(){
		super();

		/** @type {String} */
		this.title = "Title";

		/** @type {HTMLCanvasElement} */
		this._titleGraphic = null;
	}

	/** @type {UITitledWindow} */
	static FromTitle(title){

		let r = new UITitledWindow();
		r.title = title;
		r.GenerateTitleGraphic();
		r.GenerateControls();

		return r;
	}

	GenerateControls(){

		// Test UI Container
		// let baseControlGroup = new UIElementLayoutGroup();
		// baseControlGroup.selfAnchor = new Vec2(0);
		// baseControlGroup.parentAnchor = new Vec2(0);
		// baseControlGroup.positionOffset = new Vec2(-3);
		// baseControlGroup.sizeAnchor = new Vec2(0.5, 0.5);
		// baseControlGroup.sizeOffset = new Vec2(-5);
		// baseControlGroup.layoutSpacing = new Vec2(3);

		// for(let i = 15; i > 0; i--){
		// 	let but = UIButton.FromText("x");
		// 	but.sizeAnchor = new Vec2(0);
		// 	but.sizeOffset = new Vec2(Math.random() * 10 + 10, Math.random() * 10 + 5).rounded;
		// 	baseControlGroup.AddElement(but);
		// }

		// this.AddElement(baseControlGroup);

		// create the close button and position it in the top left corner
		/** @type {UIButton} */
		this.closeButton = UIButton.FromText("Close");
		this.closeButton.SizeToGraphic(new Vec2(2));
		this.closeButton.sizeAnchor = new Vec2(0);
		this.closeButton.selfAnchor = new Vec2(1);
		this.closeButton.parentAnchor = new Vec2(1);
		this.closeButton.positionOffset = new Vec2(-3);
		this.closeButton.submitAction = UIElement.Action_CloseWindow(this);
		this.AddUIElement(this.closeButton);
	}

	/** generates the title graphic that is displayed on the border */
	GenerateTitleGraphic(){

		// generate the text image based on the title string
		this._titleGraphic =  SpriteFont.default.GenerateTextImage(this.title, 1, -1);
	}

	/** sets the title and generates the new title graphic */
	SetTitle(title){
		this.title = title;
		this.GenerateTitleGraphic();
	}

	/**
	 * @override draw the window border with the title at the top
	 * @param {Camera} cam the ui camera 
	 * @param {Rect} bounds the boundaries of the inside of the window
	 */
	DrawWindowBorder(cam, bounds){

		let titleRightOffset = this._titleGraphic.height + 5;

		// this.parentAnchor = new Vec2(Math.abs(this.uiContainer.elapsedTime % 10 - 5) / 10 + 0.25);

		// define the path for the title tab in the window's top left
		let path = [
			bounds.topLeft.floored,
			bounds.topLeft.Plus(new Vec2(0, -titleRightOffset)).floored,
			bounds.topLeft.Plus(
				new Vec2(titleRightOffset + this._titleGraphic.width, -titleRightOffset)).floored,
			bounds.topLeft.Plus(
				new Vec2(2 * titleRightOffset + this._titleGraphic.width, 0)).floored,
		];

		// fill in the tab area with translucent black
		cam.DrawPathFill(path, "#00000088", true);

		//draw title at the top left of the window border
		cam.DrawImage(
			this._titleGraphic,
			bounds.topLeft.Plus(new Vec2(titleRightOffset, 0)),
			new Vec2(0, 1)
		);

		// append the rest of the border outline to the path
		path.push(
			bounds.topRight.floored,
			bounds.bottomRight.floored,
			bounds.bottomLeft.floored,
		);

		// draw the border path outline
		cam.DrawPathOutline(path, "#FFF", 1, true, true);
	}
}

class UIFloatingElement extends UIWindow{

	constructor(){
		super();

		// be default the floating element will mostly be used as a tooltip dialogue so it will 
		// not be able to be interacted with
		this.focusable = false;

		/** @type {Number} */
		this._lifeStart = null;

		/** @type {Number} */
		this._maxLife = null;
	}

	/**
	 * @type {UIFloatingElement} 
	 * @param {UIElement} elem the element to put inside the window
	 * @param {Rect} targetRect the rect position where the floating element should appear
	 * @param {Number} maxLife the amount of time before the window auto-closes, leave null for
	 * 	infinite lifetime
	*/
	static FromElement(elem, targetRect, maxLife = null){

		elem.selfAnchor = new Vec2(0.5);
		elem.parentAnchor = new Vec2(0.5);
		elem.positionOffset = Vec2.zero;

		let r = new UIFloatingElement();
		r._maxLife = maxLife;

		// position and size the window
		r.selfAnchor = Vec2.zero;
		r.parentAnchor = Vec2.zero;
		r.sizeAnchor = Vec2.zero;
		r.sizeOffset = targetRect.size.clone;
		r.positionOffset = targetRect.topLeft;

		// add the element and return the window
		r.AddUIElement(elem);
		return r;
	}

	/** @type {Number} how long the window has been open for in seconds */
	get currentLife(){
		return this.uiContainer.elapsedTime - this._lifeStart;
	}

	/** @override */
	OnAdded(uiContainer){
		super.OnAdded(uiContainer);

		// set the start time
		this._lifeStart = this.uiContainer.elapsedTime;
	}

	/** 
	 * sets the max lifetime to the current lifetime plus the specified amoount in seconds 
	 * @param {Number} additional
	 */
	SetCurrentToMaxLife(additional){
		this._maxLife = this.currentLife + additional;
	}

	/**
	 * @override draw the border
	 * @param {Camera} cam the ui camera 
	 * @param {Rect} bounds the boundaries of the inside of the window
	 */
	DrawWindowBorder(cam, bounds){
		
		// define the path for the title tab in the window's top left
		let path = [
			bounds.topLeft.floored,
			bounds.topRight.floored,
			bounds.bottomRight.floored,
			bounds.bottomLeft.floored,
		];

		// draw the border path outline
		cam.DrawPathOutline(path, "#FFF", 1, true, true);
	}

	/** @override */
	Draw(cam){
		super.Draw(cam);

		if(this._maxLife != null){
			if(this.currentLife > this._maxLife){
				this.uiContainer.CloseWindow(this);
			}
		}
	}
}

class UIInventoryWindow extends UITitledWindow{

	constructor(){
		super();

		/** @type {SlottedInventory} */
		this.inventory = null;

		/** @type {UIElementLayoutGroup} */
		this.UISlotContainer = null;
	}

	/**
	 * Creates a UIInventoryWindow from the specified inventory object
	 * @param {SlottedInventory} inventory 
	 */
	static FromInventory(inventory, title = "Inventory"){

		//create a new inventory instance
		let r = new UIInventoryWindow();
		r.SetTitle(title);

		// set the inventory and generate the controls
		r.inventory = inventory;
		r.GenerateControls();

		// return it
		return r;
	}

	/** flag the window to regenterate the slot UI elements when it is next rendered */
	MarkDirty(){
		this._dirtySlots = true;
	}

	/** Generate the elements for the inventory window */
	GenerateControls(){

		// create the slot container layout group
		this.UISlotContainer = UIInventorySlotGroup.FromInventory(this.inventory);

		// position and size the slot container
		let padding = 15;
		this.UISlotContainer.selfAnchor = new Vec2(0.5, 0);
		this.UISlotContainer.parentAnchor = new Vec2(0.5, 0);
		this.UISlotContainer.positionOffset = new Vec2(0, padding);
		this.UISlotContainer.sizeAnchor = Vec2.one;
		this.UISlotContainer.sizeOffset = new Vec2(padding * -2, -padding - 30);

		// specify the layout parameters of the slot container
		this.UISlotContainer.layoutAnchorOuter = new Vec2(0.5, 0);
		this.UISlotContainer.layoutAnchorInner = new Vec2(0, 0.5);
		this.UISlotContainer.layoutSpacing = new Vec2(3);

		// add slot container group to the window
		this.AddElement(this.UISlotContainer);

		super.GenerateControls();
	}
}