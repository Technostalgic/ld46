///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** @enum enumerator for the UI action type */
var UIActionType = {
	Select: 0,
	Deselect: 1,
	Submit: 2,
	AltSubmit: 3,
	Use: 4,
	Back: 5,
	NavLeft: 6,
	NavRight: 7,
	NavUp: 8,
	NavDown: 9,
	ScrollUp: 10,
	ScrollDown: 11
}

/**
 * @abstract a base class used for objects that are meant to contain UI Elements such as UI 
 * Windows and UI layout group elements
 */
class UIElementContainer{
	constructor(){

		/** @type {Array<UIElement>} */
		this._childElements = [];

		/** @type {Vec2} */
		this.selfAnchor = new Vec2(0.5, 0.5);

		/** @type {Vec2} */
		this.parentAnchor = new Vec2(0.5, 0.5);

		/** @type {Vec2} */
		this.positionOffset = new Vec2();
		
		/** @type {Vec2} */
		this.sizeAnchor = new Vec2(0.5, 0.5);

		/** @type {Vec2} */
		this.sizeOffset = new Vec2();

		/** @type {UIElementContainer} */
		this._parent = null;
	}

	/** @virtual @type {Vec2} */
	get position(){
		let tpos = this.parentContainer.GetPositionFromNormalized(this.parentAnchor);
		return tpos.Plus(this.positionOffset);
	}

	/** @type {Vec2} */
	get size(){
		return this._parent.bounds.size.Scaled(this.sizeAnchor).Plus(this.sizeOffset);
	}

	/** @type {Rect} */
	get bounds(){
		
		// get the position of the element and initialize a rect to return
		let pos = this.position;
		let r = new Rect();

		// set the size of the bounds to the graphic size
		r.size = this.size;

		// calculate how much the bounds are offset depending on the anchor and bounds size
		let off = this.selfAnchor.Scaled(r.size);
		r.position = pos.Minus(off);

		return r;
	}

	/**
	 * Sends the action upwards to the container's parent, if it has one
	 * @param {UIActionType} action the action type that was performed - use 'UIActionType' enum
	 * @param {UIElementContainer} target the base target of the action
	 */
	SendActionUpwards(action, target){
		if(this._parent != null)
			this._parent.SendAction(action, target);
	}

	/**
	 * @virtual Override to specify actions for the specific class
	 * @param {UIActionType} action the action type that was performed - use 'UIActionType' enum
	 * @param {UIElementContainer} target should probably be left null
	 */
	SendAction(action, target = null){
		this.SendActionUpwards(action, target || this);
	}

	/** 
	 * @virtual @type {Array<UIElement>} use depth-first traversal to recursively retreive 
	 * all child elements 
	 */
	GetChildElements(){
		
		// return null if this element does not support childElements
		if(this._childElements == null)
			return null;

		// define a return array
		let r = [];

		// iterate through each child element sequentally
		for(let i1 = 0; i1 < this._childElements.length; i1++){

			// push the direct child element
			r.push(this._childElements[i1]);

			// get all the children of the direct child element
			let rt = this._childElements[i1].GetChildElements();
			if(rt == null)
				continue;

			// push all the child elements of the direct child
			for(let i2 = 0; i2 < rt.length; i2++){
				r.push(rt[i2]);
			}
		}

		// return the array if children
		return r;
	}

	/** 
	 * @virtual @type {Array<UIElement>} gets only the child elements who are direct descendents
	 * of this container
	 */
	GetDirectChildElements(){
		
		// return null if this element does not support childElements
		if(this._childElements == null)
			return null;

		return this._childElements;
	}

	/**
	 * returns the UI element at the specified index
	 * @param {Number} index 
	 */
	GetDirectChildAt(index){
		return this._childElements[index];
	}

	/** returns the number of direct children this container has */
	GetDirectChildCount(){
		return this._childElements.length;
	}

	/** 
	 * inserts the elements at the specefied index 
	 * @param {Number} index
	 * @param {UIElement|Array<UIElement>} elements
	 */
	InsertElementsAt(index, elements){

		// convert the elements parameter to an array if necessary
		if(elements instanceof UIElement)
			elements = [elements];

		// insert each individual element in sequential order at the specified index
		for(let i = 0; i < elements.length; i++){

			// ensure element does not already have a parent
			if(elements[i]._parent != null){
				throw "this element already has a parent";
			}

			elements[i]._parent = this;
			this._childElements.splice(index + i, 0, elements[i]);
		}
	}

	/** @abstract @param {UIElement} elem */
	AddElement(elem){ 
		
		if(elem._parent != null){
			throw "this element already has a parent";
		}

		// set the element's parent and add it to the children list
		elem._parent = this;
		this._childElements.push(elem);
	}

	/** @param {Array<UIElement>} elems */
	AddElements(elems){
		for(let i = 0; i < elems.length; i++){
			this.AddElement(elems[i]);
		}
	}

	/** @param {UIElement} elem */
	RemoveElement(elem){
		
		let ind = this._childElements.indexOf(elem);

		if(ind < 0)
			return;

		elem._parent = null;
		this._childElements.splice(ind, 1);
	}

	/**
	 * removes the child element at the specified index
	 * @param {Number} index 
	 */
	RemoveElementAt(index){
		this._childElements.splice(index, 1);
	}

	/** removes all the child elements from the container */
	RemoveAllElements(){

		if(this._childElements = null)
			throw "This container does not support child elements";

		// remove reference to this from all children
		for(let i = this._childElements.length - 1; i >= 0; i --){
			this._childElements[i]._parent = null;
		}

		// set new childElements array to remove references to previous children
		this._childElements = [];
	}

	/** @virtual @type {Vec2} @param {Vec2} normalizedPos */
	GetPositionFromNormalized(normalizedPos){

		// return the normalized position weighted by the element size plus the position of the 
		// window's top left corner
		let bounds = this.bounds;
		return new Vec2(
			bounds.width * normalizedPos.x, 
			bounds.height * normalizedPos.y
		).Plus(bounds.topLeft);
	}
}