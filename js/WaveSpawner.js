///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class WaveSpawner{

	constructor(){

		/** @type {Array<WaveEvent} */
		this.waveEvents = [];
		
		this.waveIntermissionLength = 25;
		this.waveIntermission = 6;

		this.currentWave = 0;
	}

	static InitializeMessages(){
		
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_warning =  SpriteFont.default.GenerateTextImage("Warning!", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_danger =  SpriteFont.default.GenerateTextImage("Danger!", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_asteroidField = SpriteFont.default.GenerateTextImage("Approaching asteroid field", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_meteorShower = SpriteFont.default.GenerateTextImage("Incoming meteor shower", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_hostiles = SpriteFont.default.GenerateTextImage("Hostiles incoming", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_unknown = SpriteFont.default.GenerateTextImage("Danger ahead", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_criticalDamage = SpriteFont.default.GenerateTextImage("Hive ship critical damage", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_safe = SpriteFont.default.GenerateTextImage("Entering safe zone", 1, -1);
		/** @type {HTMLCanvasElement} */
		WaveSpawner.message_unsafe = SpriteFont.default.GenerateTextImage("Leaving safe zone", 1, -1);
	}

	DoIntermission(container, dt){ 

		this.waveIntermission -= dt;
		if(this.waveIntermission <= 0){

			this.currentWave++;
			this.waveEvents.push(new BaseWaveEvent(this.currentWave, this));
		}
	}

	DrawIntermission(camera){

		if(this.waveIntermission <= 5){
			this.ShowMessages(camera, [WaveSpawner.message_warning, WaveSpawner.message_unsafe]);
		}
		else if(this.waveIntermission >= this.waveIntermissionLength - 3){
			this.ShowMessages(camera, [WaveSpawner.message_safe]);
		}
	}

	ShowMessages(camera, messages){
		
		/** @type {Rect} */
		let view = camera.viewRect;

		for(let i = messages.length - 1; i >= 0; i--){
			let off = messages[i].width % 2 / 2;
			camera.DrawImage(messages[i], new Vec2(view.center.x - off, view.top + 30 + (i * 15)));
		}
	}

	Update(objectContainer, origin, range, camera, dt){

		for(let i = this.waveEvents.length - 1; i >= 0; i--){
			this.waveEvents[i].Update(objectContainer, origin, range, camera, dt);
			if(this.waveEvents[i].lifetime > this.waveEvents[i].maxLifetime){
				this.waveEvents[i].onEnd(this);
				this.waveEvents.splice(i, 1);
			}
		}
		
		if(this.waveIntermission > 0){
			this.DoIntermission(objectContainer, dt);
			return;
		}

		if(this.waveEvents.length <= 0){
			this.waveIntermission = this.waveIntermissionLength;
		}
	}

	DrawHUD(camera){

		if(this.waveIntermission > 0){
			this.DrawIntermission(camera);
		}
		else{
			if(this.waveEvents.length > 0){
				this.waveEvents[this.waveEvents.length - 1].DrawEventHUD(this, camera);
			}
		}
	}
}

class WaveEvent{

	constructor(){

		/** @type {Array<EnemySpawner>} */
		this.spawners = [];

		/** @type {Number} */
		this.difficulty = 1;

		/** @type {Number} */
		this.maxLifetime = 1;

		/** @type {Number} */
		this.lifetime = 0;

		this.beepPlayed = 0;

		/** @type {Array<HTMLCanvasElement>} */
		this.messages = [];

		this.postCooldown = 0;
	}

	static EmptyEvent(lifetime){
		let r = new WaveEvent();
		r.maxLifetime = lifetime;
		return r;
	}

	static AsteroidField(lifeWeight, difficulty){

		let density = Math.max(difficulty / 5, 0.5);
		let speed = Math.max(Math.random() * (difficulty / 2), 30);
		let chaos = Math.max(Math.random() * (difficulty) + 5, 30);

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_warning, WaveSpawner.message_asteroidField];

		r.maxLifetime = lifeWeight;
		r.spawners.push(EnemySpawner.AsteroidFieldSpawner(100 / density, 3, 5, speed, chaos));

		return r;
	}

	static MeteorShower(lifeweight, difficulty){

		let density = Math.max(difficulty / 2 + 1, 2.5);
		let tier = 2 + Math.min(5, Math.floor(difficulty / 2));
		let speed = 30 + Math.min(30, difficulty * 5);
		let chaos = 10 + Math.min(20, difficulty * 2);

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_warning, WaveSpawner.message_meteorShower];

		r.maxLifetime = lifeweight * 0.25;
		r.postCooldown = lifeweight;

		let minTier = 1;
		let maxTier = tier;
		maxTier = Math.floor(maxTier);
		let maxTier2 = Math.floor(maxTier / 2) + 1;

		let spawner = EnemySpawner.AsteroidFieldSpawner(300 / density, minTier, maxTier, 0, chaos);
		let spawner2 = EnemySpawner.AsteroidFieldSpawner(100 / density, minTier, maxTier2, 0, chaos);
		
		let vel = Vec2.RandomDirection(speed);
		spawner.velocityAddition = vel;
		spawner2.velocityAddition = vel;

		r.spawners.push(spawner);
		r.spawners.push(spawner2);

		return r;
	}

	static KamikazeTrickle(lifeweight, difficulty){
		
		let rate = 8 + (difficulty);

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_warning, WaveSpawner.message_hostiles];

		r.maxLifetime = lifeweight * 2;
		r.spawners.push(EnemySpawner.EnemyTrickle(Kamikaze, rate));

		return r;
	}

	static KamikazeSwarm(lifeweight, difficulty){
		
		let density = Math.max(difficulty / 5, 1);
		let arcSize = difficulty;

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_danger, WaveSpawner.message_hostiles];

		r.maxLifetime = lifeweight * 0.25;
		r.postCooldown = lifeweight;
		r.spawners.push(EnemySpawner.EnemySwarm(Kamikaze, 100 / density, arcSize));

		return r;
	}

	static FighterTrickle(lifeweight, difficulty){
		
		let rate = 8 + (difficulty);

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_warning, WaveSpawner.message_hostiles];

		r.maxLifetime = lifeweight * 1.25;
		r.spawners.push(EnemySpawner.EnemyTrickle(Fighter, rate));

		return r;
	}

	static FighterSwarm(lifeweight, difficulty){
		
		let density = Math.max(difficulty / 5, 0.75);
		let arcSize = difficulty;

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_danger, WaveSpawner.message_hostiles];

		r.maxLifetime = lifeweight * 0.2;
		r.postCooldown = lifeweight;
		r.spawners.push(EnemySpawner.EnemySwarm(Fighter, 100 / density, arcSize));

		return r;
	}

	static HostileTrickle(lifeweight, difficulty){
		
		let rate = 8 + (difficulty);

		let r = new WaveEvent();
		r.messages = [WaveSpawner.message_warning, WaveSpawner.message_hostiles];

		r.maxLifetime = lifeweight * 2;
		r.spawners.push(EnemySpawner.EnemyTrickle(Kamikaze, rate / 2));
		r.spawners.push(EnemySpawner.EnemyTrickle(Fighter, rate / 2));

		return r;
	}

	Update(objectContainer, origin, range, camera, dt){

		for(let i = this.spawners.length - 1; i >= 0; i--){
			this.spawners[i].Update(objectContainer, origin, range, camera, dt);
		}

		if(this.lifetime < 0.5){
			if(this.messages.length > 0){
				if(this.beepPlayed <= this.lifetime){
					this.beepPlayed += 0.25;
					Camera.sfx_Select.play();
				}
			}
		}

		this.lifetime += dt;
	}

	onEnd(waveSpawner){

		if(this.postCooldown > 0)
			waveSpawner.waveEvents.push(WaveEvent.EmptyEvent(this.postCooldown));
	}

	DrawEventHUD(waveSpawner, camera){

		if(this.lifetime <= 3){
			if(this.lifetime % 0.4 < 0.2){
				if(this.messages.length > 0){
					waveSpawner.ShowMessages(camera, this.messages);
				}
			}
		}
	}
}

class BaseWaveEvent extends WaveEvent{

	constructor(waveNumber, waveSpawner){
		super();
		
		/** @type {WaveSpawner} */
		this.waveSpawner = waveSpawner;
		
		this.waveNumber = waveNumber;
		this.maxLifetime = 60 + waveNumber * 20;

		this.eventDelay = 0;
	}

	Update(objectContainer, origin, range, camera, dt){

		super.Update(objectContainer, origin, range, camera, dt);

		if(this.eventDelay <= 0){
			let maxEvents = ((this.waveNumber - 1) / 2) + 2;
			if(this.waveSpawner.waveEvents.length < maxEvents){
				this.AddRandomEvent();
			}
		}
		this.eventDelay -= dt;
	}

	AddRandomEvent(){

		this.eventDelay = 5 + Math.random() * 5;

		let maxEventLength = this.maxLifetime - this.lifetime + (Math.random() * (5 + 2 * this.waveNumber));
		let tLength = this.maxLifetime / (this.waveNumber + 1);
		tLength = (tLength / 2) + (Math.random() * tLength / 2);
		let length = Math.min(maxEventLength, tLength);

		let events = [
			WaveEvent.AsteroidField, 
			WaveEvent.MeteorShower,
			WaveEvent.KamikazeTrickle,
			WaveEvent.KamikazeTrickle,
			WaveEvent.KamikazeSwarm,
			WaveEvent.FighterTrickle,
			WaveEvent.FighterSwarm,
			WaveEvent.HostileTrickle,
		];

		let index = Math.floor(Math.random() * events.length);
		let evt = events[index](length, this.waveNumber);
		this.waveSpawner.waveEvents.push(evt);

		console.log("Began wave event " + events[index].name + " for ~" + Math.round(evt.maxLifetime) + " sec");
	}
}

class EnemySpawner{

	constructor(enemyType){

		/** @type {Function} */
		this.enemyType = enemyType;

		/** @type {Function} */
		this.enemyConstructor = function(){
			return new this.enemyType();
		}

		/** @type {Object} */
		this.enemyProperties = {};

		/** @type {Array<Character>} */
		this.childEnemies = [];

		this.spawnDelay = 1;
		this.spawnDelayVariance = 1;
		this.nextEnemyTimer = 1;

		this.spawnDirection = 0;
		this.spawnDirectionVariance = Math.PI;

		this.velocityAddition = Vec2.zero;
		this.velocityVariation = 0;

		this.enemyPadding = 20;
		this.viewPadding = 20;
		this.spawnTries = 5;
		this.spawnRetry = true;
	}

	static AsteroidFieldSpawner(padding, minTier = 1, maxTier = 5, speed = 0, chaos = 10){
		
		let r = new EnemySpawner();
		r.enemyType = Asteroid;
		r.enemyConstructor = function(){
			let tier = Math.floor(Math.random() * (maxTier - minTier + 1)) + minTier;
			return Asteroid.FromTier(tier);
		}

		r.velocityAddition = new Vec2(-speed, 0);
		r.velocityVariation = chaos;
		r.enemyPadding = padding;
		r.spawnTries = 1;
		r.spawnRetry = true;
		r.spawnDelay = 0.1;
		r.spawnDelayVariance = 0;

		return r;
	}

	static EnemyTrickle(enemyType, rate, consistency = 0.5){

		let r = new EnemySpawner();
		r.enemyType = enemyType;

		r.spawnDelay = 60 / rate;
		r.spawnDelayVariance = r.spawnDelay * (1 - consistency);

		return r;
	}

	static EnemySwarm(enemyType, padding, arcSize){
		
		let r = new EnemySpawner();
		r.enemyType = enemyType;

		r.spawnDirection = Math.random() * Math.PI * 2;
		r.spawnDirectionVariance = arcSize;
		r.enemyPadding = padding;
		r.spawnTries = 1;
		r.spawnRetry = true;
		r.spawnDelay = 0.1;
		r.spawnDelayVariance = 0;

		return r;
	}

	Update(objectContainer, origin, range, camera, dt){

		this.nextEnemyTimer -= dt;
		if(this.nextEnemyTimer <= 0){

			this.nextEnemyTimer += this.spawnDelay + Math.random() * this.spawnDelayVariance;
			this.Spawn(objectContainer, origin, range, camera);
		}
	}

	Spawn(objectContainer, origin, range, camera){

		let tpos = this.findSpawnPosition(origin, range, camera);

		if(tpos == null){
			if(this.spawnRetry)
				this.nextEnemyTimer = 0;
			return;
		}

		let enemy = this.enemyConstructor();
		enemy.position = tpos;
		enemy.velocity = this.velocityAddition.Plus(Vec2.RandomDirection(this.velocityVariation));
		this.childEnemies.push(enemy);
		objectContainer.AddGameObject(enemy);
	}

	/** 
	 * @param {GameObjectContainer} objectContainer 
	 * @param {Camera} camera
	 */
	findSpawnPosition(origin, range, camera){

		let avoid = camera.viewRect;
		avoid.AddPadding(this.viewPadding, this.viewPadding);

		let tpos = null;
		let i = this.spawnTries;
		do{
			let dir = this.spawnDirection + (Math.random() - 0.5) * this.spawnDirectionVariance;
			let dpos = Vec2.FromDirection(dir, range);
			
			tpos = origin.Plus(dpos);
			if(avoid.OverlapsPoint(tpos)){
				tpos = Vec2.FromDirection(tpos.direction, tpos.magnitude + avoid.size.magnitude);
			}

			i--;
			if( i < 0 )
				return null;
		} while(!this.spawnPointIsValid(tpos, avoid));

		return tpos;
	}

	spawnPointIsValid(point, avoidRect = null){
		
		if(avoidRect != null){
			if(avoidRect.OverlapsPoint(point))
				return false;
		}

		for(let i = this.childEnemies.length - 1; i >= 0; i--){
			if(point.Minus(this.childEnemies[i].position).magnitude < this.enemyPadding)
				return false;
		}

		return true;
	}
}