///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** @abstract */
class Projectile extends PhysObject {
	
	constructor(){
		super();

		this._alwaysUpdate = true;

		this._lastPos = new Vec2();
		this._lastPos2 = new Vec2();
		this.team = 0;

		this.rotation = 0;
		this.damage = 1;
		this.size = 1;

		this.lifetime = 0;
		this.maxLife = 1;

		this.hitSFX = null;
	}

	/**
	 * @override @type {Rect}
	 * returns the rectangular boundaries of the beam
	 */
	get bounds(){
		return Rect.FromPoints(this.position, this._lastPos);
	}

	onUpdate(dt){

		super.onUpdate(dt);

		this.lifetime += dt;
		if(this.lifetime > this.maxLife){
			this.Remove();
		}
		
		this._lastPos2 = this._lastPos;
		this._lastPos = this.position.clone;
	}

	checkForCollisions(){

		// pre-calculate some values so that raycasts can be performed with less cpu overhead
		let ray = new Ray(this._lastPos, this.position);
		let lastRay = null;

		// calculate the area that the raycast may overlap
		let rayBox = Rect.FromPoints(this._lastPos, this.position);
		
		// create some arrays to store the information about collisions
		let objs = [];
		let rayhits = [];
		let cols = [];

		// find all the colliders that are overlapping the projectile
		let overlaps = 
			this.container.FindOverlappingColliders(new Circle(this._lastPos, this.size));

		// iterate through each overlapping collider and add those collisions to the collision 
		// array
		for(let i = overlaps.length - 1; i >= 0; i--){
			
			/** @type {GameObject} */
			let obj = overlaps[i].parentObject;

			// if the object is destroyed, or if the objects are considered to be allied, 
			// don't check for collisions
			if(obj.isDestroyed || Character.AreTeamsAllied(obj.team, this.team))
				continue;

			// create a varable to store the raycast result
			/** @type {CirclecastResult} */
			let castResult = null;
	
			// if the projectile is already inside the other object's hitbox, a collision occured
			if(obj.collider.shape.OverlapsCircle(new Circle(this._lastPos, this.size))){
	
				// TODO account for situations where the circlecasts provide no intersection point
				if(lastRay == null){
					lastRay = new Ray(this._lastPos2, this._lastPos);
				}
				castResult = CollisionShape.CircleCast(obj.collider.shape, lastRay, this.size);
			}

			// if the raycast does hit, add it to the collsions array
			if(castResult != null && castResult.didEnter)
				cols.push(ColliderRaycastResult.FromResult(overlaps[i], castResult));
		}

		// push all the circlecast hits into the collisions array
		let casthits = this.container.Circlecast(ray, this.size);
		for(let i = casthits.length - 1; i >= 0; i--){
			
			/** @type {GameObject} */
			let obj = casthits[i].collider.parentObject;
			
			// if the object is destroyed, or if the objects are considered to be allied, 
			// don't check for collisions
			if(obj.isDestroyed || Character.AreTeamsAllied(obj.team, this.team))
				continue;

			cols.push(casthits[i]);
		}

		// iterate through each object that intersected the circlecast
		for(let i = cols.length - 1; i >= 0; i--){
			
			if(!cols[i].collider.isSolid)
				continue;

			/** @type {GameObject} */
			let obj = cols[i].collider.parentObject;

			// if the object is destroyed, or if the objects are considered to be allied, 
			// don't check for collisions
			if(obj.isDestroyed || Character.AreTeamsAllied(obj.team, this.team))
				continue;


			// perform the projectile's onHit action
			objs.push(obj);
			rayhits.push(cols[i].result);
		}

		// if there was a hit, delegate the information down to this.onAllHits
		if(objs.length > 0){
			this.onAllHits(objs, rayhits);
		}
	}

	/**
	 * @virtual called when the projectile hits one or more objects, this is available for override
	 * in case the projectile requires to know which order the objects were hit in, since by
	 * default onHit is called without regard to order. (ie a pnetrating projectile may only want 
	 * to hit the closest two objects and then destroy itself so that it does not hit any objects 
	 * past its penetrating power)
	 * @param {Array<Character>} objects the objects that the projectile has hit
	 * @param {Array<CirclecastResult>} rayHits the raycast results that from the collisions
	 */
	onAllHits(objects, rayHits){

		// iterate through each object hit and call onHit for each object
		for(let i = objects.length - 1; i >= 0; i--){
			this.onHit(objects[i], rayHits[i]);
		}
	}

	/**
	 * @virtual called when the projectile hits an object
	 * @param {Character} character 
	 * @param {CirclecastResult} rayHit 
	 */
	onHit(character, rayHit){ }

	FireFrom(position, angle, speed){
		this.position = position.clone;
		this._lastPos = position.clone;
		this._lastPos2 = position.clone;
		this.velocity = Vec2.FromDirection(angle, speed);
	}
}

class Beam extends Projectile{
	constructor(){
		super();

		this.damagePerSecond = 25;
		this.forcePerSecond = 1000;
		this.force = 0;
		this.damage = 0;
		this.size = 2;
		this.maxLife = 0.05;
		this.color = new Color(186, 100, 255, 1);

		this.endPoint = null;
	}

	/**
	 * @override
	 * @param {Array<Character>} objs 
	 * @param {Array<CirclecastResult>} rayHits 
	 */
	onAllHits(objs, rayHits){

		let closestIndex = 0;
		let dif1 = 
			rayHits[closestIndex].entryPoint.Minus(
			rayHits[closestIndex].ray.rayStart).magnitude;
		
		// iterate through each hit and find the one that occured nearest to the start of 
		// the projectiles raycast
		for(let i = objs.length - 1; i > 0; i--){
			
			let dif2 = 
				rayHits[i].entryPoint.Minus(
				rayHits[i].ray.rayStart).magnitude;

			if(dif2 < dif1)
				closestIndex = i;
		}

		// only call onHit on the object that the beam collided with first
		this.onHit(objs[closestIndex], rayHits[closestIndex]);
	}

	/**
	 * @override called when the projectile hits an object
	 * @param {Character} character 
	 * @param {CirclecastResult} rayHit 
	 */
	onHit(character, rayHit){
		
		if(!character.implements_damageable)
			return;
		
		this.endPoint = rayHit.entryPoint;
		
		character.Damage(this.damage, this.velocity.normalized.Times(this.force), rayHit.firstContact);
		this.container.AddGameObject(
			Effect.SmallHit(
				rayHit.firstContact, 
				character.velocity, 
				rayHit.entryNormalAngle + Math.PI));
	}

	onUpdate(dt){
		this.damage = this.damagePerSecond * dt;
		this.force = this.forcePerSecond * dt;
		this.checkForCollisions();
		super.onUpdate(dt);
	}

	/**
	 * @param {Camera} camera 
	 */
	onDraw(camera){

		let path = [this._lastPos2];
		if(this.endPoint != null)
			path.push(this.endPoint);
		else
			path.push(this.position);

		camera.DrawPathOutline(path, this.color.ToRGBA(), this.size, false);

		if(this.endPoint != null)
			this.Remove();
	}
}

class Bullet extends Projectile {
	
	constructor(){
		super();

		this._lastPos = new Vec2();
		this.team = 0;

		this.rotation = 0;
		this.damage = 1;

		this.lifetime = 0;
		this.maxLife = 1;

		this.strokeStyle = "#FF0";
		this.lineWidth = 1;
	}

	onUpdate(dt){

		this.checkForCollisions();
		super.onUpdate(dt);
	}

	/**
	 * called when the projectile hits an object
	 * @param {Character} character 
	 * @param {CirclecastResult} rayHit 
	 */
	onHit(character, rayHit){

		if(this.hitSFX != null){
			this.container.QueueSimpleSound(this.hitSFX, this.position)
		}

		this.Remove();
		character.Damage(this.damage, this.velocity.Times(this.mass), rayHit.firstContact);
		this.container.AddGameObject(
			Effect.SmallHit(
				rayHit.firstContact, 
				character.velocity, 
				rayHit.entryNormalAngle + Math.PI));
	}

	/** @param {Camera} camera */
	onDraw(camera){
		
		let path = [
				this.position,
				this.position.Plus(Vec2.FromDirection(this.rotation, this.velocity.magnitude * -0.03))
			];
		camera.DrawPathOutline(path, this.strokeStyle, this.lineWidth, false);
	}
}

class Shrapnel extends Bullet {
	
	constructor(){
		super();

		this._lastPos = new Vec2();
		this.team = 0;

		this.rotation = Math.random() * Math.PI;
		this.angularVelocity = Math.random() - 0.5;
		this.damage = 4;

		this.lifetime = 0;
		this.maxLife = 1;

		this.frame = Math.floor(Math.random() * 16);
		this.isFlipped = Math.random() >= 0.5;
	}

	onUpdate(dt){
		super.onUpdate(dt);
		this.rotation += this.angularVelocity * dt;
	}

	/** @param {Camera} camera */
	onDraw(camera){
		
		let path = [
				this.position,
				this.position.Plus(Vec2.FromDirection(this.rotation, this.velocity.magnitude * -0.02))
			];
		camera.DrawPathOutline(path, "#464", 1, false);

		let viewRect = camera.viewRect;
		viewRect.AddPadding(5, 5);

		if(!camera.viewRect.OverlapsPoint(this.position)){
			this.Remove();
			return;
		}

		camera.DrawSpriteFromSheet(Debris.spriteSheet_debris, this.frame, this.position, this.rotation, this.isFlipped);
	}
}

class CannonShot extends Bullet {
	
	constructor(){
		super();

		this._lastPos = new Vec2();
		this.team = 0;

		this.rotation = 0;
		this.damage = 1;

		this.lifetime = 0;
		this.maxLife = 1;

		this.penetrate = 1;

		this.strokeStyle = "#FF0";
		this.lineWidth = 1;
	}

	/**
	 * called when the projectile hits an object
	 * @param {Character} character 
	 * @param {CirclecastResult} rayHit 
	 */
	onHit(character, rayHit){

		character.Damage(this.damage, this.velocity.Times(this.mass), rayHit.firstContact);
		this.container.AddGameObject(Effect.SmallHit(rayHit.firstContact, character.velocity, -rayHit.entryNormalAngle));

		if(this.hitSFX != null){
			this.container.QueueSimpleSound(this.hitSFX, this.position)
		}
		
		if(!character.isRemoved){
			this.Remove();
		}
		else if (this.penetrate > 0){
			
			this.penetrate -= 1;
			this.damage -= 4;
			this.velocity = this.velocity.Times(0.5);
		}
	}
}

class EnemyBullet extends Projectile {
	
	constructor(){
		super();

		this._lastPos = new Vec2();
		this.team = 1;
		this.isThreat = true;

		this._hitbox = new Rect(new Vec2(), new Vec2(5))

		this.rotation = 0;
		this.damage = 1;

		this.lifetime = 0;
		this.maxLife = 2.25;

		this.lineWidth = 1;
	}

	/** @type {Rect} */
	get hitbox(){
		return this._hitbox;
	}

	centerHitbox(){
		this._hitbox.center = this.position;
	}

	onUpdate(dt){

		this.centerHitbox();
		this.checkForCollisions();
		super.onUpdate(dt);
	}

	Damage(){
		this.Remove();
	}

	/**
	 * called when the projectile hits an object
	 * @param {Character} character 
	 * @param {CirclecastResult} rayHit 
	 */
	onHit(character, rayHit){

		if(this.hitSFX != null){
			this.container.QueueSimpleSound(this.hitSFX, this.position);
		}

		this.Remove();
		character.Damage(this.damage, this.velocity.Times(this.mass), rayHit.firstContact);
		this.container.AddGameObject(Effect.SmallHit(rayHit.firstContact, character.velocity, rayHit.entryNormalAngle));
	}

	FireFrom(position, angle, speed){
		this.position = position.clone;
		this._lastPos = position.clone;
		this.velocity = Vec2.FromDirection(angle, speed);
	}

	/** @param {Camera} camera */
	onDraw(camera){
		
		let ss = this.lifetime % 0.5 >= 0.25 ? "#FF0" : "#F00";

		let path = [
				this.position,
				this.position.Plus(this.velocity.normalized.Times(-7.5))
			];
		camera.DrawPathOutline(path, ss, this.lineWidth, false);
	}
}

/** @abstract */
class Weapon{
	
	constructor(){ 

		this.maxAmmo = 100;
		this.currentAmmo = this.maxAmmo;
		this.ammoPack = 10;
	}

	get outOfAmmo(){
		return this.currentAmmo < 1;
	}

	get ammoPercent(){
		return this.currentAmmo / this.maxAmmo;
	}

	/**
	 * @virtual
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 */
	Trigger_begin(position, aim, owner){ }

	/**
	 * @virtual
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	Trigger(position, aim, owner, dt){ }

	/**
	 * @virtual
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	Update(position, aim, owner, dt){ }

	DrawReticle(camera, position, aim){
		this.drawDefaultReticle(camera, position, aim);
	}

	/**
	 * @param {Camera} camera 
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 */
	drawDefaultReticle(camera, position, aim, color = "#0F0"){
		
		camera.FillRect(new Rect(position.Minus(new Vec2(0.5)), Vec2.one), color);
	}
}

class Cannon extends Weapon{

	constructor(){
		super();

		this.fireDelay = 0.35;
		this.cooldown = 0;

		this.maxAmmo = Number.POSITIVE_INFINITY;
		this.currentAmmo = this.maxAmmo;
		this.ammoPack = 10;

		this.kick = 34600;
		this.projectileSpeed = 850;
	}

	get ammoPercent(){
		return 1;
	}

	Trigger_begin(position, aim, owner){

		if(this.cooldown <= 0){
			this.fire(position, aim, owner, 0.03);
		}
	}

	/**
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	fire(position, aim, owner, dt){

		if(this.outOfAmmo){
			return;
		}

		let proj = new CannonShot();
		proj.FireFrom(position, aim, this.projectileSpeed);
		proj.mass = 50;
		proj.damage = 12;
		proj.rotation = aim;
		proj.team = owner.team;
		
		owner.container.QueueSimpleSound(Camera.sfx_CannonFire, owner.position);
		proj.hitSFX = Camera.sfx_CannonHit;

		owner.ApplyImpulse(Vec2.FromDirection(aim, -this.kick))

		owner.container.AddGameObject(proj);
		proj.Update(dt);
		proj.velocity = proj.velocity.Plus(owner.velocity);

		this.cooldown += this.fireDelay;
	}

	Update(position, aim, owner, dt){

		if(this.cooldown > 0){
			this.cooldown -= dt;
		}
		else if(this.cooldown < 0){
			this.cooldown = 0;
		}
	}

}

class MassEjector extends Weapon{

	constructor(){
		super();

		this.fireDelay = 0.4;
		this.cooldown = 0;

		this.maxAmmo = Number.POSITIVE_INFINITY;

		this.burstSequence = 0;
		this.burstCount = 2;
		this.burstDelay = 0.1;

		this.kick = 5500;
		this.projectileSpeed = 175;
	}

	get ammoPercent(){
		return 1;
	}

	Trigger(position, aim, owner, dt){

		if(this.cooldown <= 0){
			this.fire(position, aim, owner, dt);
		}
	}

	/**
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	fire(position, aim, owner, dt){

		let projCount = 3;
		for(let i = projCount; i > 0; i--){

			let spread = (Math.random() - 0.5) * 0.2;
			let spos = Vec2.RandomDirection(2.5);
			let spd = (this.projectileSpeed) * (i / projCount)  + (Math.random() * 150);

			let proj = new Shrapnel();
			proj.FireFrom(position.Plus(spos), aim + spread, spd);
			proj.strokeStyle = "#0F0";
			proj.mass = 5;
			proj.damage = 3;
			proj.rotation = aim + spread;
			proj.team = owner.team;
			proj.hitSFX = Camera.sfx_PulseLaserHit;

			owner.container.AddGameObject(proj);
			proj.Update(dt);
			proj.velocity = proj.velocity.Plus(owner.velocity);
		}
		
		owner.container.QueueSimpleSound(Camera.sfx_PulseLaserFire, owner.position);
		owner.ApplyImpulse(Vec2.FromDirection(aim, -this.kick))
		
		this.burstSequence++;
		if(this.burstSequence >= this.burstCount){
			this.burstSequence = 0;
		}

		if(this.burstSequence > 0)
			this.cooldown += this.burstDelay;
		else
			this.cooldown += this.fireDelay;
	}

	Update(position, aim, owner, dt){

		if(this.cooldown > 0){
			this.cooldown -= dt;
		}
		else if(this.cooldown < 0){
			this.cooldown = 0;
		}
		
		if(this.burstSequence > 0){
			if(this.cooldown <= 0){
				this.fire(position, aim, owner, dt);
			}
		}
	}

}

class PulseLaser extends Weapon{

	constructor(){
		super();

		this.fireDelay = 0.425;
		this.cooldown = 0;

		this.maxAmmo = Number.POSITIVE_INFINITY;

		this.burstSequence = 0;
		this.burstCount = 4;
		this.burstDelay = 0.065;

		this.kick = 1275;
		this.projectileSpeed = 580;
	}

	get ammoPercent(){
		return 1;
	}

	Trigger(position, aim, owner, dt){

		if(this.cooldown <= 0){
			this.fire(position, aim, owner, dt);
		}
	}

	/**
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	fire(position, aim, owner, dt){

		let spread = (Math.random() - 0.5) * 0.075;
		let spos = Vec2.RandomDirection(1.5);

		let proj = new Bullet();
		proj.FireFrom(position.Plus(spos), aim + spread, this.projectileSpeed);
		proj.strokeStyle = "#0F0";
		proj.mass = 5;
		proj.size = 3;
		proj.lineWidth = 1.5;
		proj.damage = 2;
		proj.rotation = aim + spread;
		proj.team = owner.team;

		owner.container.QueueSimpleSound(Camera.sfx_PulseLaserFire, owner.position);
		proj.hitSFX = Camera.sfx_PulseLaserHit;
		
		owner.ApplyImpulse(Vec2.FromDirection(aim, -this.kick));
		
		owner.container.AddGameObject(proj);
		proj.Update(dt);
		proj.velocity = proj.velocity.Plus(owner.velocity);
		
		this.burstSequence++;
		if(this.burstSequence >= this.burstCount){
			this.burstSequence = 0;
		}

		if(this.burstSequence > 0)
			this.cooldown += this.burstDelay;
		else
			this.cooldown += this.fireDelay;
	}

	Update(position, aim, owner, dt){

		if(this.cooldown > 0){
			this.cooldown -= dt;
		}
		else if(this.cooldown < 0){
			this.cooldown = 0;
		}
		
		if(this.burstSequence > 0){
			if(this.cooldown <= 0){
				this.fire(position, aim, owner, dt);
			}
		}
	}

}

class BeamLaser extends Weapon{

	constructor(){
		super();

		this.maxAmmo = Number.POSITIVE_INFINITY;
		this.currentAmmo = this.maxAmmo;

		this.kick = 1550;
		this.range = 400;
		this.radius = 2;
		this.damage = 25; // damage per second, NOT per hit
		this.force = 50000; // force per second, NOT per hit
		this.color = new Color(186, 100, 255, 1);
		this.repeatingSound = null;

		this.chargeTime = 0.25;
		this.charge = 0;

		this._chargeEffect = null;
		this._didTrigger = false;
	}

	/**
	 * @virtual
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	Update(position, aim, owner, dt){ 

		// if the repeating sound exists, reposition it to match the position of the weapon, and 
		// remove it if it's been destroyed
		if(this.repeatingSound != null){
			this.repeatingSound.position = owner.position;
			if(this.repeatingSound.isRemoved)
				this.repeatingSound = null;
		}

		// if the weapon wasn't triggered by it's owner this last frame
		if(!this._didTrigger){

			// reset the weapon charge
			this.charge = 0;

			// stop looping and remove the reference to the looping sound if necessary
			if(this.repeatingSound != null){
				this.repeatingSound.loopAudio = false;
			}

			// remove the charge effect if necessary
			if(this._chargeEffect != null){
				this.removeChargeEffect();
			}
		}

		// set the _didTrigger flag to false (this statement gets invalidated and the flag is 
		// set to true in the Trigger function if it's called)
		this._didTrigger = false;
	}
	
	/**
	 * @override
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	Trigger(position, aim, owner, dt){ 

		// set the flag to notify the weapon that it has been triggered this frame
		this._didTrigger = true;

		// if the weapon is not finished charging / spooling up, handle the charging
		if(this.charge < this.chargeTime)
			this.handleCharging(position, owner, dt);

		// if the weapon has been spooling for long enough and the charge time has elapsed, fire 
		// the weapon
		else 
			this.Fire(position, aim, owner, dt);

	}

	/**
	 * handle's the weapon spooling up when it is triggered before it actually fires
	 * @param {Vec2} position the position of the weapon's barrel
	 * @param {Character} owner the entity firing the weapon
	 * @param {Number} dt delta time
	 */
	handleCharging(position, owner, dt){

		// increment the charge counter
		this.charge += dt;

		// create the charge effect if it does not yet exist
		if(this._chargeEffect == null){
			this.createChargeEffect(owner, position);
		}

		// set the position and size of the charge effect
		this._chargeEffect.position = position.clone;
		this._chargeEffect.radius = (this.charge / this.chargeTime) * 3 + 2;
	}

	/**
	 * creates an instance of the charge effect for the weapon to show that it's beginning to
	 * get ready to fire
	 * @param {Character} owner 
	 * @param {Vec2} position 
	 */
	createChargeEffect(owner, position){

		// create the charge effect instance
		this._chargeEffect = new CircleEffect();
		this._chargeEffect.color = this.color;
		this._chargeEffect.position = position.clone;

		// add the effect to the simulation
		owner.container.AddGameObject(this._chargeEffect);
	}

	/** removes the charge effect from the game */
	removeChargeEffect(){

		// remove the charge effect from the simulation and remove the reference to it
		this._chargeEffect.Remove();
		this._chargeEffect = null;
	}

	/** fire the beam projectile from the weapon 
	 * @override
	 * @param {Vec2} position 
	 * @param {Number} aim 
	 * @param {Character} owner 
	 * @param {Number} dt
	 */
	Fire(position, aim, owner, dt){

		// create the beam projectile
		let beam = new Beam();

		// set the appropriate projectile properties
		beam.damagePerSecond = this.damage;
		beam.forcePerSecond = this.force;
		beam.size = 1;
		beam.color = this.color;
		beam.team = owner.team;
		
		// set the beam's initial velocity and add the owner's velocity
		beam.FireFrom(position, aim, 7000);
		beam.velocity = beam.velocity.Plus(owner.velocity);

		// add the beam projectile to the simulation and update it instantly
		owner.container.AddGameObject(beam);
		beam.Update(dt);
		
		// apply the very slight amount of recoil to the owner
		owner.ApplyForce(Vec2.FromDirection(aim, -this.kick), dt);

		// remove the charge effect if necessary
		if(this._chargeEffect != null){

			// remove the charge effect from simulation
			this.removeChargeEffect();

			// create a sound effect that loops as long as the player is firing
			this.repeatingSound = new SoundEffectInstance(Camera.sfx_BeamLaser, 1, owner.position);
			this.repeatingSound.loopAudio = true;
			this.repeatingSound.loopEndTime = 0.0075;
			owner.container.AddGameObject(this.repeatingSound);
		}

		// reset the time of the sound effect so that it starts at the beginning again, this should
		// happen each frame for now, but eventually we will revamp the sound system to allow for 
		// more robust looping sound effects (see relevant issue on gitlab issue board)
		//if(this.repeatingSound != null)
		//	this.repeatingSound.audio.currentTime = 0;
	}
}