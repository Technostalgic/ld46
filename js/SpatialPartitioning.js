///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech
/// 	twitter @technostalgicGM
///

class SpacialPartitioning{

	constructor(){

		/** @type {Array<Array<GameObject>>} the array that holds references to the partitions */
		this.partitions = [];

		/** @type {Array<Boolean>} */
		this._objsIterated = [];
		/** @type {Array<Boolean>} */
		this._objsToIterate = [];

		/** @type {Number} the width / height of each partition */
		this.partitionSize = 100;
		
		/** 
		 * @type {Number} 
		 * the offset of the partition's indices, since we can't have arrays with 
		 * negative indices we must offset them to prevent the likeliness of going
		 * into partitions with negative indices
		 */
		this.partitionOffset = 500;
	}

	/**
	 * @private @type {Boolean} returns a bool indicating whether or not a partition exists at the 
	 * specified index
	 * @param {Number} x The x-index of the partition to check
	 * @param {Number} y The y-index of the partition to check
	 */
	partitionExists(x, y){
		
		// if the partition doesn't exist, return false
		if(this.partitions[x] == null){
			return false;
		}
		if(this.partitions[x][y] == null){
			return false;
		}

		// otherwise return true
		return true;
	}

	/**
	 * @private creates an empty partition at the specified index if one does not already exist
	 * @param {Number} x the integer index x component
	 * @param {Number} y the integer index y component
	 */
	createPartition(x, y){
		
		// if the x component arra has no value at the specified x index, create a new array to
		// hold the new partition
		if(this.partitions[x] == null)
			this.partitions[x] = [];
		
		// if a partition does not already exist at the specified index, create an empty one
		if(this.partitions[x][y] == null)
			this.partitions[x][y] = [];
	}

	/**
	 * cleans up (removes) all the GameObjects that are outside of the specified range
	 * @param {Rect} activeRange the index range where objects should not be cleaned up
	 */
	PerformCleanup(activeRange){

		// define an array variable to hold the new partitions
		let newParts = [];

		// iterate through each integer within the 2D index range
		for(let x = activeRange.left; x <= activeRange.right; x++){
			for(let y = activeRange.top; y <= activeRange.bottom; y++){

				// skip the iteration if no partition already exists at the current index
				if(!this.partitionExists(x, y))
					continue;

				// if no array exists at the x-index in the new partition array, create one
				if(newParts[x] == null)
					newParts[x] = [];

				// add the partition to the new partition array
				newParts[x][y] = this.PartitionAt(x, y);
			}
		}

		// apply the new partitions
		this.partitions = newParts;
	}

	/**
	 * Clears all empty partitions from the 2d partitions array so that they don't take up memory
	 */
	ClearEmptyPartitions(){

		// define a new array that will not store any empty partitions
		let r = [];

		// iterate through each nested array / partition in the 2D partitions array
		for(let x in this.partitions){
			for (let y in this.partitions[x]){

				// if there is a non-empty partition at the index
				if(this.partitions[x][y].length > 0){

					// ensure the new array has an array at the current x-index
					if(r[x] == null)
						r[x] = [];
					
					// store the partition in the new array
					r[x][y] = this.partitions[x][y];
				}
			}

			// force the browser to immediately free the memory of the current x-index array 
			// instead of waiting for it to freed by the garbage collector which could cause lag
			// spikes to occur when the GC runs, if this function is called frequently
			this.partitions[x].length = 0;
		}

		// ** same as above but with the base array instead of the x-index arrays
		this.partitions.length = 0;

		// replace the partitions variable with the new 2d array which now only contains non-empty
		// partitions
		this.partitions = r;
	}

	/**
	 * @deprecated @type {Number} counts the total number of objects that exist within the 
	 * partitions. Somewhat inefficient, should only be used for debugging
	 */
	FindTotalObjectCount(){

		// create an array to store the uids of the objects in the partition
		let uids = [];

		// iterate through each row/column
		for(let x in this.partitions){

			// if the partition row does not exist, skip iteration
			if(this.partitions[x] == null) continue;
			
			// iterate through the row
			for(let y in this.partitions[x]){

				// if partition column does not exist, skip iteration
				if(this.partitions[x][y] == null) continue;

				// iterate through each object in the partition
				for(let obj in this.partitions[x][y]){
					
					// store a flag at the specified object's uid
					uids[this.partitions[x][y][obj].uid] = true;
				}
			}
		}

		// count the amount of uids in the array. NOTE: this is different from array.length because
		// the array stores the flag at the uid index of the array, it's not just pushed to the
		// last array index. This is to prevent duplicates of the same object being added
		let i = 0;
		for(let fl in uids)
			i += uids[fl] ? 1 : 0;
		
		// return the count
		return i;
	}

	/**
	 * @type {Array<GameObject>} returns the partition at the specified index
	 * @param {Number} x the x-component of the (2d) index
	 * @param {Number} y the y-component of the (2d) index
	 */
	PartitionAt(x, y){

		// if the partition doesn't exist, create an empty one
		if(!this.partitionExists(x, y))
			this.createPartition(x, y);

		// otherwise, return the partition at the specified index 2d
		return this.partitions[x][y];
	}

	/**
	 * @deprecated @type {Vec2} returns the partition index that contains the specified 
	 * object - WARNING: very inefficient and should only be used for debugging.
	 * @param {GameObject} obj 
	 */
	PartitionOf(obj){
		for(let x in this.partitions){
			for(let y in this.partitions[x]){
				let index = this.partitions[x][y].indexOf(obj);
				if(index >= 0)
					return new Vec2(x, y);
			}
		}
		return null;
	}

	/**
	 * iterates through all the partitions that exist at the specified index range, and calls the 
	 * specified function, passing the partition as a parameter to it
	 * @param {Rect} indexRange2D the 2D range of partition indices that is iterated through, can 
	 * 	be found from world coordinates using the 'this.WorldRectToPartitionRange()' function
	 * @param {Function(Array<GameObject>)} func the function that's called for partition that's 
	 * 	iterated through
	 * @param {Boolean} forceCreate whether or not a partition is created if it doesn't exist at an
	 * 	index that is being iterated through. 
	 */
	IterateThrough(indexRange2D, func, forceCreate = false){
		
		// iterate through each integer within the 2D index range
		for(let x = indexRange2D.left; x <= indexRange2D.right; x++){
			for(let y = indexRange2D.top; y <= indexRange2D.bottom; y++){

				// skip the iteration if it's specified to do so when no partition already exists at 
				// the current index
				if(!forceCreate && !this.partitionExists(x, y))
					continue;

				// otherwise call the specified function and pass the partition at the 
				// current index
				func(this.PartitionAt(x, y));
			}
		}
	}

	/**
	 * iterates through all the game objects that exist in the partitions at the specified index 
	 * range, and calls the specified function, passing the game object as a parameter to it
	 * @param {Rect} indexRange2D the 2D range of partition indices that is iterated through, can 
	 * 	be found from world coordinates using the 'this.WorldRectToPartitionRange()' function
	 * @param {Function(GameObject)} func the function that's called for each game object in each
	 * 	partition that's iterated through
	 */
	IterateThroughObjects(indexRange2D, func){

		// define a set to store objects that have already been iterated through
		let objsIterated = [];
		let objsToIterate = [];

		// iterate through each integer inside the index range
		for(let x = indexRange2D.left; x <= indexRange2D.right; x++){
			for(let y = indexRange2D.top; y <= indexRange2D.bottom; y++){

				// skip the iteration if no partition exists at the current index
				if(!this.partitionExists(x, y))
					continue;

				// find the partition at the index that is currently being iterated through
				let part = this.PartitionAt(x, y);
				
				// iterate through each object in the partition
				for(let i = part.length - 1; i >= 0; i--){

					// implemented to catch a rarely occuring bug where sometimes part[i] is 
					// undefined for unknown reasons
					try{
						// if the object has already been iterated through
						if(objsIterated[part[i].uid])
							continue;
					}

					// catch for weird bug that sometimes happens
					catch(e){
						console.error(e);
						console.log(this);
						console.log(part);
						console.log(i);
					}

					// creates a <key, value> pair that has a key of the object's uid, and a 
					// value of 'true'. Then adds that pair to the 'this._objsIterated' set
					// BUG: for some reason 'i' sometimes gets incremented in the 'func(part[i])'
					// 	line through some javascrippt fuckery, so this line must come before it
					objsIterated[part[i].uid] = true;

					// flag the object for iteration
					objsToIterate.push(part[i]);
					//func(part[i]);
				}
			}
		}

		// iterate through the objects that are flagged
		for(let i = 0; i < objsToIterate.length; i++){

			// call the specified function with the object passed as a parameter
			func(objsToIterate[i]);
		}
	}

	/**
	 * iterates through all the game objects that exist in the partitions who overlap the specified
	 * world rect, and calls the specified function, passing the game object as a parameter to it
	 * @param {Rect} worldRect 
	 * @param {Function(GameObject)} func 
	 */
	IterateThroughWorldRectObjects(worldRect, func){
		this.IterateThroughObjects(this.WorldRectToPartitionRange(worldRect), func);
	}

	/**
	 * @type {Vec2} returns the 2d index of the spatial partition at the specified worl position
	 * @param {Vec2} worldPos 
	 */
	WorldPositionToPartitionIndex(worldPos){

		// divide the world position by the partition size and floor the result
		let index = worldPos.Times(1 / this.partitionSize).floored;

		// finally add the partition offset to get the index at the specified worl position
		index = index.Plus(new Vec2(this.partitionOffset));

		// make sure the x OR y index never goes below 0
		index.x = Math.max(0, index.x);
		index.y = Math.max(0, index.y);

		return index;
	}

	/**
	 * @type {Rect} rect with the with minimum value of the 2d partition index at the world rect's
	 * top left and a maximum value of the 2d partition index at the world rect's bottom right.
	 * Iterating through each integer coordinate (inclusive max) in the rectangle that is returned,
	 * is the same as iterating through the index of each partition that intersects with the 
	 * world rect parameter provided.
	 * @param {Rect} worldRect
	 */
	WorldRectToPartitionRange(worldRect){
		
		// define the min index as the partition at the top left of the rectangle
		let min = this.WorldPositionToPartitionIndex(worldRect.topLeft);

		// define the max index as the partition at the bottom right of the world rect
		let max = this.WorldPositionToPartitionIndex(worldRect.bottomRight);

		// return an axis aligned rectangle created from these min and max values
		return Rect.FromPoints(min, max);
	}
}