///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * stores information about a sound effect that was played in the gameObject container so it can
 * stored in a queue and later played by a camera object
 */
class SimpleSoundInstance{

	/**
	 * @param {HTMLAudioElement} audio the audio element that the sound effect consists of
	 * @param {Number} volume the base volume of the audio playback
	 * @param {Vec2} position the origin of where the sound occured in world space
	 */
	constructor(audio, origin, baseVolume = 1){

		/** @type {HTMLAudioElement} */
		this.audio = audio;

		/** @type {Number} */
		this.baseVolume = baseVolume;

		/** @type {Vec2} */
		this.origin = origin;
	}
}

/**
 * stores information about sound effects that are played back and updates them each frame
 */
class SoundEffectInstance extends GameObject{

	/**
	 * @param {HTMLAudioElement} audio the audio element that the sound effect will play
	 * @param {Number} volume the base volume of the audio clip's playback
	 * @param {Vec2} position the position of the sound effect, leave null if you do not want 
	 * 	volume to be position based
	 */
	constructor(audio, volume = 1, position = null){
		super();

		this.position = position == null ? new Vec2() : position.clone;

		/** @type {HTMLAudioElement} */
		this.audio = audio;

		this.usePosition = position != null;
		this.baseVolume = volume;
		this.startTimestamp = 0;

		this.loopAudio = false;
		this.loopEndTime = 0;
		this.loopStartTime = 0;

		this._alwaysRender = true;
	}

	onAdded(container){
		this.startTimestamp = container.timeSimulated;
		this.audio.currentTime = 0;
	}

	/** @param {Camera} camera */
	onDraw(camera){

		// handle looping if necessary
		if(this.loopAudio){
			if(this.audio.currentTime >= this.loopEndTime){
				this.audio.currentTime = this.loopStartTime;
				this.startTimestamp = this.container.timeSimulated;
			}
		}

		// determine if the sound lifetime has elapsed or not, and remove the sound effect if its
		// life has fully elapsed
		let timeElapsed = this.container.timeSimulated - this.startTimestamp;
		if(timeElapsed >= this.audio.duration){
			this.Remove();
			return;
		}

		// continue the sound, but recalculate the properties so that it changes dynamically as 
		// it's playing
		let npos = this.usePosition ? this.position : null;
		camera.ContinueSound(this.audio, this.baseVolume, npos);
	}
}