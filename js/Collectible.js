///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Collectible extends PhysObject{

	constructor(){
		super();

		/** @type {Boolean} */
		this.isCollected = false;

		/** @type {Number} */
		this.amount = 1;

		/** @type {Collider} */
		this._collider = Collider.FromShape(new Circle(this.position, 5), this);
		this._collider.isSolid = false;
		this.implements_collider = true;

		/** @type {String} */
		this.name = "Collectible";
	}

	/** @type {Sprite} */
	static get defaultIcon(){

		// generate default icon if it hasn't been generated yet
		if(Collectible._defaultIcon == null){
			Collectible._defaultIcon = Sprite.FromGraphic(
				SpriteFont.default.GenerateTextImage("x", 1, 0)
			);
		}

		return Collectible._defaultIcon;
	}

	/** @type {Collectible} */
	static get empty(){
		let r = new Collectible();
		r.amount = 0;

		return r;
	}

	/** @virtual @type {Sprite} */
	get icon(){
		if(this._icon == null)
			return Collectible.defaultIcon;
		return this._icon;
	}
	set icon(value){
		this._icon = value;
	}

	/** @type {Collider} */
	get collider(){
		return this._collider;
	}

	/** @type {Collectible} */
	get clone(){

		// create a new object of the same type and copy this object's fields
		let r = new (this.constructor)();
		r.CopyFields(this);

		// return the new instance with the same data
		return r;
	}

	/** @type {Boolean} */
	get isEmpty(){
		return this.amount <= 0;
	}

	/** 
	 * @virtual copies all the fields from another object onto this object. NOTE this must be 
	 * overriden in all classes who inherit from 'Collictible' in order for this.clone to work
	 * properly
	 * @param {Collectible} collectible the object to copy the field data from
	 */
	CopyFields(collectible){

		/** @type {Vec2} */
		this.position = collectible.position.clone;
		this.isCollected = collectible.isCollected;
		this.amount = collectible.amount;
		this.name = collectible.name;
		this._icon = collectible._icon;
	}

	/**
	 * checks collisions with all the game objects that the item could possibly be colliding with
	 */
	CheckCollisions(){

		// iterate through each overlapping collider
		let cols = this.collider.FindOverlappingColliders();
		for(let i = cols.length - 1; i >= 0; i--){

			// get picked up by a colliding object if it has a collectible container
			if(cols[i].parentObject.implements_collectibleContainer)
				this.OnPickup(cols[i].parentObject);
		}
	}

	/**
	 * @virtual Called when the collectible is picked up by a container object
	 * @param {GameObject} obj 
	 */
	OnPickup(obj){

		/** @type {CollectibleContainer} */
		let colCont = obj.collectibleContainer;
		let vel = obj.velocity || this.velocity;

		colCont.Add(this);
		this.container.AddGameObject(Effect.SmallFlash(this.position, vel, false));
		
		this.Remove();
	}

	Update(dt){
		super.Update(dt);
		this.collider.CenterOnParent();
		this.CheckCollisions();
	}
}

/**
 * resources are collectible types that are non-unique, such as currency, scrap metal, etc
 */
class Resource extends Collectible{ 

	constructor(){
		super();

		this.name = "Resource";
		this.key = "resourceKey";
	}

	/** @type {String} the resource key for the gold resource type */
	static get key_gold(){ return "gold"; }

	/** @type {String} the resource key for the scrap metal resource type */
	static get key_scrapMetal(){ return "scrapMetal"; }
	
	/**
	 * @type {Resource} Returns a scrap metal resource of he specified amount
	 * @param {Number} amount 
	 */
	static Gold(amount = 1){
		let r = new Resource();
		r.name = "Gold";
		r.key = Resource.key_scrapMetal;
		r.amount = amount;
		return r;
	}

	/**
	 * @type {Resource} Returns a scrap metal resource of he specified amount
	 * @param {Number} amount 
	 */
	static ScrapMetal(amount = 1){
		let r = new Resource();
		r.name = "Scrap Metal";
		r.key = Resource.key_scrapMetal;
		r.amount = amount;
		return r;
	}

	/** 
	 * @override
	 * @param {Resource} res
	 */
	CopyFields(res){
		super.CopyFields(res);
		this.key = res.key;
	}	

	/**
	 * @override 
	 * @param {Camera} cam
	*/
	onDraw(cam){
		this.collider.shape.DrawOutline(cam, Color.Green, 1);
	}
}

/**
 * @abstract an item is any collectible that is not a resource, such as a weapon, rare drop, or something
 * that can be managed in the player's inventory
 */
class Item extends Collectible{

	constructor(){
		super();

		this.name = "Item";

		/** @type {Number} */
		this._oid = -1;
	}

	static get nextOID(){

		// ensure that the current oid field exists
		if(Item._curOID == null){
			Item._curOID = 0;
		}

		// return the current oid and then increment it
		return Item._curOID++;
	}

	/** @type {Number} */
	get oid(){
		return this._oid;
	}

	/** 
	 * @override 
	 * @param {Item} itm
	 */
	CopyFields(itm){
		super.CopyFields(itm);
		this._oid = itm._oid;
	}
}