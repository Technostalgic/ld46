///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class GameObject{

	constructor(){

		/**@type {Number} */
		this._uid = GameObject.nextID;
		
		/**@type {Boolean} */
		this.implements_collider = false;
		/**@type {Boolean} */
		this.implements_controller = false;
		/**@type {Boolean} */
		this.implements_movementDynamics = false;
		/**@type {Boolean} */
		this.implements_rotationalDynamics = false;
		/**@type {Boolean} */
		this.implements_collectibleContainer = false;
		/**@type {Boolean} */
		this.implements_damageable = false;
		/**@type {Boolean} */
		this.implements_interaction = false;

		/**@type {boolean} */
		this._alwaysUpdate = false;
		/**@type {boolean} */
		this._alwaysRender = false;

		/**@type {Rect} */
		this.partitionRange = new Rect();
		
		/**@type {GameObjectContainer} */
		this.container = null;
		/**@type {Boolean} */
		this.isRemoved = false;

		/**@type {Vec2} */
		this.position = new Vec2();

		/**@type {Boolean} */
		this.isThreat = false;
		/**@type {Boolean} */
		this.isFriendly = false;
		/**@type {Boolean} */
		this.isInteractable = false;

		/**@type {Number} */
		this.renderOrder = 10;

		/** @type {Number} */
		this._lastUpdateStep = -1;
		/** @type {Number} */
		this._lastDrawStep = -1;

		/** @type {Boolean} */
		this.needsCleanup = true;
	}

	/**@type {Number}*/
	static get nextID(){

		// store the current ID and increment the next one
		let curID = GameObject._currentID || 0;
		GameObject._currentID = curID + 1;

		// return the non-incremented value
		return curID;
	}

	get uid(){
		return this._uid;
	}

	/** @type {Boolean} whether or not the game object is subscribed to always update */
	get alwaysUpdate(){
		return this._alwaysUpdate;
	}

	/** @type {Boolean} whether or not the game object is subscribed to always update */
	get alwaysRender(){
		return this._alwaysRender;
	}

	/**
	 * @virtual @type {Rect} returns the bounds of the object, is used to calculate which
	 * spatial partitions this game object belongs to
	 */
	get bounds(){
		return new Rect(this.position.clone, Vec2.zero);
	}

	UpdateBounds(){}

	/**
	 * updates the spatial partitions that this object belongs to, removes it from the old ones and
	 * adds it to the new ones
	 */
	updateSpatialPartitionRange(){

		// create flag for whether or not the object's partitions should actually be refreshed
		let doPartUpdate = false;

		// create variable to store the new collider partition range outside the scope of the
		// conditional statement
		let newColRange = null;

		// if there is a collider, calculate it's new partition range and see if it has changed
		if(this.implements_collider){
			
			// calculate new collider partition range
			newColRange = 
				this.container.collisionPartitions.WorldRectToPartitionRange(
					this.collider.boundingBox
				);

			// set flag to change partitions if the new range is different from the old one
			if(!newColRange.IsEqual(this.collider.partitionRange)){
				doPartUpdate = true;
			}
		}

		// calculate the new partition range that contains all the partitions which the object 
		// should belong to
		let newRange = this.container.objectPartitions.WorldRectToPartitionRange(this.bounds);

		// set flag to change partitions if the new range is different from the old one
		if(!newRange.IsEqual(this.partitionRange))
			doPartUpdate = true;

		// if none of the partition ranges that the object belongs to have changed, do nothing
		if(!doPartUpdate)
			return;

		// iterate through all the partitions this currently exists in and remove it from them
		this.RemoveFromPartitions();

		// if it's destroyed, don't add it to the new partitions
		if(this.isRemoved)
			return;

		// if there's a collider, update it's new partition range
		if(this.implements_collider){
			this.collider.partitionRange = newColRange;
		}

		// update the partition range to reflect the object's current bounds
		this.partitionRange = newRange;

		// iterate through the new partitions this object now exists in and add it to them
		this.AddToPartitions(newRange);
	}

	/**
	 * adds the object to the partitions in the specified range
	 * @param {Rect} range 2d index range that specifies which partitions this will be added to
	 */
	AddToPartitions(range){

		// define this to have a reference to it inside the anonymous function
		let ths = this;

		/**
		 * @private define a function that adds this object to the new partitions that it now exists in
		 * @param {Array<GameObject>} part 
		 */
		let func = function(part){
			part.push(ths);
		}

		// iterate through the new partitions this object now exists in and add it to them
		this.container.objectPartitions.IterateThrough(this.partitionRange, func, true);

		// add to collision partitions as well if applicable
		if(this.implements_collider){
			
			/**
			 * @private define a function that adds this object to the new partitions that it now exists in
			 * @param {Array<GameObject>} part 
			 */
			let func2 = function(part){
				part.push(ths._collider);
			}
			
			// iterate through the new partitions the collider now exists in and add it to them
			this.container.collisionPartitions.IterateThrough(this.collider.partitionRange, func2, true);
		}
	}

	/**
	 * Removes the gameobject to all the partitions it belongs to, effectively removing it from
	 * the game
	 */
	RemoveFromPartitions(){

		// define this to have a reference to it inside the anonymous function
		let ths = this;

		/**
		 * @private define a function to remove this from the partitions it exists in
		 * (only used in "updateSpatialPartitionRange()")
		 * @param {Array<GameObject>} part 
		 */
		let func = function(part){

			// find the index of this in the partition
			let ind = part.indexOf(ths);

			// remove the object from the partition
			if(ind >= 0)
				part.splice(ind, 1);
		}

		// iterate through all the partitions this currently exists in and remove it from them
		this.container.objectPartitions.IterateThrough(this.partitionRange, func);

		// remove from collision partitions as well if applicable
		if(this.implements_collider){
			
			/**
			 * @private define a function that adds this object to the new partitions that it now exists in
			 * @param {Array<GameObject>} part 
			 */
			let func2 = function(part){

				// find the index of this in the partition
				let ind = part.indexOf(ths._collider);
	
				// remove the collider from the partition
				if(ind >= 0)
					part.splice(ind, 1);
			}

			// iterate through all the partitions the collider currently exists in and remove it
			this.container.collisionPartitions.IterateThrough(this.collider.partitionRange, func2);
		}

		// set partitionrange to empty
		this.partitionRange = new Rect();
	}

	Remove(){
		this.isRemoved = true;
		this.RemoveFromPartitions();
		this.onRemoved();
	}

	Update(dt){

		if(this.isRemoved)
			return;

		if(this.container != null){
			
			// set the last update to the current game state time
			this._lastUpdateStep = this.container.parentState.playTime;
		}

		this.onUpdate(dt);
	}

	Draw(camera){
		
		if(this.isRemoved)
			return;
		
		if(this.container != null){
			
			// set the last draw to the current game state time
			this._lastDrawStep = this.container.playTime;
		}

		this.onDraw(camera);
	}

	/**
	 * Used for debugging, displays which partition this object belongs to
	 * @param {Camera} camera 
	 */
	DrawPartition(camera){

		// if the partition image does not exist
		if(this._partImg == null){

			// store the vector that the image is based off of so you can compare it
			this._partVec = this.partitionRange.topLeft.clone;

			// generate an image to represent the current partition
			this._partImg = SpriteFont.default.GenerateTextImage(
				this.partitionRange.left + "," + this.partitionRange.top, 
				1, -1);
		}

		// otherwise, if the partition vector is different from the vector that is being rendered
		else if(this._partVec.x != this.partitionRange.left || this._partVec.y != this.partitionRange.top){
			
			// store the vector that the image is based off of so you can compare it
			this._partVec = this.partitionRange.topLeft.clone;

			// generate an image to represent the current partition
			this._partImg = SpriteFont.default.GenerateTextImage(
				this.partitionRange.left + "," + this.partitionRange.top, 
				1, -1);
		}

		// render the text
		camera.DrawImage(this._partImg, this.position)
	}
	
	/** @virtual @param {GameObjectContainer} container */
	onAdded(container){ }

	/** @virtual */
	onRemoved(){ }

	/**
	 * @virtual
	 * @param {Number} dt 
	 */
	onUpdate(dt){ }
	
	/**
	 * @virtual
	 * @param {Camera} camera 
	 */
	onDraw(camera){ }

	/**
	 * @virtual
	 */
	onDestroy(){ }
}

/**
 * @abstract
 */
class PhysObject extends GameObject{

	constructor(){
		super();

		/** @type {Vec2} in newtons */
		this._netForce = new Vec2();

		/** @type {Boolean} */
		this.implements_movementDynamics = true;

		/** @type {Vec2} */
		this.velocity = new Vec2();

		/** @type {Number} in KG */
		this.mass = 1;
	}

	resolveForces(){

		let acceleration = this._netForce.Times(1 / this.mass);
		this.velocity.Translate(acceleration);
		
		this._netForce.x = 0;
		this._netForce.y = 0;
	}

	physicsUpdate(dt){

		this.resolveForces();
		this.position.Translate(this.velocity.Times(dt));

		// update the spatial partition this belongs to but only if its actually inside one
		if(this.container != null)
			this.updateSpatialPartitionRange();
	}

	/**
	 * @override
	 * @param {Number} dt 
	 */
	Update(dt){
		if(this.isRemoved)
			return;
		this.physicsUpdate(dt);
		super.Update(dt);
	}

	/**
	 * @param {Vec2} force
	 * @param {Number} dt 
	 */
	ApplyForce(force, dt){
		this._netForce.Translate(force.Times(dt));
	}

	/**
	 * @param {Vec2} force 
	 */
	ApplyImpulse(force){
		this._netForce.Translate(force);
	}
}

class Debris extends PhysObject{

	constructor(){
		super();

		this.frame = Math.floor(Math.random() * Debris.spriteSheet_debris.sprites.length);

		this.rotation = Math.random() * Math.PI;
		this.angularVelocity = Math.random() - 0.5;

		this.isFlipped = Math.random() >= 0.5;

		this.life = 0;
		if(this.frame >= 12)
			this.life = Math.random() * 0.5;
	}

	/** @type {SpriteSheet} */
	static get spriteSheet_debris(){

		if(Debris.spriteSheetAsset_debris == null)
			Debris.spriteSheetAsset_debris = AssetPack.GetAsset("vanilla","spriteSheets","debris");
		return Debris.spriteSheetAsset_debris.globalInstance;
	}

	Update(dt){

		super.Update(dt);
		this.rotation += this.angularVelocity * dt;

		if(this.life > 0){
			this.life -= dt;
			if(this.life <= 0){
				
				this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.25)));
				this.container.QueueSimpleSound(Camera.sfx_DebrisExplode, this.position);
				this.Remove();
			}
		}
	}

	/** @param {Camera} camera */
	Draw(camera){

		let viewRect = camera.viewRect;
		viewRect.AddPadding(5, 5);

		if(!camera.viewRect.OverlapsPoint(this.position)){
			this.Remove();
			return;
		}

		camera.DrawSpriteFromSheet(Debris.spriteSheet_debris, this.frame, this.position, this.rotation, this.isFlipped);
	}
}

class SolidObject extends PhysObject{

	constructor(){
		super();

		this.implements_collider = true;

		/**@type {Collider} */
		this._collider = null;

		this.team = null;
	}

	/** @type {Collider} */
	get collider(){
		return this._collider;
	}

	/** @type {Rect} */
	get bounds(){
		return this._collider.boundingBox;
	}

	Draw(camera){
		super.Draw(camera);
		if(Input.devTools){
			this.collider.shape.DrawOutline(camera, Color.White, 0.5);
		}
	}

	/** 
	 * @override forces the hitbox to be re-centered since that's what the bounds depend on 
	 * for SolidObjects 
	 */
	UpdateBounds(){
		this._collider.CenterOnParent();
	}

	physicsUpdate(dt){
		
		this.resolveForces();
		this.position.Translate(this.velocity.Times(dt));
		this._collider.CenterOnParent();

		// update the spatial partition this belongs to but only if its actually inside one
		if(this.container != null)
			this.updateSpatialPartitionRange();
	}
}

/**
 * @abstract
 */
class Character extends SolidObject{
	
	constructor(){
		super();

		this.implements_controller = true;
		this.implements_damageable = true;

		/** @type {Rect} */
		this._collider = null;

		this.maxHealth = 10;
		this.health = 10;
	}

	get healthPercent(){
		return this.health / this.maxHealth;
	}

	/**
	 * @type {Boolean} indicates whether or not the two specified teams are hostile towards 
	 * each other
	 * @param {Number} t1 
	 * @param {Number} t2 
	 */
	static AreTeamsHostile(t1, t2){
		if(t1 == null || t2 == null)
			return false;
		if(t1 < 0 || t2 < 0)
			return false;
		return Math.floor(t1) != Math.floor(t2);
	}

	/**
	 * @type {Boolean} indicates whether or not the two specified teams are allied with each other
	 * @param {Number} t1 
	 * @param {Number} t2 
	 */
	static AreTeamsAllied(t1, t2){
		if(t1 == null || t2 == null)
			return false;
		if(t1 < 0 || t2 < 0)
			return false;
		return Math.floor(t1) == Math.floor(t2);
	}

	/**
	 * @type {Boolean} indicates whether or not the two specified teams are nuetral towards
	 * each other
	 * @param {Number} t1 
	 * @param {Number} t2 
	 */
	static AreTeamsNuetral(t1, t2){
		if(t1 == null || t2 == null)
			return false;
		return t1 < 0 || t2 < 0
	}

	/** 
	 * @abstract
	 * @param {Number} damage
	 * @param {Vec2} impulse
	 * @param {Vec2} position
	 */
	Damage(damage, impulse, position){

		this.ApplyImpulse(impulse);
		this.health -= damage;

		if(this.health <= 0){
			this.Remove();
		}
	}
}

class StarField extends GameObject{

	/** 
	 * @param {Vec2} size 
	 */
	constructor(size, density){
		super();

		/**@type {HTMLCanvasElement} */
		this.canvas = document.createElement("Canvas");
		this.canvas.width = size;
		this.canvas.height = size;

		this._alwaysRender = true;

		/**@type {CanvasRenderingContext2D} */
		this.context = this.canvas.getContext("2d");
		
		this.fillArea(density);
		
		this.needsCleanup = false;
		this.renderOrder = 0;
	}

	/**
	 * @param {Rect} area 
	 * @param {Number} density 
	 */
	fillArea(density = .001){

		let area = new Rect(Vec2.zero, new Vec2(this.canvas.width, this.canvas.height));
		let hex = ['2','3','4','5','6','7','8','9','A','B','C', 'D', 'E', 'F'];

		let starCount= area.area * density;
		for(let i = starCount; i >= 1; i--){

			let starPos = new Vec2(Math.random() * area.width, Math.random() * area.height);
			starPos = starPos.floored;

			let h = Math.floor(Math.random() * hex.length);
			let c = hex[h];

			this.context.fillStyle = "#" + c + c + c;
			this.context.fillRect(starPos.x, starPos.y, 1, 1);
		}
	}

	/**
	 * @param {Camera} camera
	 */
	onDraw(camera){

		let view = camera.viewRect;
		let size = new Vec2(this.canvas.width, this.canvas.height);
		let tl = this.position.Plus(size.Times(-0.5));

		let minX = Math.floor((view.left - tl.x) / size.x);
		let maxX = Math.ceil((view.right - tl.x) / size.x);
		let minY = Math.floor((view.top - tl.y) / size.y);
		let maxY = Math.ceil((view.bottom - tl.y) / size.y);

		for(let x = minX; x < maxX; x++){
			for(let y = minY; y < maxY; y++){
				
				let tpos = new Vec2(x * size.x, y * size.y);
				camera.DrawImage(this.canvas, tpos);
			}
		}
	}
}