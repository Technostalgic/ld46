///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * handles looping and time keeping
 */
class Game {

	/**
	 * creates a game instance
	 * @param {HTMLCanvasElement} canvas 
	 */
	constructor(canvas){

		if(Game.instance != null){
			throw "There is already a game singleton instance that must be ended before a new one is created";
		}
		/**@type {Game} */
		Game._instance = this;

		/**@type {HTMLCanvasElement}*/
		this.canvas = canvas;
		/**@type {CanvasRenderingContext2D} */
		this.context = canvas.getContext("2d");
		this.context.imageSmoothingEnabled = false;

		/**@type {GameState} */
		this.state = new GameState();

		/**@type {Number} */
		this.masterTimescale = 1;
		/**@type {Number} */
		this.gameplayTimescale = 1;
		/**@type {Number} */
		this.uiTimescale = 1;

		/**@type {Boolean} */
		this.isLoaded = false;
		/**@type {Array<XMLHttpRequest} */
		this.currentLoadRequests = [];
		/**@type {Number} */
		this.loadingStartTime = 0;

		/**@type {Number} */
		this.updateCycleTimer = 0;
		/** @type {Number} */
		this.drawCycleTimer = 0;
		/** @type {Number} */
		this.objectIterations = 0;

		/**@type {Number} */
		this.totalSteps = 0;
		/**@type {Number} */
		this.lastUpdate = 0;
		/**@type {Number} */
		this.playTime = 0;
		/**@type {Boolean} */
		this.isPlaying = true;
	}

	/**@type {Game} */
	static get instance(){
		return Game._instance;
	}

	/**
	 * @param {Array<XMLHttpRequest>} requests 
	 * @param {Funciton} onFinishLoading
	 */
	static LoadRequests(requests, onFinishLoading = null){
		Game.instance.LoadRequests(requests, onFinishLoading);
	}

	/**
	 * setst the game state
	 * @param {GameState} state 
	 */
	SetState(state){

		state.parentGame = this;
		let oldState = this.state;
		this.state = state;
		
		oldState.onEnd(state);
		state.onBegin(oldState);
	}

	Step(){

		let now = performance.now();
		let dt = Math.min((now - this.lastUpdate) / 1000, 0.1);
		this.playTime += dt;
		this.lastUpdate = now;

		this.objectIterations = 0;

		let updateStartTime = performance.now();
		this.state.Update(dt * this.masterTimescale);
		this.updateCycleTimer = performance.now() - updateStartTime;

		let drawStartTime = performance.now();
		this.state.Draw();
		this.drawCycleTimer = performance.now() - drawStartTime;
		
		if(Input.devTools){
			this.RenderDebugInfo();
		}

		Input.Step();

		this.totalSteps++;
	}

	ClearCanvas(){
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	/**
	 * @param {Color} color 
	 */
	FillCanvas(color){

		this.context.fillStyle = color.ToRGBA();
		this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
	}

	/**
	 * renders the specified image to the game canvas
	 * @param {CanvasImageSource} image 
	 */
	Render(image){
		this.context.drawImage(image, 0, 0, this.canvas.width, this.canvas.height);
	}

	RenderDebugInfo(){

		let updateCycleTxt = "update cycle:       ";
		let ucNumTxt = this.updateCycleTimer.toFixed(2);
		updateCycleTxt = updateCycleTxt.substr(0, updateCycleTxt.length - ucNumTxt.length);
		updateCycleTxt = updateCycleTxt + ucNumTxt;
		let updateCycleImg = SpriteFont.default.GenerateTextImageFixedWidth(updateCycleTxt, 1, -2);
		this.context.drawImage(updateCycleImg, 0, 0);

		let renderCycleTxt = "render cycle:       ";
		let rcNumTxt = this.drawCycleTimer.toFixed(2);
		renderCycleTxt = renderCycleTxt.substr(0, renderCycleTxt.length - rcNumTxt.length);
		renderCycleTxt = renderCycleTxt + rcNumTxt;
		let renderCycleImg = SpriteFont.default.GenerateTextImageFixedWidth(renderCycleTxt, 1, -2);
		this.context.drawImage(renderCycleImg, 0, 10);

		let objectCountTxt = "object count:       ";
		let ocNum = this.state.objectContainer != null ? this.state.objectContainer.activeObjects : 0;
		let ocNumTxt = ocNum.toFixed(0);
		objectCountTxt = objectCountTxt.substr(0, objectCountTxt.length - ocNumTxt.length);
		objectCountTxt = objectCountTxt + ocNumTxt;
		let objectCountImg = SpriteFont.default.GenerateTextImageFixedWidth(objectCountTxt, 1, -2);
		this.context.drawImage(objectCountImg, 0, 20);

		let objectIterTxt = "obj iterations:     ";
		let oiNum = this.state.totalObjectIterations || 0;
		let oiNumTxt = oiNum.toFixed(0);
		objectIterTxt = objectIterTxt.substr(0, objectIterTxt.length - oiNumTxt.length);
		objectIterTxt = objectIterTxt + oiNumTxt;
		let objectIterImg = SpriteFont.default.GenerateTextImageFixedWidth(objectIterTxt, 1, -2);
		this.context.drawImage(objectIterImg, 0, 30);

		let objectIterRatioTxt = "obj it ratio:       ";
		let oirNumTxt = (oiNum / ocNum).toFixed(2);
		objectIterRatioTxt = objectIterRatioTxt.substr(0, objectIterRatioTxt.length - oirNumTxt.length);
		objectIterRatioTxt = objectIterRatioTxt + oirNumTxt;
		let objectIterRatioImg = SpriteFont.default.GenerateTextImageFixedWidth(objectIterRatioTxt, 1, -2);
		this.context.drawImage(objectIterRatioImg, 0, 40);
	}

	/**
	 * @param {Function} onFinishLoading 
	 */
	InitialLoad(onFinishLoading = null){ 

		this.isLoaded = false;

		let requests = [];

		UIContainer.Load();
		Effect.Load();
		// Drone.Load();
		// Hiveship.Load();
		// Asteroid.Load();
		// Kamikaze.Load();
		// Fighter.Load();
		// Debris.Load();
		Camera.LoadSounds();

		// load the vanilla asset pack
		AssetPack.LoadFromFile("./content/SpriteSheets.json");
		AssetPack.LoadFromFile("./content/Collectibles.json");

		this.LoadRequests(requests, onFinishLoading, true);
	}

	/** loads a resource pack */
	LoadResourcePack(pack){
		
	}

	/**
	 * @param {Array<XMLHttpRequest} requests 
	 */
	LoadRequests(requests, onFinishLoading, hideLoadingScreen = false){

		if(this.currentLoadRequests.length <= 0){
			this.loadingStartTime = performance.now();
		}

		this.currentLoadRequests = this.currentLoadRequests.concat(requests);
		this.isLoaded = false;

		this.goToLoadingScreen();

		let ths = this;
		setTimeout(function(){
			ths.loadingCheck(onFinishLoading);
		});
	}

	loadingCheck(onFinishLoading = null){

		for(let i = this.currentLoadRequests.length - 1; i >= 0; i--){
			if(this.currentLoadRequests[i].readyState >= 4 || this.currentLoadRequests[i].complete) {
				this.currentLoadRequests.splice(i, 1);
			}
		}

		// determine if the game is still loading by checking to see if any more asset packs are 
		// loading
		this.isLoaded = this.currentLoadRequests.length <= 0 || !AssetPack.isLoading;

		if(this.isLoaded){

			let loadTime = performance.now() - this.loadingStartTime;
			console.log("Loading finished in " + loadTime + "ms");

			if(onFinishLoading != null){
				setTimeout(onFinishLoading);
			}
			
			return;
		}

		let ths = this;
		setTimeout(function(){
			ths.loadingCheck(onFinishLoading);
		});
	}

	pointerLockCheck(){

		if(this.isLoaded){
			if(!Input.isPointerLocked && !(this.state instanceof LostFocusScreen)){
				this.SetState(new LostFocusScreen(this.state));
			}
		}
	}

	goToLoadingScreen(){

		if(this.state instanceof LoadingScreen)
			return;
		
		this.SetState(new LoadingScreen(this.state));
	}

	Start(){

		let state = new GameplayState();
		this.SetState(state);

		this.lastUpdate = performance.now();
		this.continueGameLoop();
	}

	StartTitleScreen(){

		let state = new TitleScreen();
		this.SetState(state);

		this.lastUpdate = performance.now();
		this.continueGameLoop();
	}

	continueGameLoop(){

		if(!this.isPlaying)
			return;

		var ths = this;
		requestAnimationFrame(function(){
			ths.Step();
			requestAnimationFrame(function(){
				ths.continueGameLoop();
			});
		});
	}

	End(){

		if(Game.instance == this)
			Game.instance = null;
		this.isPlaying = false;
	}
}

/**
 * @abstract a state that the game can be set to
 */
class GameState{

	constructor(){

		/** @type {Game} */
		this.parentGame = null;

		/** @type {Number} */
		this.playTime = 0;
	}

	/** @type {UIContainer} */
	get ui(){ return this.parentGame.ui; }

	/** @virtual */
	onBegin(previousState){ }

	onEnd(nextState){ }

	Update(dt){ 
		this.playTime += dt;
	}

	Draw(){}
}

class LoadingScreen extends GameState {

	/** @param {GameState} prevState */
	constructor(prevState){
		super();
		
		this.previousState = prevState;

		if(SpriteFont.default != null)
			this.loadingText = SpriteFont.default.GenerateTextImage("Loading...", 2, 1);
	}

	Update(dt){
		
		if(Game.instance.isLoaded){
			Game.instance.SetState(this.previousState);
		}
	}

	Draw(){

		if(this.loadingText != null)
			Game.instance.context.drawImage(this.loadingText, 0, 0);
	}
}

class LostFocusScreen extends GameState{

	constructor(prevState){
		super();

		this.previousState = prevState;

		this.text = SpriteFont.default.GenerateTextImage("Please click here to regain focus", 2, 0);
	}

	onBegin(previousState){
		super.onBegin(previousState);
		Game.instance.canvas.addEventListener("mouseup", Input.LockPointer);
	}

	onEnd(nextState){
		Game.instance.canvas.removeEventListener("mousedown", Input.LockPointer);
	}

	Update(dt){

		if(Input.isPointerLocked){
			Game.instance.SetState(this.previousState);
			Camera.sfx_Select.play();
		}
	}

	Draw(){

		this.previousState.Draw();
		Game.instance.FillCanvas(new Color(0, 0, 0, 0.75));

		let tpos = new Vec2(Game.instance.canvas.width / 2 - this.text.width / 2, Game.instance.canvas.height / 2).floored;
		Game.instance.context.drawImage(this.text, tpos.x, tpos.y);
	}
}

class GameOverScreen extends GameState{

	constructor(score, wave){
		super();

		this.alpha = 0;
		this.prevTimescale = 1;
		this.prevState = null;
		this.restarted = false;

		this.text_gameOver = SpriteFont.default.GenerateTextImage("Game Over", 3 * 2, 0);
		this.text_score = SpriteFont.default.GenerateTextImage("Score: " + score.toString(), 1 * 2, 0);
		this.text_wave = SpriteFont.default.GenerateTextImage("Wave: " + wave.toString(), 1 * 2, 0);
		this.text_retry = SpriteFont.default.GenerateTextImage("Press Enter to play again", 1 * 2, 0);
	}

	onBegin(previousState){
		super.onBegin(previousState);
		previousState.enforceScreenLock = false; 
		this.prevState = previousState;
	}

	Update(dt){
		super.Update(dt);
			
		let fade = dt / 5;
		this.prevTimescale = Math.max(0, this.prevTimescale - fade);
		this.alpha = Math.min(this.alpha + fade, 1);
		
		if(this.alpha < 1)
			this.prevState.Update(dt * this.prevTimescale);

		if(!this.restarted && Input.keysDown[13]){
			Camera.sfx_Submit.play();
			this.restarted = true;
			setTimeout(function(){
				Game.instance.Start();
			}, 100);
		}
	}

	Draw(){

		Game.instance.ClearCanvas();

		this.prevState.Draw();

		let context = Game.instance.context;
		let canvas = Game.instance.canvas;
		let center = new Vec2(canvas.width / 2, canvas.height / 2);
		
		let ga = context.globalAlpha;
		
		context.globalAlpha = this.alpha * 0.75;
		context.fillStyle = "#000";
		context.fillRect(0, 0, canvas.width, canvas.height);

		context.globalAlpha = this.alpha;
		context.drawImage(this.text_gameOver, center.x - this.text_gameOver.width / 2, center.y - 60);
		context.drawImage(this.text_score, center.x- this.text_score.width / 2, center.y + 20);

		context.drawImage(this.text_wave, center.x- this.text_wave.width / 2, center.y + 50);

		if(this.playTime % 1 >= 0.5){
			context.drawImage(this.text_retry, center.x- this.text_retry.width / 2, canvas.height - 50);
		}

		context.globalAlpha = ga;
	}
}

class GameObjectState extends GameState{ 

	constructor(){
		super();

		/**@type {GameObjectContainer} */
		this.objectContainer = new GameObjectContainer();
		this.objectContainer.parentState = this;
	}

	AddScore(points){ }

	clearMiniMapCanvas(){
		
		let csize = new Vec2(this.minimapCanvas.width, this.minimapCanvas.height);
		this.minimapContext.clearRect(0, 0, csize.x, csize.y);
		
		this.minimapContext.fillStyle = "rgba(255,255,255, 0.15)";
		this.minimapContext.strokeStyle = "rgba(255,255,255, 0.15)";
		this.minimapContext.lineWidth = 1;
		this.minimapContext.beginPath();
		this.minimapContext.arc(csize.x / 2, csize.y / 2, csize.x / 2, 0, Math.PI * 2);
		this.minimapContext.closePath();
		this.minimapContext.fill();
		this.minimapContext.stroke();
	}

	PlaySound(audio, volume = 1, position = null){
		this.camera.PlaySound(audio, volume, position);
	}

	/** @param {Character} obj */
	writeObjToMinimap(obj){

		let dpos = obj.position.Minus(this.minimapOrigin);

		if(dpos.magnitude > this.minimapRange){
			if(!(obj instanceof Drone)){
				return;
			}
			else{
				dpos = dpos.normalized.Times(this.minimapRange - 20);
			}
		}

		dpos = dpos.Times((this.minimapCanvas.width / 2) / this.minimapRange);
		dpos = dpos.Plus(new Vec2(this.minimapCanvas.width / 2));
		dpos = dpos.rounded;

		if(obj.isThreat){

			let size = 1;
			let alpha = 1;

			if(obj instanceof Asteroid){
				switch(obj.tier){
					case 1:
						alpha = 0.25;
						break;
					case 2: 
						alpha = 0.5;
						break;
					case 4:
						size = 2;
						break;
					case 5:
						size = 2;
						break;
				}
			}
			if(obj instanceof Projectile){
				alpha = 0.25;
			}

			this.minimapContext.fillStyle = Character.AreTeamsHostile(obj.team, 0) ? 
				"rgba(255,48,0," + alpha.toString() +")" : 
				"rgba(255,192,0," + alpha.toString() +")";

			dpos = dpos.Plus(new Vec2(size / -2)).rounded;
			this.minimapContext.fillRect(dpos.x, dpos.y, size, size);
		}
		if(obj.isFriendly){

			let size = 1;
			if(obj instanceof Hiveship)
				size = 3

			
			this.minimapContext.fillStyle = "#0AF";
			
			dpos = dpos.Plus(new Vec2(size / -2)).rounded;
			this.minimapContext.fillRect(dpos.x, dpos.y, size, size);
		}
	}

	setMinimapCanvas(canvas, context, camera){

		this.minimapCanvas = canvas;
		this.minimapContext = context;
		this.minimapCamera = camera;
	}
}

class TitleScreen extends GameObjectState{
	
	constructor(){
		super();

		let resolution = new Vec2();
		resolution.x = Game.instance.canvas.width / 2;
		resolution.y = Game.instance.canvas.height / 2;

		/**@type {Camera} */
		this.camera = new Camera(resolution);

		this.asteroidSpawner = EnemySpawner.AsteroidFieldSpawner(50, 1, 5, 5, 5);
		this.spawnRadius = 300;
		this._lastCleanup = 0;

		this.transitionOut = 0;

		this.initializeText();
		this.objectContainer.AddGameObjects([new StarField(1000, 0.001)]);
	}

	initializeText(){
		
		this.gameTitle1 = SpriteFont.default.GenerateTextImage("Space Drone Guy...", 2, 0);
		this.gameTitle2 = SpriteFont.default.GenerateTextImage("or something", 1, -1);
		this.startPrompt = SpriteFont.default.GenerateTextImage("Press enter to start", 1, -1);
	}

	Update(dt){
		super.Update(dt);

		this.camera.position.Translate(new Vec2(dt * 10, 0));

		this.asteroidSpawner.Update(this.objectContainer, this.camera.position, this.spawnRadius, this.camera, dt);
		this.objectContainer.UpdateGameObjects(dt, this.camera.viewRect);

		if(this.transitionOut <= 0){
			if(Input.keysDown[13]){
				this.transitionOut += dt;
				this.camera.PlaySound(Camera.sfx_Submit);
			}
		}
		else{
			this.transitionOut += dt;
			if(this.transitionOut >= 1){
				Game.instance.SetState(new TutorialState(this.parentGame));
			}
		}
	}

	Draw(){

		this.camera.ClearCanvas();
		
		this.objectContainer.DrawGameObjects(this.camera);
		this.DrawText();

		if(this.transitionOut > 0){
			this.camera.FillRect(this.camera.viewRect, "rgba(0,0,0," + this.transitionOut * 1.75 + ")");
		}

		Game.instance.FillCanvas(new Color(15, 15, 25));
		Game.instance.Render(this.camera.renderTarget);
	}

	DrawText(){
		let viewRect = this.camera.viewRect;

		this.camera.DrawImage(this.gameTitle1, new Vec2(viewRect.center.x, viewRect.top + 50));
		this.camera.DrawImage(this.gameTitle2, new Vec2(viewRect.center.x, viewRect.top + 70));

		if(this.playTime % 1 >= 0.5){

			this.camera.DrawImage(this.startPrompt, new Vec2(viewRect.center.x, viewRect.bottom - 30));
		}
	}
}

class GameplayState extends GameObjectState{

	constructor(){
		super();

		let resolution = new Vec2();
		resolution.x = Game.instance.canvas.width / 2;
		resolution.y = Game.instance.canvas.height / 2;

		this.score = 0;
		this.enforceScreenLock = true;
		this.transitionIn = 1;

		/**@type {Camera} */
		this.camera = new Camera(resolution);

		/**@type {Hiveship} */
		this.hiveship = new Hiveship();
		this.hiveship.drone = new Drone();
		this.hiveship.drone.hiveship = this.hiveship;
		this.hiveship.InitializeRadarCanvas(55);

		this.centerOnDrone = 1;

		this.objectContainer.AddGameObjects([new StarField(1000, 0.001), this.hiveship, this.player]);
		
		WaveSpawner.InitializeMessages();
		this.waveSpawner = new WaveSpawner();
		
		this.initilizeHUD();
		this._regenerateScoreText = false;
		//this.addRandomEnemies();
	}

	// a reference to the hiveship's current drone
	get player(){
		return this.hiveship.drone;
	}

	/** @type {Number} for debug and benchmarking */
	get totalObjectIterations(){
		
		// add together the objectContainer's total iterations and the hiveship's radar iterations
		let iters = this.objectContainer.totalObjectIterations;
		iters += this.hiveship.radarIterations;

		// reset the iteration counts
		this.objectContainer.totalObjectIterations = 0;
		this.hiveship.radarIterations = 0;

		// return the total number of iterations
		return iters;
	}

	addRandomEnemies(){

		let obs = [];

		for(let i = 10; i > 0; i--){

			let asteroid = Asteroid.FromTier(Math.floor(Math.random() * 3) + 3);
			asteroid.position = Vec2.RandomDirection(Math.random() * 150 + 150);
			obs.push(asteroid);
		}

		for(let i = 10; i > 0; i--){

			let kamikazee = new Kamikaze();
			kamikazee.position = Vec2.RandomDirection(Math.random() * 200 + 250);
			obs.push(kamikazee);
		}

		this.objectContainer.AddGameObjects(obs);
	}

	initilizeHUD(){
		
		let mapSize = 55;
		this.minimapCanvas = document.createElement("canvas");
		this.minimapCanvas.width = mapSize;
		this.minimapCanvas.height = mapSize;

		this.minimapContext = this.minimapCanvas.getContext("2d");
		this.minimapContext.imageSmoothingEnabled = false;
		this.minimapOrigin = this.hiveship.position;

		// this.text_droneLabel = SpriteFont.default.GenerateTextImage("Drone:", 1, -1);
		// this.text_hiveshipLabel = SpriteFont.default.GenerateTextImage("Hive:", 1, -1.01);
		this.text_scoreLabel = SpriteFont.default.GenerateTextImage("Score:", 1, -1.01);
		this.text_scoreText = SpriteFont.default.GenerateTextImage(Math.round(this.score).toString(), 1, -1.0);
		this.text_waveLabel = SpriteFont.default.GenerateTextImage("Wave:", 1, -1.01);
		this.text_waveText = SpriteFont.default.GenerateTextImage(this.waveSpawner.currentWave.toString(), 1, -1.0);
		// this.text_weakSignal = SpriteFont.default.GenerateTextImage("Weak signal", 1, -1.0);
		// this.text_returnToShip = SpriteFont.default.GenerateTextImage("Return to hiveship", 1, -1.0);

		this.textid_interaction = null;
		this.text_interaction = SpriteFont.default.GenerateTextImage("Interact", 1, -1.0);
	}

	UpdateWaveText(){
		this.text_waveText = SpriteFont.default.GenerateTextImage(this.waveSpawner.currentWave.toString(), 1, -1.0);
	}

	AddScore(points){
		this.score += points;
		this._regenerateScoreText = true;
	}

	handleCamera(dt){

		if(!(this.hiveship.drone.isRemoved || this.hiveship.health <= 0))
			this.centerOnDrone = 1;
		else{
			if(this.hiveship.health <= 0)
				this.centerOnDrone = 0;
			this.centerOnDrone -= dt;
		}
		
		if(this.centerOnDrone > 0){
			this.camera.position = this.hiveship.drone.position.clone;
		}
		else{
			this.camera.position = this.camera.position.Plus(this.hiveship.position).Times(0.5);
		}
	}

	/**
	 * @override
	 * @param {Number} dt 
	 */
	Update(dt){
		super.Update(dt);
		
		if(this.enforceScreenLock)
			Game.instance.pointerLockCheck();

		if(this.transitionIn > 0){
			this.transitionIn -= dt;
			if(this.transitionIn < 0){
				this.transitionIn = 0;
			}
			return;
		}

		// handle camera movement
		this.handleCamera(dt);
		
		// handle the in-game hiveship ui
		this.hiveship.uiContainer.Update(dt);

		// update the wave spawner to spawn more enemies and progress through waves
		this.waveSpawner.Update(
			this.objectContainer, 
			this.hiveship.position, 
			this.hiveship.droneRange, 
			this.camera, 
			dt);

		// create a rect that covers both the entire hiveship's radar range and the 
		// camera's viewport rect
		let updateRect = Rect.OverlapRect(
			Rect.FromRadius(this.hiveship.radarRange, this.hiveship.position),
			this.camera.viewRect);

		// update all the objects within the updateRect
		this.objectContainer.UpdateGameObjects(dt, updateRect);

		// play the sound effects that occured in the update cycle
		this.objectContainer.PlayQueuedSounds(this.camera, true);
		
		if(this.hiveship.health <= 0 && Game.instance.state == this)
			Game.instance.SetState(new GameOverScreen(this.score, this.waveSpawner.currentWave));
	}

	Draw(){

		this.camera.ClearCanvas();

		if(this.hiveship != null){
			this.minimapOrigin = this.hiveship.position;
		}
		this.objectContainer.DrawGameObjects(this.camera);

		this.drawHUD(this.camera);

		if(this.transitionIn > 0){
			this.camera.FillRect(this.camera.viewRect, "rgba(0,0,0," + this.transitionIn + ")");
		}

		Game.instance.FillCanvas(new Color(15, 15, 25));
		Game.instance.Render(this.camera.renderTarget);
	}

	drawHUD(){

		let ctx = this.camera.renderContext;
		let cvs = this.camera.renderTarget;
		ctx.fillStyle = "#000";

		// make the camera viewport circular by reverse-filling a black circle around the edges
		ctx.beginPath();
		ctx.arc(cvs.width / 2, cvs.height / 2, cvs.height / 2, 0, Math.PI * 2);
		ctx.lineTo(cvs.width, 0);
		ctx.lineTo(0, 0);
		ctx.lineTo(0, cvs.height);
		ctx.lineTo(cvs.width, cvs.height);
		ctx.closePath();
		ctx.fill();

		// draw the player's score
		this.drawScore();
		
		// draw the interaction prompt if available
		this.drawInteractionPrompt();

		// draw the in-game hiveship UI
		this.hiveship.uiContainer.Draw(this.camera.renderContext);

		// draw the wave spawner HUD / messages
		this.waveSpawner.DrawHUD(this.camera);
	}

	drawInteractionPrompt(){

		// find the interaction target
		let interactionTarg = null;
		if(this.hiveship.drone != null && !this.hiveship.drone.isRemoved)
			interactionTarg = this.hiveship.drone._interactionTarget;

		// return if there is not interaction prompt available
		if(interactionTarg == null)
			return;

		// regenerate the interaction text if necessary
		if(this.textid_interaction != interactionTarg.uid){
			this.text_interaction = 
				SpriteFont.default.GenerateTextImage(
					interactionTarg.GetInteractionText(this.hiveship.drone), 1, -1.0
				);
			this.textid_interaction = interactionTarg.uid;
		}

		let canvasRect = this.camera.viewRect;

		// render the text
		this.camera.DrawImage(
			this.text_interaction, 
			new Vec2(canvasRect.center.x, canvasRect.bottom - 45),
			new Vec2(0.5, 0));
	}

	drawScore(){

		// regenerate the score text if necessary
		if(this._regenerateScoreText){
			this.text_scoreText = SpriteFont.default.GenerateTextImage(Math.round(this.score).toString(), 1, -1.01);
			this._regenerateScoreText = false;
		}

		let canvasRect = this.camera.viewRect;

		this.camera.DrawImage(this.text_scoreLabel, canvasRect.topRight.Plus(new Vec2(-30, 0)), new Vec2(1,0));
		this.camera.DrawImage(this.text_scoreText, canvasRect.topRight, new Vec2(1,0));

		this.camera.renderContext.globalAlpha = 0.5;
		this.camera.DrawImage(this.text_waveLabel, canvasRect.topRight.Plus(new Vec2(-30, 10)), new Vec2(1,0));
		this.camera.DrawImage(this.text_waveText, canvasRect.topRight.Plus(new Vec2(0, 10)), new Vec2(1,0));
		this.camera.renderContext.globalAlpha = 1;
	}
}

class TutorialState extends GameState{

	constructor(parentGame){
		super();

		this.parentGame = parentGame;

		let resolution = new Vec2();
		resolution.x = Game.instance.canvas.width / 2;
		resolution.y = Game.instance.canvas.height / 2;

		this.camera = new Camera(resolution);

		let sstate = new GameplayState();
		sstate.parentGame = this.parentGame;
		sstate.transitionIn = 0;
		sstate.player.position = new Vec2(0, 80);
		sstate.addRandomEnemies();
		sstate.Update(0);
		sstate.camera.position = new Vec2(0, 50);
		sstate.Draw();

		this.exampleImage = sstate.camera.renderTarget;
		this.initializeText();

		this.transitionOut = 0;
	}

	initializeText(){
		
		this.txt_droneExp0 = SpriteFont.default.GenerateTextImage("This is your drone", 1, -1);
		this.txt_droneExp1 = SpriteFont.default.GenerateTextImage("Move with W, A, S, D", 1, -1);
		this.txt_droneExp2 = SpriteFont.default.GenerateTextImage("Aim with mouse", 1, -1);
		this.txt_droneExp2 = SpriteFont.default.GenerateTextImage("Fire with Left AND Right Click, different weapons", 1, -1);
		
		this.txt_hiveShip0 = SpriteFont.default.GenerateTextImage("This is your hive ship", 1, -1);
		this.txt_hiveShip1 = SpriteFont.default.GenerateTextImage("Protect it with your drone", 1, -1);

		this.txt_radar0 = SpriteFont.default.GenerateTextImage("<< This is your enemy radar", 1, -1);
		this.txt_radar1 = SpriteFont.default.GenerateTextImage("PAY ATTENTION TO IT", 1, -1);
		this.txt_radar2 = SpriteFont.default.GenerateTextImage("Red and Yellow dots are threats", 1, -1);
		this.txt_radar3 = SpriteFont.default.GenerateTextImage("Blue dots represent friendlies", 1, -1);
		
		this.txt_startPrompt = SpriteFont.default.GenerateTextImage("Press enter to start", 1, -1);
	}

	Update(dt){
		super.Update(dt);
		
		if(this.transitionOut <= 0){
			if(Input.keysDown[13]){
				this.transitionOut += dt;
				this.camera.PlaySound(Camera.sfx_Submit);
			}
		}
		else{
			this.transitionOut += dt;
			if(this.transitionOut >= 1){
				Game.instance.SetState(new GameplayState());
			}
		}
	}

	Draw(){
		
		this.camera.ClearCanvas();

		this.camera.DrawImage(this.exampleImage, new Vec2());
		this.DrawText();

		if(this.transitionOut > 0){
			this.camera.FillRect(this.camera.viewRect, "rgba(0,0,0," + this.transitionOut * 1.75 + ")");
		}

		Game.instance.FillCanvas(new Color(15, 15, 25));
		Game.instance.Render(this.camera.renderTarget);
	}

	DrawText(){ 
		
		let viewRect = this.camera.viewRect;

		let y = 40;
		let x = 0;
		this.camera.DrawImage(this.txt_droneExp0, viewRect.center.Plus(new Vec2(0, y + 0)));
		this.camera.DrawImage(this.txt_droneExp1, viewRect.center.Plus(new Vec2(0, y + 15)));
		this.camera.DrawImage(this.txt_droneExp2, viewRect.center.Plus(new Vec2(0, y + 30)));
		
		y = -15
		this.camera.DrawImage(this.txt_hiveShip0, viewRect.center.Plus(new Vec2(0, y + 0)));
		this.camera.DrawImage(this.txt_hiveShip1, viewRect.center.Plus(new Vec2(0, y + 15)));

		y = -140
		x = -40;
		this.camera.DrawImage(this.txt_radar0, viewRect.center.Plus(new Vec2(x + 0, y + 0)));
		this.camera.DrawImage(this.txt_radar1, viewRect.center.Plus(new Vec2(x + 0, y + 15)));
		this.camera.DrawImage(this.txt_radar2, viewRect.center.Plus(new Vec2(x + 0, y + 30)));
		this.camera.DrawImage(this.txt_radar3, viewRect.center.Plus(new Vec2(x + 0, y + 45)));

		if(this.playTime % 1 >= 0.5){
			this.camera.DrawImage(this.txt_startPrompt, new Vec2(viewRect.center.x, viewRect.bottom - 30));
		}
	}
}