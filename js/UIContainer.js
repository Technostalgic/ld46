///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * a data structure used to control and display user interface windows
 */
class UIContainer{

	constructor(){
		
		/** @type {Array<UIWindow>} */
		this._windows = [];

		/** @type {Number} */
		this._elapsedTime = 0;

		/** @type {Number} */
		this._focusIndex = 0;

		/** @type {CanvasImageSource} */
		this.cursorGraphic = UIContainer.graphic_defaultCursor;

		/** @type {Vec2} */
		this.cursorPosition = new Vec2();
		/** @type {Vec2} */
		this._lastCursorPos = this.cursorPosition.clone;

		/** @type {boolean} */
		this.submitPressed = false;
		/** @type {boolean} */
		this.submitPressed_initial = false;
		/** @type {boolean} */
		this.altSubmitPressed = false;
		/** @type {boolean} */
		this.altSubmitPressed_initial = false;

		/** @type {Boolean} */
		this._cursorEnabled = false;

		/** @type {Camera} */
		this.uiCam = null;

		/** @type {Array<UIElement>} */
		this.selectedElements = [];
	}

	/** 
	 * @static @type {UIContainer} 
	 * @param {Vec2} resolution
	 */
	static FromResolution(resolution){
		
		let r = new UIContainer();
		r.uiCam = new Camera(resolution);
		
		return r;
	}

	static Load(){

		UIContainer.graphic_defaultCursor = IO.LoadImageFile("./gfx/UICursor.png", function(){});
		Game.LoadRequests([UIContainer.graphic_defaultCursor]);
	}

	/** @type {UIWindow} */
	get focusWindow(){

		// clamp the window index to the range of the window array indices
		let index = Math.max(Math.min(this._focusIndex, this._windows.length - 1), 0);
		this._focusIndex = index;

		// return the window at the index
		return this._windows[index];
	}
	set focusWindow(value){
		let ind = this._windows.indexOf(value);
		if(ind >= 0)
			this._focusIndex = ind;
	}

	/** @type {Boolean} */
	get cursorEnabled(){ return this._cursorEnabled; }
	set cursorEnabled(value){ this._cursorEnabled = value; }

	/** 
	 * the position of the cursor from the topleft of the ui view rect, so that the top lef of the
	 * ui container is <0,0> instead of the center of the screen being <0,0>
	 */
	get cursorPositionOffset(){
		return this.uiCam.offset.Plus(this.cursorPosition)
	}

	/** @type {Boolean} */
	get anyOpenWindows(){ return this._windows.length > 0; }

	/** @type {Number} */
	get elapsedTime() { return this._elapsedTime; }

	/**
	 * @type {UIWindow} adds a ui window to the top of the stack
	 * @param {UIWindow} uiWindow 
	 */
	AddWindowOnTop(uiWindow){

		// insert a window in the begining of the list
		uiWindow.OnAdded(this);
		this._windows.splice(0, 0, uiWindow);

		return uiWindow;
	}

	/**
	 * @type {UIWindow} adds a UI window to the bottom of the stack
	 * @param {UIWindow} uiWindow 
	 */
	AddWindowOnBottom(uiWindow){
		
		// add a window to the end of the window stack
		uiWindow.OnAdded(this);
		this._windows.push(uiWindow);

		return uiWindow;
	}

	/**
	 * closes the specified ui window
	 * @param {UIWindow} uiWindow 
	 */
	CloseWindow(uiWindow){
		
		// get the index of the window in the windows array
		let i = this._windows.indexOf(uiWindow);

		// if the window is not in the array, do nothing
		if(i < 0)
			return;

		// close the window and remove it from the array
		this._windows[i].uiContainer = null;
		this._windows.splice(i, 1);

		// iterate through each selected element and remove the ones that were part of the window 
		// that closed
		for(let i = this.selectedElements.length - 1; i >= 0; i--){
			if(this.selectedElements[i].parentWindow == uiWindow)
				this.selectedElements.splice(i, 1);
		}

		// disable cursor if all windows are closed
		if(this.cursorEnabled && !this.anyOpenWindows){
			this.cursorEnabled = false;
			this.cursorPosition = Vec2.zero;
		}
	}

	/**
	 * closes all the windows inside the UI container
	 */
	CloseAllWindows(){

		// iterate through each window and close it
		while(this._windows.length > 0){
			this.CloseWindow(this._windows[0]);
		}
	}

	/**
	 * handles update logic to do with manipulating the on-screen ui cursor
	 */
	HandleCursor(){

		let mouseDelta = Input.mouseDelta;
		this._lastCursorPos = this.cursorPosition.clone;
		this.cursorPosition = this.cursorPosition.Plus(mouseDelta.Times(0.5));
		let cursorDif = this.cursorPosition.Minus(this._lastCursorPos);
			
		// if the cursor is outside the bounds of the ui camera, clamp it to the ui bounds
		if(!this.uiCam.viewRect.OverlapsPoint(this.cursorPosition)){
			this.cursorPosition = this.uiCam.viewRect.ClosestPoint(this.cursorPosition);
		}

		// if the cursor was moved
		if (!(cursorDif.x == 0 && cursorDif.y == 0)){
			this.FocusWindowUnderCursor();
			this.SelectElementsFromCursorPosition();
		}

		// left click
		if(Input.mouseButtonsDown[0]){

			// only set submitPressed_initial if the mouse button is not held down
			if(!this.submitPressed)
				this.submitPressed_initial = true;

			// set the submitPressed button to true
			this.submitPressed = true;
		}
		else{

			// reset the flags when the mouse is not pressed
			this.submitPressed = false;
			this.submitPressed_initial = false;
		}
		
		// right click
		if(Input.mouseButtonsDown[2]){

			// only set submitPressed_initial if the mouse button is not held down
			if(!this.altSubmitPressed)
				this.altSubmitPressed_initial = true;

			// set the submitPressed button to true
			this.altSubmitPressed = true;
		}
		else{

			// reset the flags when the mouse is not pressed
			this.altSubmitPressed = false;
			this.altSubmitPressed_initial = false;
		}
	}

	/**
	 * sets the focus window to the window currently under the cursor
	 */
	FocusWindowUnderCursor(){

		// iterate through each window in the UI container
		for(let i = 0; i < this._windows.length; i++){
			if(!this._windows[i].focusable)
				continue;

			if(this._windows[i].bounds.OverlapsPoint(this.cursorPosition)){
				this._focusIndex = i;
				break;
			}
		}
	}

	/**
	 * Selects the specified elements
	 * @param {Array<UIElement>} elements 
	 */
	SetElementSelection(elements){

		// iterate through each of the previously selected elements
		for(let i = this.selectedElements.length - 1; i >= 0; i--){

			// if it's not included in the newly selected elements, deselect it
			if(elements.indexOf(this.selectedElements[i]) < 0)
				this.selectedElements[i].SendAction(UIActionType.Deselect);
		}

		// iterate through each element specified
		for(let i = elements.length - 1; i >= 0; i--){

			// if element is not already selected, send the select UI action to it
			let selIndex = this.selectedElements.indexOf(elements[i]);
			if(selIndex < 0){
				elements[i].SendAction(UIActionType.Select);
			}
		}

		// set the new selection
		this.selectedElements = elements;
	}

	/**
	 * selects the UI element currently overlapping the cursor
	 */
	SelectElementsFromCursorPosition(){

		// if there are any open windows, iterate through all the elements on the focused window
		let selection = null;
		if(this.anyOpenWindows){

			// get all child elements of focus window and iterate through them
			let window = this.focusWindow;
			let elems = window.GetChildElements();
			for(let i = elems.length - 1; i >= 0; i--){

				// get the reference to the element that's currently being iterated through
				let elem = elems[i];

				// skip the iteration if the element is not selectable
				if(!elem.isSelectable)
					continue;

				// if the cursor is hovering over the current element, select it
				if(elem.bounds.OverlapsPoint(this.cursorPosition)){
					selection = elem;
				}
			}
		}

		// change the selection to the elements touched by the mouse
		this.SetElementSelection(selection != null ? [selection] : []);
	}

	/**
	 * renders the cursor onto the ui cam
	 */
	DrawCursor(){

		// set the default cursor of the cursor is not specified
		if(this.cursorGraphic == null){
			this.cursorGraphic = UIContainer.graphic_defaultCursor;
		}

		// render cursor graphic
		this.uiCam.DrawImage(this.cursorGraphic, this.cursorPosition.rounded, Vec2.zero);
	}

	/**
	 * handle update logic to do with the UI
	 * @param {*} dt 
	 */
	Update(dt){
		
		// update the elapsed time and move the cursor
		this._elapsedTime += dt;

		// handle cursor movement if necessary
		if(this.cursorEnabled){
			if(this.anyOpenWindows)
				this.HandleCursor();
			else
				this.cursorEnabled = false;
		}

		// TODO make proper input controller with control bindings and stronger encapsulation
		if(this.submitPressed_initial){
			for(let i = this.selectedElements.length - 1; i >= 0; i--){

				// get the selected element
				let elem = this.selectedElements[i]

				// call the onSubmit funtion for the element
				elem.SendAction(UIActionType.Submit);
			}
		}

		if(this.altSubmitPressed_initial){
			for(let i = this.selectedElements.length - 1; i >= 0; i--){

				// get the selected element
				let elem = this.selectedElements[i]

				// call the onSubmit funtion for the element
				elem.SendAction(UIActionType.AltSubmit);
			}
		}

		// reset the submitPressed initial flag
		this.submitPressed_initial = false;
		// reset the submitPressed initial flag
		this.altSubmitPressed_initial = false;
	}

	/**
	 * draw the UI on the specified context
	 * @param {CanvasRenderingContext2D} ctx 
	 */
	Draw(ctx){

		// clear the ui canvas
		this.uiCam.ClearCanvas();

		// iterate through each window backwards so the top window is drawn last
		for(let i = this._windows.length - 1; i >= 0; i--){

			// draw each window individually
			this._windows[i].Draw(this.uiCam);
		}

		// draw the cursor if necessary
		if(this._cursorEnabled){
			this.DrawCursor();
		}

		// render the camera image onto the specified target context
		ctx.drawImage(this.uiCam.renderTarget, 0, 0, ctx.canvas.width, ctx.canvas.height);
	}
}