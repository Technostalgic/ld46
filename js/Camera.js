///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Camera {

	/**
	 * creates a camera object with the specified rendering resolution
	 * @param {Vec2} resolution 
	 */
	constructor(resolution){
		
		/**@type {Vec2} */
		this.position = new Vec2();

		/**@type {Number} */
		this.zoom = 1;

		// create the render canvas and context to draw to
		/** @type {HTMLCanvasElement} */
		this.renderTarget = document.createElement("canvas");
		this.renderTarget.width = resolution.x;
		this.renderTarget.height = resolution.y;

		/** @type {CanvasRenderingContext2D} */
		this.renderContext = this.renderTarget.getContext('2d');
		this.renderContext.imageSmoothingEnabled = false;
	}

	static LoadSounds(){
		
		this.sfx_Select = IO.LoadAudioFile("./sfx/Select.wav");
		this.sfx_Submit = IO.LoadAudioFile("./sfx/Submit.wav");
		
		this.sfx_CannonFire = IO.LoadAudioFile("./sfx/CannonFire.wav");
		this.sfx_CannonHit = IO.LoadAudioFile("./sfx/CannonHit.wav");
		this.sfx_EnemyWeaponFire = IO.LoadAudioFile("./sfx/EnemyWeaponFire.wav");
		this.sfx_EnemyWeaponHit = IO.LoadAudioFile("./sfx/EnemyWeaponHit.wav");
		this.sfx_PulseLaserFire = IO.LoadAudioFile("./sfx/PulseLaserFire.wav");
		this.sfx_PulseLaserHit = IO.LoadAudioFile("./sfx/PulseLaserHit.wav");
		this.sfx_BeamLaser = IO.LoadAudioFile("./sfx/BeamLaser.wav");

		this.sfx_HiveshipExplode = IO.LoadAudioFile("./sfx/HiveshipExplode.wav");
		this.sfx_EnemyExplode = IO.LoadAudioFile("./sfx/EnemyExplode.wav");
		this.sfx_DebrisExplode = IO.LoadAudioFile("./sfx/DebrisExplode.wav");

		Game.instance.LoadRequests([
			this.sfx_Select,
			this.sfx_Submit,
			this.sfx_CannonFire,
			this.sfx_CannonHit,
			this.sfx_EnemyWeaponFire,
			this.sfx_EnemyWeaponHit,
			this.sfx_PulseLaserFire,
			this.sfx_PulseLaserHit,
			this.sfx_HiveshipExplode,
			this.sfx_EnemyExplode,
			this.sfx_DebrisExplode
		]);
	}

	/** @type {Vec2} the offset of the center of the camera's viewport */
	get offset(){
		return new Vec2(this.renderTarget.width / 2, this.renderTarget.height / 2);
	}

	/** @type {Vec2} the resolution the camera renders at */
	get resolution(){
		return new Vec2(this.renderTarget.width, this.renderTarget.height);
	}

	/** @type {Rect} the rect that represents the area in worldspace that the camera is currently rendering */
	get viewRect(){
		let size = new Vec2(this.renderTarget.width, this.renderTarget.height);
		return new Rect(this.position.Plus(size.Times(-0.5)), size);
	}
	
	/**
	 * @type {Vec2} transforms a world position to a position relative to the camera's viewport and returns the value
	 * @param {Vec2} position 
	 */
	WorldToViewportPosition(position){

		// apply camera translation
		let tpos = position.Minus(this.position);

		// apply camera zoom
		tpos = tpos.Times(this.zoom);

		// return the result with the center offset applied
		return tpos.Plus(this.offset);
	}

	/**
	 * @type {Vec2} opposite of WorldToViewportPosition
	 * @param {Vec2} position 
	 */
	ViewportToWorlPosition(position){

		// undo the camera center offset
		let tpos = position.Minus(this.offset);

		// reverse apply the camera zoom
		tpos = tpos.Times(1 / this.zoom);

		// reverse-apply translation
		return tpos.Plus(this.position);
	}

	/**
	 * draws a box fill to the camera
	 * @param {Rect} box 
	 * @param {String} fillstyle 
	 */
	FillRect(box, fillstyle = "#fff"){

		// set the fill color
		this.renderContext.fillStyle = fillstyle;

		// convert the box's world position to the camera position
		let tpos = this.WorldToViewportPosition(box.position).rounded;

		// fill in the tranformed rectangle
		this.renderContext.fillRect(tpos.x, tpos.y, box.width, box.height);
	}
	
	/**
	 * draws a box fill to the camera without setting the fillstyle
	 * @param {Rect} box 
	 */
	FillRectFast(box){

		// convert the box's world position to the camera position
		let tpos = this.WorldToViewportPosition(box.position).rounded;

		// fill in the tranformed rectangle
		this.renderContext.fillRect(tpos.x, tpos.y, box.width, box.height);
	}
	
	/**
	 * draws a box outline to the camera
	 * @param {Rect} box 
	 * @param {String} strokeStyle 
	 */
	StrokeRect(box, strokeStyle = "#fff", lineWidth = 1){

		// set the stroke color
		this.renderContext.strokeStyle = strokeStyle;
		
		// set the line width
		this.renderContext.lineWidth = lineWidth;

		// convert the box's world position to the camera position
		let tpos = this.WorldToViewportPosition(box.position).rounded;

		// shrink the box by 0.5 pixels if line width is odd, to avoid unwanted anti-aliasing
		let tsize = box.size.clone;
		if(lineWidth % 2 != 0){
			tpos = tpos.Minus(new Vec2(0.5));
			tsize = tsize.Minus(new Vec2(0.5));
		}

		// draw outline at the tranformed rectangle
		this.renderContext.strokeRect(tpos.x, tpos.y, box.width, box.height);
	}

	/**
	 * draws the specified path outline
	 * @param {Array<Vec2>} path an array of vertices that form the path
	 * @param {String} strokestyle the stroke style of the line
	 * @param {Number} lineWidth the width of the path's outline
	 * @param {Boolean} closePath whether or not the first and last vertex of the path are 
	 * 	connected
	 * @param {Boolean} shift whether or not the path is shifted downward and to the right by 
	 * 	1/2 pixel
	 */
	DrawPathOutline(path, strokestyle, lineWidth, closePath, shift = false){

		// set the context stroke settings and style
		this.renderContext.strokeStyle = strokestyle;
		this.renderContext.lineWidth = lineWidth;

		// begin the path
		this.renderContext.beginPath();

		// set the starting position of the path to the first vertex of the path transformed by the camera
		let origin = this.WorldToViewportPosition(path[0]);
		if(shift)
			origin = origin.Plus(new Vec2(0.5));
		this.renderContext.moveTo(origin.x, origin.y);

		// create a line from each vertex to the next
		for(let i = 1; i < path.length; i++){

			// add path node at transformed coordinates
			let vert = this.WorldToViewportPosition(path[i]);

			if(shift)
				vert = vert.Plus(new Vec2(0.5));

			this.renderContext.lineTo(vert.x, vert.y);
		}

		// connect the path's start and end vertices if necessary
		if(closePath){
			this.renderContext.closePath();
		}

		// draw the path to the canvas
		this.renderContext.stroke();
	}

	/**
	 * draws the speciefied path fill
	 * @param {Array<Vec2>} path an array of vertices that form the path
	 * @param {String} fillstyle the fill style of the line
	 * @param {Boolean} closePath whether or not the first and last vertex of the path are connected
	 */
	DrawPathFill(path, fillstyle, closePath){

		// set the context fill style
		this.renderContext.fillStyle = fillstyle;

		// begin the path
		this.renderContext.beginPath();

		// set the starting position of the path to the first vertex of the path transformed by the camera
		let origin = this.WorldToViewportPosition(path[0]);
		this.renderContext.moveTo(origin.x, origin.y);

		// create a line from each vertex to the next
		for(let i = 1; i < path.length; i++){

			// add path node at transformed coordinates
			let vert = this.WorldToViewportPosition(path[i]);
			this.renderContext.lineTo(vert.x, vert.y);
		}

		// connect the path's start and end vertices if necessary
		if(closePath){
			this.renderContext.closePath();
		}

		// draw the path fill to the canvas
		this.renderContext.fill();
	}

	
	/**
	 * draws a sprite at the specified world position
	 * @param {SpriteSheet} image 
	 * @param {Rect} bounds 
	 * @param {Vec2} pivot
	 * @param {Vec2} position 
	 * @param {Number} rotation the rotation in radians
	 * @param {Boolean} flipped whether or not the sprite should be drawn flipped horizontally
	 */ 
	DrawSpriteFromImage(image, bounds, pivot, position, rotation = 0, flipped = true){
		
		// save the render context's transform
		this.renderContext.save();
		
		// calculate the viewport position from a world position
		/**@type {Vec2} */
		let tpos = this.WorldToViewportPosition(position).rounded;

		// translate the context so the subject is drawn in the center of the camera viewport
		this.renderContext.translate(tpos.x, tpos.y);
		
		// rotate the context to orient the subject to the specified position
		this.renderContext.rotate(rotation);
		
		// calculate the x offset from the sprite frame's pivot point
		let xOff = -pivot.x;

		// invert the x offset across the sprite's width if the sprite is drawing as flipped along the horizontal axis
		if(flipped){
			xOff = -(bounds.width - pivot.x);
		}

		// offset the subject by it's pivot so the rotation origin is at the specified pivot instead of the top left
		this.renderContext.translate(Math.round(xOff), Math.round(-pivot.y));
		
		// flip the context and adjust for flipped position offset if necessary
		if(flipped){
			this.renderContext.scale(-1, 1);
			this.renderContext.translate(-bounds.width, 0);
		}

		// draw the sprite image to the camera's canvas
		this.renderContext.drawImage(image,
			bounds.left, bounds.top, bounds.width, bounds.height,
			0, 0, bounds.width, bounds.height);

		// restore the render context's transform to the initial state
		this.renderContext.restore();
	}

	/**
	 * draws a sprite at the specified world position
	 * @param {SpriteSheet} spriteSheet 
	 * @param {Number} frameIndex 
	 * @param {Vec2} position 
	 * @param {Number} rotation the rotation in radians
	 * @param {Boolean} flipped whether or not the sprite should be drawn flipped horizontally
	 */ 
	DrawSpriteFromSheet(spriteSheet, frameIndex, position, rotation = 0, flipped = false){

		// store the sprite's frame
		let sprite = spriteSheet.SpriteAt(frameIndex);

		this.DrawSpriteFromImage(
			spriteSheet.image, 
			sprite.bounds, 
			sprite.pivot, 
			position, 
			rotation, 
			flipped
		);
	}

	/**
	 * draws a sprite at the specified world position, with the current frame according to the 
	 * sprite's current frame index property
	 * @param {Sprite} sprite 
	 * @param {Vec2} position where the sprite is drawn
	 * @param {Number} rotation the rotation in radians
	 */ 
	DrawSprite(sprite, position, rotation = 0, flipped = false){
		this.DrawSpriteFromImage(
			sprite.spriteSheet.image, 
			sprite.bounds,
			sprite.pivot, 
			position,
			rotation, 
			flipped
		);
	}

	/**
	 * @param {CanvasImageSource} image 
	 * @param {Vec2} position 
	 * @param {Vec2} anchor 
	 */
	DrawImage(image, position, anchor = new Vec2(0.5)){

		let offset = new Vec2(image.width * -anchor.x, image.height * -anchor.y);
		let tpos = this.WorldToViewportPosition(position.Plus(offset));

		// calculate the image size
		let size = new Vec2(image.width * this.zoom, image.height * this.zoom);

		// draw the image
		this.renderContext.drawImage(image, tpos.x, tpos.y, size.x, size.y);
	}

	/** 
	 * @param {HTMLAudioElement} audio
	 * @param {Vec2} position
	 */
	PlaySound(audio, volume = 1, position = null){

		let vol = volume;

		if(position != null){
			let minFalloff = 0.1;
			let falloffStart = 75;
			let falloffDist = 300;
			let dist = position.Minus(this.position).magnitude;
			let ffactor = Math.max(minFalloff, Math.min( 1 - (dist - falloffStart) / (falloffDist), 1));
			vol *= ffactor;
		}
		
		audio.volume = vol;
		audio.currentTime = 0;
		audio.play();
	}

	/** 
	 * @param {HTMLAudioElement} audio
	 * @param {Vec2} position
	 */
	ContinueSound(audio, volume = 1, position = null){
		
		// do not play if the sound has already completed playing
		if(audio.currentTime >= audio.duration)
			return;
		
		let vol = volume;

		if(position != null){
			let minFalloff = 0.1;
			let falloffStart = 75;
			let falloffDist = 300;
			let dist = position.Minus(this.position).magnitude;
			let ffactor = Math.max(minFalloff, Math.min( 1 - (dist - falloffStart) / (falloffDist), 1));
			vol *= ffactor;
		}
		
		audio.volume = vol;
		audio.play();
	}

	/**
	 * @param {SimpleSoundInstance} soundInstance 
	 */
	PlaySoundInstance(soundInstance){
		this.PlaySound(soundInstance.audio, soundInstance.baseVolume, soundInstance.origin);
	}
	
	/** clears the camera's render target */
	ClearCanvas(){
		this.renderContext.clearRect(0, 0, this.renderTarget.width, this.renderTarget.height);
	}
}