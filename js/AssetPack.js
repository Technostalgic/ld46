///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * class used for loading content into the game dynamically
 */
class AssetPack{

	constructor(){

		/** @type {String} */
		this.key = "assetPack";

		/** @type {Object} the raw data that's been loaded from the resource pack file */
		this.rawData = null;

		/** @type {String} */
		this.filePath = null;

		/** @type {String} */
		this._relativePath = null;

		/** @type {Boolean} */
		this._isLoaded = false;

		/** @type {Array<Asset>} */
		this._nonLoadedAssets = null;

		/** @type {Array<Asset>} the assets loaded from this specific AssetPack instance */
		this.selfAssets = [];

		/** @type {Array<Function>} */
		this._onFinishedLoadingCallbacks = [];
	}
	
	/** 
	 * @returns {AssetPack}
	 * @param {String} path
	 */
	static LoadFromFile(path){
		
		let r = new AssetPack();
		r.filePath = IO.GetLocalPath(path);

		// ensure there is a variable to keep track of outgoing file requests
		if(AssetPack.outgoingFileRequests == null)
			AssetPack.outgoingFileRequests = 0;

		// increment the outgoing file request counter by one
		AssetPack.outgoingFileRequests += 1;

		// load the specified file asyncronously
		IO.LoadDataFile(path, function(dataStr, request){
			
			// decrement outgoing file request counter when file is retrieved
			AssetPack.outgoingFileRequests -= 1;

			// load the file data into the asset pack
			r.LoadDataFromStr(dataStr);
		});

		// return the asset pack that will be loaded
		return r;
	}

	/** 
	 * @returns {AssetPack} 
	 * @param {String} strData
	 * @param {String} targetPath the path that the assets in the asset pack will be 
	 *  referenced from
	 */
	static FromStringData(strData, targetPath = null){

		let r = new AssetPack();
		r._relativePath = targetPath;
		r.LoadDataFromStr(strData);
		return r;
	}

	/** 
	 * @returns {AssetPack}
	 * @param {Object} objData
	 * @param {String} targetPath the path that the assets in the asset pack will be 
	 *  referenced from
	 */
	static FromObjectData(objData, targetPath = null){
		
		let r = new AssetPack();
		r._relativePath = targetPath;
		r.LoadDataFromObj(objData);
		return r;
	}

	/** @type {Boolean} whether or not there are any asset packs loading */
	static get isLoading(){

		// if there are any outgoing file requests that have not been resolved, still loading
		if(AssetPack.outgoingFileRequests > 0)
			return true;

		// iterate through each asset pack and ensure it is loading
		if(AssetPack.library._assetPacks != null){
			for(let i = AssetPack.library._assetPacks.length - 1; i >= 0; i--){
				if(!AssetPack.library._assetPacks[i].finishedLoading)
					return true;
			}
		}

		// if all the tests pass, no asset packs are being loaded
		return false;
	}

	/** @type {Object} */
	static get library(){
		
		if(AssetPack._library == null)
			AssetPack._library = {};

		return AssetPack._library;
	}
	
	/**
	 * @type {Asset} gets the asset in the specified asset pack and asset collection with the 
	 * specified asset key
	 * @param {String} assetPack 
	 * @param {String} collection 
	 * @param {String} key 
	 */
	static GetAsset(assetPack, collection, key){

		let pack = AssetPack.library[assetPack];
		if(pack == null)
			throw "asset pack '" + assetPack + "' does not exist";

		let col = pack[collection];
		if(col == null)
			throw(
				"collection '" + collection + 
				"' does not exist inside asset pack '" + assetPack + 
				"'" );

		let asset = col[key];
		if(asset == null)
			throw "asset '" + key + "' not found";

		return asset; 
	}

	/** @type {Object} */
	get library(){
		if(AssetPack.library[this.key] == null)
			AssetPack.library[this.key] = {};
		return AssetPack.library[this.key];
	}

	/** 
	 * @type {Boolean} returns true if the data concerning the resources that this pack includes have
	 * been loaded from the file or not, note: this does not mean that each resource has been loaded
	*/
	get finishedLoading(){
		return this._isLoaded;
	}

	/**
	 * @type {Array<Asset>} 
	 * get all the assets that belong to an asset pack with the same name as this instance
	 */
	GetAllAssets(){

		// iterate through each key in each collection and add it to the list
		let r = [];
		for(let collection in this.library){
			for(let key in this.library[collection]){
				r.push(this.library[collection][key]);
			}
		}

		// return the list of assets
		return r;
	}

	/**
	 * same as LoadDataFromObj but the string is parsed into a JS object from JSON
	 * @param {String} string 
	 */
	LoadDataFromStr(string){
		this._isLoaded = false;
		this.LoadDataFromObj(JSON.parse(string));
	}

	/**
	 * loads information regarding what resources need to be loaded from a data object into the 
	 * resource pack
	 * @param {Object} obj 
	 */
	LoadDataFromObj(obj){
		this._isLoaded = false;
		this.rawData = obj;
		this.LoadData();
	}

	/** loads the information from the resource pack raw data*/
	LoadData(){
		
		// set the asset pack key to what it is according to the file
		if(this.rawData.assetPack != null)
			this.key = this.rawData.assetPack;

		// push the asset pack to the library
		if(AssetPack.library._assetPacks == null)
			AssetPack.library._assetPacks = [];
		AssetPack.library._assetPacks.push(this);
		
		// make sure that the loading flag is properly set
		this._isLoaded = false;
		this._nonLoadedAssets = [];
		let data = this.rawData;

		// load all the spritesheets
		if(data.spriteSheets != null){
			
			// ensure there is a spritesheet collection in the library
			if(this.library.spriteSheets == null)
				this.library.spriteSheets = {};

			// iterate through each spritesheet data
			for(let i = 0; i < data.spriteSheets.length; i++){

				if(this._relativePath != null)
					data.spriteSheets[i].source = IO.GetRelativePath(data.spriteSheets[i].source, this._relativePath);

				// load the spritesheet from the object data
				let asset = SpriteSheetAsset.FromData(data.spriteSheets[i]);
				this.library.spriteSheets[asset.key] = asset;

				// TODO load individual sprites but only after spritesheet image data is 
				// finished loading
				let ths = this;
				asset.OnLoad(function(){
					asset.LoadIndividualSpritesToAssetPack(ths);
				});

				// push the asset to non loaded asset array since images never load immediately
				this._nonLoadedAssets.push(asset);

				// add the asset to the pack's asset list
				this.selfAssets.push(asset);
			}
		}

		// load all the collectibles and items
		if(data.collectibles != null){

			// ensure there is a collectible collection in the library
			if(this.library.collectibles == null)
				this.library.collectibles = {};

			// iterate through each collectible data
			for(let i = 0; i < data.collectibles.length; i++){
				let asset = CollectibleAsset.FromData(data.collectibles[i]);
				this.library.collectibles[asset.key] = asset;

				// if the asset was not fully loaded, add it to be loaded later
				if(!asset._isLoaded){
					this._nonLoadedAssets.push(asset);
				}
				
				// add the asset to the pack's asset list
				this.selfAssets.push(asset);
			}
		}

		// check to see if the assets have fully loaded
		this.Verify();
	}

	/** loads the non loaded assets if any */
	ContinueLoading(){
		let ths = this;
		window.setTimeout(function(){
			ths.Verify();
		},0);
	}

	/** checks to see if all the assets in the pack have been loaded */
	Verify(){

		// iterate through each non loaded asset
		if(this._nonLoadedAssets != null){
			this._isLoaded = false;
			for(let i = this._nonLoadedAssets.length - 1; i >= 0; i--){

				// verify the asset loading state
				let asset = this._nonLoadedAssets[i];

				// if the asset is not loaded, retry the loading
				if(!asset._isLoaded){
					// TODO reload
					continue;
				}

				// if the asset is loaded, remove it from the non loaded assets list
				else
					this._nonLoadedAssets.splice(i, 1);
			}
		}

		// if all the assets are loaded, set the isLoaded flag
		if(this._nonLoadedAssets == null || this._nonLoadedAssets.length <= 0){
			this._nonLoadedAssets = null;
			this._isLoaded = true;
			this._onFinishedLoading();
			return;
		}

		// add event listener to check again later if the asset pack is not yet fully loaded
		this.ContinueLoading();
	}

	/** 
	 * unloads the resource pack and it's resources so that it can be collected by the
	 * javascript Garbage Collector
	 */
	Unload(){

		// TODO implement
		throw "not implemented";
	}

	/** @param {Function} func */
	OnFinishedLoading(func){
		this._onFinishedLoadingCallbacks.push(func);
	}

	/** @private */
	_onFinishedLoading(){

		// iterate through each callback and call the function
		for(let i = this._onFinishedLoadingCallbacks.length - 1; i >= 0; i--){
			this._onFinishedLoadingCallbacks[i]();
		}
	}
}

class Asset{

	constructor(){
		
		/** @type {String} */
		this.key = null;

		/** @type {String} */
		this.name = "";

		/** @type {ClassDecorator} */
		this.classType = null;
		
		/** @type {Object} */
		this._rawData = null;

		/** @type {Object} type of this.class */
		this._globalInstance = null;

		/** @type {Boolean} */
		this._isTyped = false;

		/** @type {Boolean} */
		this._isLoaded = false;

		/** @type {Boolean} */
		this._isBusy = false;

		/** @type {Array<Function>} */
		this._onLoad = null;
	}

	/**
	 * @type {Asset} creates an asset from the specified values
	 * @param {String} key 
	 * @param {String} name 
	 * @param {Object} classType 
	 * @param {Object} instance of type classType
	 */
	static Create(key, name, classType, instance){
		let r = new Asset();

		r.key = key;
		r.name = name;
		r.classType = classType;
		r._globalInstance = instance;
		r.HandleLoadState();

		return r;
	}

	/**
	 * @type {Asset} creates an asset from the specified object data
	 * @param {Object} data 
	 */
	static FromData(data){

		// create a new asset and set its fields
		let r = new Asset();
		r.ProcessData(r, false);
		r.HandleLoadState();
		return r;
	}

	/** 
	 * @type {Asset} 
	 * @param {Object} aType the data for fetching the asset
	 * @param {Boolean} strict if false, a null asset can be returned
	 */
	static FindJSONAsset(aType, strict = true){
		
		// if it's an asset, the type needs to be gotten from another asset
		if(aType.isAsset){

			// parse the destination from string in format AssetPack/Collection/AssetKey
			/** @type {String} */
			let dest = aType.destination;
			let splDest = dest.split('/');

			if(splDest.length < 3)
				throw "invalid destination format: " + dest;

			let pak = AssetPack.library[splDest[0]]
			if(pak == null){
				if(!strict) return null;
				throw "asset pack '" + splDest[0] + "' does not exist";
			}

			let collection = pak[splDest[1]];
			if(collection == null){
				if(!strict) return null;
				throw (
					"collection '" + splDest[1] + "' does not exist inside asset pack '" +
					splDest[0] + "'"
				);
			}

			let asset = collection[splDest[2]];
			if(asset == null && strict)
				throw (
					"asset '" + splDest[2] + "' does not exist inside collection '" +
					splDest[1] + "' or asset pack '" + splDest[0] + "'"
				);

			// return the asset
			return asset;
		}

		throw "specified type is not an asset";
	}

	/** @type {Object} */
	static FindJSONInstance(aType, strict = true){
		
		// if it's an asset, the instance needs to be gotten from another asset
		if(aType.isAsset){
			return Asset.FindJSONAsset(aType, strict).globalInstance;
		}

		// if the type is not an asset:
		// looks confusing but actually simple, basically looking at rType.destination which 
		// is a class name and calling eval on a function made from a string that returns 
		// that class type
		return (
			new Function("return " + aType.destination)
		)();
	}

	/** @type {Object} */
	static FindJSONAssetType(aType, strict = true){

		// if it's an asset, the type needs to be gotten from another asset
		if(aType.isAsset){

			// return the asset class type
			return Asset.FindJSONAsset(aType, strict).classType;
		}

		// if the type is not an asset:
		// looks confusing but actually simple, basically looking at rType.destination which 
		// is a class name and calling eval on a function made from a string that returns 
		// that class type
		return (
			new Function("return " + aType.destination)
		)();
	}

	/** @type {Boolean} */
	get isLoaded(){ return this._isLoaded; }

	/** @type {Object} */
	get globalInstance(){

		if(!this._isLoaded)
			throw "Cannot use asset while it's still loading";

		if(this._globalInstance == null)
			this._globalInstance = this.CreateInstance();

		return this._globalInstance;
	}

	/**
	 * @param {Object} data 
	 */
	ProcessData(data){
		this._rawData = data;
		this.name = data.name;
		this.key = data.key;
		this.classType = Asset.FindJSONAssetType(data.class, false);
	}

	/** if verification fails the asset is not loaded properly */
	Verify(){

		// if still loading, return false
		if(this._isBusy){
			return this._isLoaded = false;
		}

		// if this check fails, the asset is not loaded
		return this._isLoaded = (
			this.classType != null
		);
	}

	/** handles asyncronous continuation of verification */
	HandleLoadState(){
		
		// if asset is actively loading
		if(this._isBusy){
			
			// queue up another function call when thread is free to check on the loadstate of 
			// the asset
			let ths = this;
			setTimeout(function(){
				ths.HandleLoadState();
			}, 0);
		}

		// if the asset has stopped loading, verify the loading is complete
		else if(!this.Verify()){

			// reload if the verification failed
			this.ProcessData(this._rawData);
			
			// queue up another function call when thread is free to check on the loadstate of 
			// the asset
			let ths = this;
			setTimeout(function(){
				ths.HandleLoadState();
			}, 0);
		}

		// if the asset passes verification, it is done loading
		else {
			this._isLoaded = true;

			// if there are any functions in the OnLoad callback array, call them
			if(this._onLoad != null){
				for(let i = this._onLoad.length - 1; i >= 0; i--){
					this._onLoad[i]();
				}
				this._onLoad = null;
			}
		}
	}

	/**
	 * Checks to see if this asset inherits from the specified type
	 * @param {Object} type 
	 */
	InheritsFrom(type){
		return ((new this.classType()) instanceof type);
	}

	/** @type {Object} type this.classType; creats an instance of this asset */
	CreateInstance(){
		
		if(!this._isLoaded)
			throw "Cannot use asset while it's still loading";

		return null;
	}

	/** @param {Function} func */
	OnLoad(func){

		// ensure the callback array exists
		if(this._onLoad == null)
			this._onLoad = [];

		// add the function to the callback array
		this._onLoad.push(func);
	}
}

// ------------------------------------------------------------------------------------------------

class SpriteSheetAsset extends Asset{
	constructor(){
		super();

		/** @type {CanvasImageSource} */
		this.image = null;

		/** @type {Array<Asset>} */
		this.spriteAssets = [];
	}

	/** @type {SpriteSheetAsset} */
	static FromData(data){
		let r = new SpriteSheetAsset();
		r.ProcessData(data, false);
		r.HandleLoadState();
		return r;
	}

	/**
	 * @type {Array<Sprite>}
	 * @param {SpriteSheet} sheet 
	 * @param {Array<String>} keys 
	 */
	static ExtractSpriteAssets(sheet, defaultKey, keys = []){

		// create array to hold assets
		let r = [];

		// create assets from all the sprites in the sprite sheet
		for(let i = 0; i < sheet.sprites.length; i++){
			let sprt = sheet.sprites[i];
			let key = keys[i] || (defaultKey + '_' + i.toString());
			let asset = Asset.Create(key, key, Sprite, sprt);
			r.push(asset);
		}

		// return the sprite assets
		return r;
	}

	/** @type {Boolean} */
	get isLoaded() { return this._isLoaded; }

	/** @override */
	ProcessData(data){
		super.ProcessData(data);

		// sprites are loaded when image is done loading
		this.LoadImage(data.source, data.sprites);
	}

	/** @override */
	Verify(){
		return this._isLoaded = (
			super.Verify() &&
			this.image.width > 0 &&
			this.image.height > 0
		);
	}
	
	/**
	 * generate the spritesheet's sprites from the JSON data
	 * @param {Object} data 
	 */
	LoadSprites(data){

		// create the spriteSheet object from the image
		/** @type {SpriteSheet} */
		this._globalInstance = SpriteSheet.FromImage(this.image);

		// if the sprites are supposed to be auto generated
		if(data.generate){

			// get the sprite size from the data, or if not specified, set it to image size
			let size = null;
			if(data.size != null)
				size = Vec2.FromArrayData(data.size);
			else
				size = new Vec2(this.image.width,this.image.height);

			// get the pivot from data
			let pivot = null;
			if(data.pivot != null)
				pivot = Vec2.FromArrayData(data.pivot);

			// generate the sprites from the specified size and pivot
			this._globalInstance.GenerateSprites(size, pivot);
		}

		// if the sprites are manually specified in the data
		else{
			
			// create an array to hold the sprites that are defined in the data
			let sprites = [];

			// iterate through each manually specified sprite
			let arr = data.data;
			for(let i = 0; i < arr.length; i++){

				// create the sprite from the data
				let sprData = arr[i];
				let sprite = Sprite.FromBounds(this._globalInstance, Rect.FromJSONArray(sprData.rect));

				// set the pivot manually if specified
				if(sprData.pivot != null){
					sprite.pivot = Vec2.FromArrayData(sprData.pivot);
				}

				// add the sprite to the sprite array
				sprites.push(sprite);
			}

			// assign the sprite array to the spritesheet
			this._globalInstance.sprites = sprites;
		}
		
		// get the sprite assets from the generated sprites
		let keys = data.keys || [];
		let spriteAssets = SpriteSheetAsset.ExtractSpriteAssets(this._globalInstance, this.key, keys);
		this.spriteAssets = spriteAssets;
	}

	/** @param {AssetPack} pack */
	LoadIndividualSpritesToAssetPack(pack){

		// ensure there is a sprite collection in the pack
		if(pack.library.sprites == null)
			pack.library.sprites = {};

		// iterate through each sprite asset
		for(let i = 0; i < this.spriteAssets.length; i++){
			let sprtAss = this.spriteAssets[i];
			pack.library.sprites[sprtAss.key] = sprtAss;

			// add the sprite asset to the pack's asset list
			pack.selfAssets.push(sprtAss);
		}
	}

	LoadImage(source, spriteData){

		// mark the asset busy
		this._isBusy = true;

		// create the image element
		this.image = new Image();
		
		// set function callback for when image data is finished loading
		let ths = this;
		this.image.onload = function(){
			
			// load the sprite data
			if(spriteData != null)
				ths.LoadSprites(spriteData);

			// set the busy flag to signify it is done loading
			ths._isBusy = false;
		}

		// load the image data from the source
		this.image.src = source;
	}
}

class CollectibleAsset extends Asset{
	constructor(){
		super();

		/** @type {Asset} */
		this.icon = null;

		/** @type {Array<Object>} */
		this.recipes = null;

		/** @type {Array<Object>} */
		this.components = null;

		/** @type {Boolean} */
		this._isItem = false;
	}

	/**
	 * @type {CollectibleAsset} creates an asset from the specified object data
	 * @param {Object} data 
	 */
	static FromData(data){

		// create a new Asset and set its fields
		let r = new CollectibleAsset();
		r.ProcessData(data, false);
		r.HandleLoadState();
		// return the asset
		return r;
	}

	/**
	 * @type {Object} creates a recipe object from the recipe data
	 * @param {Object} data 
	 * @param {Boolean} strict whether or not an error should be thrown if an asset cannot be 
	 * 	loaded
	 */
	static ParseRecipe(data){
		
		let recipe = {};
		recipe.ingredients = [];

		// iterate through each ingredient in the recipe data
		for(let i = 0; i < data.input.length; i++){

			// create a reference to the ingredient required for the recipe
			let ingrData = data.input[i];
			let ingrAsset = Asset.FindJSONAsset(ingrData, false);
			
			// if the asset has not yet been loaded, return null
			if(ingrAsset == null || !ingrAsset._isLoaded){
				return null;
			}

			let ingr = ingrAsset.CreateInstance();
			ingr.amount = ingrData.amount;

			// add the ingredient to the recipe
			recipe.ingredients.push(ingr);
		}

		// create the required recipe fields
		recipe.output = {};
		recipe.output.amount = data.amount;
		recipe.output.byproducts = [];
		
		// if there are any byproducts from the recipe
		if(data.output.byproducts != null){
			for(let i = 0; i < data.output.byproducts.length; i++){

				// create a reference to the byproduct item
				let bpData = data.output.byproducts[i];
				let bpAsset = Asset.FindJSONAsset(bpData, false);

				// if the asset has not yet been loaded, return null
				if(bpAsset == null){
					return null;
				}

				let bp = bpAsset.CreateInstance();
				bp.amount = bpData.output;

				// add the byproduct to the recipe
				recipe.output.byproducts.push(bp);
			}
		}

		// add the recipe to the asset
		return recipe;
	}

	/** 
	 * @override 
	 * @param {Object} data 
	 * @param {Boolean} strict whether or not an error should be thrown if an asset cannot be 
	 * 	loaded
	 */
	ProcessData(data){
		super.ProcessData(data);
		
		if(data.icon != null){
			this.icon = Asset.FindJSONAsset(data.icon, false);

			// if icon could not be loaded, return with null icon to try again later
			if(this.icon == null){
				return;
			}

			// if the icon is not loaded, mark this asset as busy and return to try again later
			else if(!this.icon.isLoaded){
				this._isBusy = true;

				// make sure the icon reports back and marks this asset as not busy when it's 
				// finished loading
				let ths = this;
				this.icon.OnLoad(function(){
					ths._isBusy = false;
				});
				return;
			}
		}

		// create lists for recipes and components
		this.recipes = [];
		this.components = [];

		// if there is a recipes field, add the recipes
		if(data.recipes != null){

			// iterate through each recipe and add it
			for(let i = 0; i < data.recipes.length; i++){

				// parse the recipe
				let recipe = CollectibleAsset.ParseRecipe(data.recipes[i], false);

				// if the recipe could not be parsed, flag for reload to try again later, as
				// this is usually caused due to the recipe depending on an asset in the asset
				// pack that has not yet been loaded
				if(recipe == null){
					this.recipes = null;
					return;
				}

				this.recipes.push(recipe);
			}
		}

		// if there is a components field
		if(data.components != null){
			for(let i = 0; i < data.components.length; i++){
				
				// find a reference to the component asset
				let compAsset = Asset.FindJSONAsset(data.components[i], false);
				
				// if the asset is not yet loaded, flag for reload
				if(compAsset == null){
					this.components = null;
					return;
				}

				let component = compAsset.CreateInstance();
				component.amount = data.components[i].amount;

				// add the component to the array
				this.components.push(component);
			}
		}

		// determine if the resource is an item or not
		this._isItem == this.InheritsFrom(Item);
	}

	/** @override */
	Verify(){
		return this._isLoaded = (
			super.Verify() &&
			this.icon != null &&
			this.icon.isLoaded &&
			this.recipes != null &&
			this.components != null
		);
	}

	/** @override */
	CreateInstance(){

		if(!this._isLoaded)
			throw "Cannot use asset while it's still loading";
		
		/** @type {Collectible} */
		let r = new (this.classType)();

		// set the fields
		r.name = this.name;
		r.icon = this.icon.globalInstance;
		
		// set the corresponding resource or item fields
		if(this._isItem) this.SetItemFields(r);
		else this.SetResourceFields(r);

		// TODO set recipes and components??

		return r;
	}

	/**
	 * @param {Resource} col 
	 */
	SetResourceFields(col){
		col.key = this.key;
	}

	/**
	 * @param {Item} col 
	 */
	SetItemFields(col){
		col._oid = Item.nextOID;
	}
}

class WeaponAsset extends Asset{

	constructor(){
		super();

		this.classType = Weapon;
	}

	/** @override */
	ProcessData(data){
		super.ProcessData(data);

		// TODO weapons specific stuff
	}
}