///
///		code by Isaiah Smith
///		
///		https://technostalgic.tech  
///		twitter @technostalgicGM
///

class IO{

	/**@type {String} returns the folder that index.html exists in */
	static get location(){

		// if location has not been precomputed
		if(IO._location == undefined){

			// get the url
			let loc = window.location.href;

			// truncate the 'index.html' part of the path
			IO._location = loc.substring(0, loc.lastIndexOf('/'));
		}

		// return the result
		return IO._location;
	}

	/**
	 * @type {String} returns a local path given an absolute path
	 * @param {String} absolutePath
	 */
	static GetLocalPath(absolutePath){
		return absolutePath.replace(IO.location, '.');
	}

	/**
	 * Converts a local path relative to the window, into a path relative to the reference
	 * @param {String} target the target path to contvert
	 * @param {String} reference the reference path to get the local path from
	 */
	static GetRelativePath(target, reference){
		
		// convert the reference path into an absolute path if it isn't already
		reference = IO.GetAbsolutePath(reference);

		// truncate reference filename
		reference = reference.substring(0, reference.lastIndexOf('/'));

		// return the target file from the reference frame
		return target.replace('.', reference);
	}

	static GetRelativeLocalPath(target, reference){

		target = IO.GetAbsolutePath(target);
		reference = IO.GetAbsolutePath(reference);

		target = target.replace(IO.GetCommonDirectory(target, reference), '.');
		return target;
	}

	/**
	 * Returns the absolute path of the comon directory between the two specified paths
	 * @param {String} pathA 
	 * @param {String} pathB 
	 */
	static GetCommonDirectory(pathA, pathB){

		// while path b is not inside of path a
		while (pathA.indexOf(pathB) < 0){
			
			let ind = pathB.lastIndexOf('/')
			if(ind < 0) ind = pathB.lastIndexOf('\\');

			if(ind < 0) return null;

			pathB = pathB.substring(0, ind);
		}

		return pathB;
	}

	/**
	 * converts a local path to an absolut path
	 * @param {String} localPath 
	 */
	static GetAbsolutePath(localPath){

		//TODO optomize???
		let surrogate = new Image();

		// get absolute path of reference
		surrogate.src = localPath;
		let abs = surrogate.src;

		// prevent any unneccesary file loads
		surrogate.src = "";

		return abs;
	}

	/**
	 * prompts a download for a text file 'filename' that contains the specified data
	 * @param {string} filename - the name of the file that will be downloaded 
	 * @param {string} data - the data that is contained in the file
	 */
	static DownloadDataFile(filename, data) {

		var blob = new Blob([data], { type: 'text/csv' });

		if (window.navigator.msSaveOrOpenBlob) {
			window.navigator.msSaveBlob(blob, filename);
		}

		else {
			var elem = window.document.createElement('a');
			elem.href = window.URL.createObjectURL(blob);
			elem.download = filename;
			document.body.appendChild(elem);
			elem.click();
			document.body.removeChild(elem);
		}
	}

	/**
	 * opens a file dialogue to locate saved data files, note: must be called from a user triggered event (ie "click")
	 * @param {function(string)} onload the function that is called when the file is loaded, with the file's contents passed as the parameter
	 * @param {string} accept the file extentions that are accepted by the file dialogue 
	 */
	static UploadDataFile(onload, accept = null) {

		// create an input element for the file 
		var elem = document.createElement("input");
		elem.type = "file";
		elem.accept = accept;

		// simulate a click on the element to open the file dialogue
		elem.click();

		elem.addEventListener("change", function () {

			// define the file reader
			let reader = new FileReader();

			// set a callback that calls 'onload' when the file is loaded
			reader.onload = function () { onload(reader.result); };

			// load the file as text
			reader.readAsText(elem.files[0]);
		});
	}

	/**
	 * @type {XMLHttpRequest} Loads the specified file
	 * @param {string} path the path/url to the file that you are trying to load
	 * @param {function(string, XMLHttpRequest)} onload the function that is called when the file is loaded, with the file's contents passed as the first parameter
	 * @param {function(XMLHttpRequest)} onerror the function that is called if there is an error loading the file, with the request passed as a parameter
	 */
	static LoadDataFile(path, onload, onerror = function(){ }){

		// create a new request for the file
		let request = new XMLHttpRequest();

		// open the request to the specified file
		request.open('GET', path);

		// set the callback for when the file is loaded
		request.onload = function(){ 
			onload(request.responseText, request); 
		};

		// set the callback for if the file can't be loaded
		request.onerror = function(){
			onerror(request);
		};
		
		// send the file request
		request.send();

		// return the request object
		return request;
	}

	/**
	 * @type {Image}
	 * @param {String} path 
	 * @param {Function} onload 
	 * param {Function} onerror 
	 */
	static LoadImageFile(path, onload = null){

		let r = new Image();
		r.src = path;

		if(onload != null)
			r.addEventListener("load", onload);

		return r;
	}
	
	/**
	 * @type {Image}
	 * @param {String} path 
	 * @param {Function} onload 
	 * param {Function} onerror 
	 */
	static LoadAudioFile(path, onload = null){

		let r = new Audio(path);

		if(onload != null)
			r.addEventListener("load", onload);

		return r;
	}

	/**
	 * @type {String} retrieves an item from the browser's local storage at the specified key
	 * @param {String} key the key of the data item to retrieve 
	 */
	static GetLocalStorageItem(key){

		let r = "";

		try{
			r = localStorage.getItem(key);
		} 
		catch(e){
			console.error(e);
		}

		return r;
	}

	/**
	 * attempts to store an item in the browser's local storage at the specified key
	 * @param {String} key 
	 * @param {String} data 
	 */
	static StoreLocalStorageItem(key, data){

		try{
			localStorage.setItem(key, data);
		}
		catch(e){
			console.error(e);
		}
	}
}