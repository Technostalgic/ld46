///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class GameObjectContainer{

	constructor(){

		/**@type {SpacialPartitioning} */
		this.objectPartitions = new SpacialPartitioning();
		/**@type {SpacialPartitioning} */
		this.collisionPartitions = new SpacialPartitioning();

		/**@type {Array<SimpleSoundInstance>} */
		this.queuedSounds = [];
		/**@type {Array<GameObject>} */
		this.alwaysUpdate = [];
		/**@type {Array<GameObject>} */
		this.alwaysRender = [];

		/** @type {HTMLCanvasElement} */
		this.minimapCanvas = null;
		/** @type {CanvasRenderingContext2D} */
		this.minimapContext = null;
		/** @type {Vec2} */
		this.minimapOrigin = new Vec2();
		/** @type {Number} */
		this.minimapRange = 600;

		/** @type {GameObjectState} */
		this.parentState = null;
		/** @type {Number} */
		this.timeSimulated = 0;

		/** @type {Number} for debug and benchmarking */
		this.activeObjects = 0;
		/** @type {Number} for debug and benchmarking */
		this.totalObjectIterations = 0;
	}

	AddScore(points){ 
		this.parentState.AddScore(points);
	}

	/**
	 * @returns @type {Array<ColliderRaycastResult>} tests for raycast intersections with all the 
	 * colliders currently inside container, and returns an array of raycast intersection results
	 * @param {Ray} ray
	 */
	Raycast(ray){

		// create self reference for callback function
		let ths = this;

		// create an array to hold all the colliders hit by the raycast
		let r = [];
		let rayBox = Rect.FromPoints(ray.rayStart, ray.rayEnd);

		// iterate through each collider that could possible be hit and call the function defined
		// below
		this.collisionPartitions.IterateThroughWorldRectObjects(rayBox, 
			/** @param {Collider} col */
			function(col){

				// for debug and benchmarking
				ths.totalObjectIterations++;

				// cast the ray against the collider's shape and add it to the return array if hit
				let result = CollisionShape.RayCast(col.shape, ray);
				if(result.didEnter){
					r.push(ColliderRaycastResult.FromResult(col, result));
				}
			}
		);

		// return the array of colliders that were hit
		return r;
	}

	/**
	 * @returns @type {Array<ColliderRaycastResult>} tests for circlecast intersections with all 
	 * the colliders currently inside container, and returns an array of circlecast intersection 
	 * results
	 * @param {Ray} ray 
	 * @param {Number} radius the radius of the circle to be casted
	 */
	Circlecast(ray, radius){
		
		// create self reference for callback function
		let ths = this;

		// create an array to hold all the colliders hit by the raycast
		let r = [];
		let rayBox = Rect.FromPoints(ray.rayStart, ray.rayEnd).Expanded(radius);

		// iterate through each collider that could possible be hit and call the function defined
		// below
		this.collisionPartitions.IterateThroughWorldRectObjects(rayBox, 
			/** @param {Collider} col */
			function(col){
				
				// for debug and benchmarking
				ths.totalObjectIterations++;

				// cast the ray against the collider's shape and add it to the return array if hit
				let result = CollisionShape.CircleCast(col.shape, ray, radius);
				if(result.didEnter){
					r.push(ColliderRaycastResult.FromResult(col, result));
				}
			}
		);

		return r;
	}

	/**
	 * @returns @type {Array<Collider>} finds each collider that is overlapping the specified shape
	 * and return an array of all the colliders that are
	 * @param {CollisionShape} shape the shape to check each collider against
	 */
	FindOverlappingColliders(shape){

		// create an array to store all the colliders that overlap the shape
		let r = [];

		// iterate through each collider in the container that could possibly be colliding with 
		// the shape and call the function defined below for each collider
		this.collisionPartitions.IterateThroughWorldRectObjects(shape.boundingBox, 
			/** @param {Collider} col */
			function(col){

				// if the shape and collider are overlapping, add the collider to the return array
				if(shape.OverlapsShape(col.shape))
					r.push(col);
			}
		);

		// return the array of colliders that are overlapping the shape
		return r;
	}

	/**
	 * @param {GameObject} obj 
	 * @param {Vec2} position
	 */
	AddGameObject(obj, position = null){

		if(position != null)
			obj.position = position.clone;
		
		obj.container = this;
		obj.UpdateBounds();
		obj.updateSpatialPartitionRange();
		obj.onAdded(this);

		// always update the object if specified. Even if it's out of range
		if(obj.alwaysUpdate)
			this.alwaysUpdate.push(obj);
		
		// always render the object if specified, even if out of range
		if(obj.alwaysRender)
			this.alwaysRender.push(obj);
	}

	/**
	 * @param {Array<GameObject>} objs 
	 */
	AddGameObjects(objs){

		for(let i = 0; i < objs.length; i++){
			this.AddGameObject(objs[i]);
		}
	}

	/**
	 * @param {GameObject} obj 
	 */
	RemoveGameObject(obj){
		obj.isRemoved = true;
		obj.RemoveFromPartitions();
	}

	/**
	 * Updates all the objects detectable by the radar and inside the view rect
	 * @param {Number} dt the delta time to update the object by
	 * @param {Rect} viewportRect the viewport rect that will be rendered during the draw cycle
	 */
	UpdateGameObjects(dt, viewportRect){

		// reset the active objects counter
		this.activeObjects = 0;

		// update each of the "alwaysUpdate" objects
		for(let i = this.alwaysUpdate.length - 1; i >= 0; i--){
			
			//update the object
			this.alwaysUpdate[i].Update(dt);
			
			// for debugging
			this.activeObjects++;
			this.totalObjectIterations++;

			// if it's been destroyed, remove it
			if(this.alwaysUpdate[i].isRemoved)
				this.alwaysUpdate.splice(i, 1);
		}

		// calculate the world rect that contains all the space displayed on the radar.
		let updateRect = Rect.FromPoints(
			new Vec2(
				this.minimapOrigin.x - this.minimapRange, 
				this.minimapOrigin.y - this.minimapRange),
			new Vec2(
				this.minimapOrigin.x + this.minimapRange,
				this.minimapOrigin.x + this.minimapRange)
		);

		// expand the update rect to include the area that is going to be rendered on the viewport
		updateRect = Rect.OverlapRect(updateRect, viewportRect);

		// clear the minimap if it exists
		if(this.minimapCanvas != null){
			this.clearMiniMapCanvas();
		}

		// define this to have a reference to it in the anonymous function
		let ths = this;

		// iterate through each game object in the rect and update it
		this.objectPartitions.IterateThroughWorldRectObjects(updateRect, 
			/** @param {GameObject} obj */
			function(obj){
				
				// don't update if it's subscribed to always update, since it's already been updated
				if(obj.alwaysUpdate)
					return;

				// for debugging
				ths.activeObjects++;
				ths.totalObjectIterations++;

				// update the object
				obj.Update(dt);
			}
		);

		// increment the amount of time that's been simulated in this container
		this.timeSimulated += dt;
	}

	/**
	 * Renders all the objects in the camera's viewport
	 * @param {Camera} camera 
	 */
	DrawGameObjects(camera){

		// define an array to store objects that need to be rendered
		let renderQueue = [];

		// render each of the "alwaysRender" objects
		for(let i = this.alwaysRender.length - 1; i >= 0; i--){

			// push the object to the render queue
			renderQueue.push(this.alwaysRender[i]);

			// if the object it destroyed, remove it
			if(this.alwaysRender[i].isRemoved)
				this.alwaysRender.splice(i, 1);
		}

		// define the render rect as the camera's viewport rect expanded by 25 pixels 
		// in each direction
		let renderRect = camera.viewRect.clone;
		renderRect.AddPadding(25, 25);

		// iterate through all objects in the camera's viewport
		this.objectPartitions.IterateThroughWorldRectObjects(renderRect, 
			/** @param {GameObject} obj */
			function(obj){
				
				// don't render if it's subscribed to always render, since it's already been rendered
				if(obj.alwaysRender)
					return;

				// add the object to the render queue
				renderQueue.push(obj);
			}
		);

		// sort the renderQueue so that the object with the highest renderOrder is rendered last
		renderQueue.sort(function(a, b){
			return b.renderOrder - a.renderOrder;
		});

		// iterate through each object in the render queue
		for(let i = renderQueue.length - 1; i >= 0; i--){
			
			// render the object to the camera
			renderQueue[i].Draw(camera);
			
			// for debug
			this.totalObjectIterations++;
		}
	}

	/**
	 * Queues a sound to later be played back by a camera object
	 * @param {HTMLAudioElement} audio the audio element that the sound effect consists of
	 * @param {Number} volume the base volume of the audio playback
	 * @param {Vec2} position the origin of where the sound occured in world space
	 */
	QueueSimpleSound(audio, position = null, volume = 1){

		let sound = new SimpleSoundInstance(audio, position, volume);
		this.queuedSounds.push(new SimpleSoundInstance(audio, position, volume));
		return sound;
	}

	/**
	 * plays all the queued sounds through a camera
	 * @param {Camera} camera the camera object to play the sounds through
	 * @param {Boolean} clearQueue whehter or not the queue should be cleared after the sounds 
	 * are played
	 */
	PlayQueuedSounds(camera, clearQueue = true){

		// do nothing if there are no queued sounds
		if(this.queuedSounds.length <= 0)
			return;

		// iterate through each queued sound instance and play it through the specified camera
		for(let i = this.queuedSounds.length - 1; i >= 0; i--){
			camera.PlaySoundInstance(this.queuedSounds[i]);
		}

		// clear the queue if specified
		if(clearQueue){
			this.queuedSounds.length = 0;
		}
	}
}