///
/// 	code by Isaiah Smith
///
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/** a data type used for storing a single or a sequence of sprites on a spritesheet */
class SpriteSheet {

	/**
	 * creates a sprite object
	 * @param {CanvasImageSource} image the image to use for the spritesheet
	 */
	constructor(image, sprites = []){
		
		/**@type {CanvasImageSource} */
		this.image = image;

		/**@type {Array<Sprite>} */
		this.sprites = sprites;
	}

	/**
	 * @type SpriteSheet
	 * @param {CanvasImageSource} image 
	 */
	static FromImage(image){

		let r = new SpriteSheet(image);
		r.sprites = [
			Sprite.FromBounds(r, new Rect(
				new Vec2(0, 0),
				new Vec2(image.width, image.height)
			))
		];

		return r;
	}

	/**
	 * @type {Sprite} creates a sprite from the specified sprite frame index
	 * @param {Number} index the index of the sprite frame on the sheet
	 * @param {Boolean} flipped whether or not the sprite is flipped horizontally
	 */
	GetSprite(index, flipped = false){
		return Sprite.FromSheetIndex(this, index, flipped);
	}

	/**@type {Sprite} */
	SpriteAt(index){
		return this.sprites[index];
	}

	/** 
	 * generates frames by splitting the image into frames of the specified size
	 * @param {Vec2} spriteSize
	 * @param {Vec2} spritePivot
	 */
	GenerateSprites(spriteSize, spritePivot = null){

		// ensure that the frame size is non-zero
		if(spriteSize.x === 0 || spriteSize.y === 0){
			return;
		}

		// define the sprite list and iteration rectangle
		let f = [];
		let trect = new Rect(Vec2.zero, spriteSize);

		// iterate from top to bottom by increments of frameSize.y
		while(trect.bottom <= this.image.height){

			// iterate from left to right by increments of frameSize.x
			while(trect.right <= this.image.width){

				// define the sprite frame
				let frame = Sprite.FromBounds(this, trect.clone);

				// set the pivot if necessary
				if(spritePivot != null){
					frame.pivot = spritePivot.clone;
				}

				// add a clone of the rect to the frame list
				f.push(frame);

				// move the iteration rectangle to the right
				trect.position.x += spriteSize.x;
			}

			// move the iteration rectangle downward
			trect.position.y += spriteSize.y;

			// reset horizontal frame position
			trect.position.x = 0;
		}

		// assign the new frame array to the proper field
		this.sprites = f;
	}

	/** 
	 * renders the sprite centered at the specified position
	 * @param {CanvasRenderingContext2D} ctx the render context to draw the image with
	 * @param {Vec2} position the position of the center of the sprite
	 */
	Render(ctx, frameIndex, position){

		let frame = this.SpriteAt(frameIndex);
		let tpos = position.Minus(this.SpriteAt().size.Times(0.5));
		ctx.drawImage(this.image, frame.left, frame.top, frame.width, frame.height, tpos.x, tpos.y, frame.width, frame.height);
	}

	/** 
	 * renders the sprite at the specified rect target
	 * @param {CanvasRenderingContext2D} ctx 
	 * @param {Rect} target 
	 */
	RenderAtRect(ctx, frameIndex, target){

		let frame = this.SpriteAt(frameIndex);
		ctx.drawImage(this.image, frame.left, frame.top, frame.width, frame.height, target.left, target.top, target.width, target.height);
	}

	/** @returns {Object} */
	ToJSONObject(){
		let r = {};
		r.key = this.key;
		r.class = { isAsset: false, destination: this.constructor.name };
		r.source = this.image.src;
		r.sprites = {};
		r.sprites.data = [];
		r.sprites.keys = [];
		for(let i = 0; i < this.sprites.length; i++){
			r.sprites.data.push(this.sprites[i].ToJSONObject());
			r.sprites.keys.push(this.sprites[i].key || this.key + "_" + i);
		}
		return r;
	}
}

/** an object that can be used to animate a sprite */
class SpriteAnimation {
	
	/**
	 * An object used for animating sprites
	 * @param {SpriteSheet} sprite the sprite that will be animated
	 * @param {Array<Number>} frameIndices an array of numbers that represent the frame indices 
	 * @param {Number} animationSpeed the speed, in frames per second, that the animation plays at
	 */
	constructor(frameIndices, animationSpeed){

		/**@type {Array<Number>} */
		this._frameIndices = frameIndices;
		/**@type {Number} */
		this.animSpeed = animationSpeed;

		/**@type {Boolean} */
		this.loop = true;
		/**@type {Boolean} */
		this._isComplete = false;

		/**@type {Number} current frame index */
		this._index = 0;
	}

	/** @type {Boolean} whether or not the animation has reached the end at least once */
	get isComplete(){
		return this._isComplete;
	}

	/** @type {Number} the index of the animation (different from the index of the sprite frame) */
	get animationIndex(){
		return Math.floor(this._index);
	}

	/** @type {Number} the current index of the sprite's frame that should be displayed */
	get frameIndex(){
		return this._frameIndices[this.animationIndex];
	}

	/**
	 * resets the animation
	 */
	Reset(){
		this._frameIndex = 0;
		this._isComplete = false;
	}

	/**
	 * @type {Number} plays the animation and applies it to the sprite and returns the sprite's frame index based on the animation
	 * @param {Number} dt how much time has passed in the animation this frame
	 */
	Play(dt){

		// update the frame index
		if(!this.isComplete || this.loop){
			this._index += dt * this.animSpeed;
		}

		// if animation is finished
		if(this._index >= this._frameIndices.length){
			
			// set isComplete flag to true
			this._isComplete = true;

			// loop if set to do so
			if(this.loop){
				this._index = this._index % this._frameIndices.length;
			}

			// if not set to loop, max out at last frame
			else{
				this._index = this._frameIndices.length - 1;
			}
		}

		return this.frameIndex;
	}
}

/** an object used to store information about a sprite and other render information needed for it*/
class Sprite {

	constructor(){

		/** @type {SpriteSheet} */
		this.spriteSheet = null;

		/**@type {Rect} */
		this.bounds = new Rect();

		/**@type {Vec2} */
		this.pivot = new Vec2();

		/** @type {Boolean} */
		this.isFlipped = false;
	}

	/**
	 * @type {Sprite} creates a sprite out of raw image data
	 * @param {CanvasImageSource} graphic 
	 */
	static FromGraphic(graphic){
		let sheet = SpriteSheet.FromImage(graphic);
		return sheet.SpriteAt(0);
	}
	
	/**
	 * @type {Sprite}
	 * @param {SpriteSheet} sheet 
	 * @param {Rect} bounds 
	 */
	static FromBounds(sheet, bounds){
		let r = new Sprite();
		r.spriteSheet = sheet;
		r.bounds = bounds;
		r.pivot = bounds.size.Times(0.5);
		return r;
	}

	/** @type {CanvasImageSource} */
	get image() { return this.spriteSheet.image; }
	/** @type {Vec2} */
	get size() { return this.bounds.size; }
	/** @type {Number} */
	get width() { return this.size.x; }
	/** @type {Number} */
	get height() { return this.size.y; }

	/** @returns {Object} */
	ToJSONObject(){
		let r = {};
		r.rect = this.bounds.ToJSONArray();
		r.pivot = this.pivot.ToJSONArray();
		return r;
	}

	/**
	 * render the sprite profile
	 * @param {Camera} camera 
	 */
	Draw(camera, position, rotation = 0, flipped = false){
		camera.DrawSprite(this, position, rotation, flipped);
	}
}