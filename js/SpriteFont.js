///
///	code by Isaiah Smith
///		
///	https://technostalgic.tech  
///	twitter @technostalgicGM
///

/** an object used to create a font from a spritesheet of pre-drawn text characters */
class SpriteFont {
	
	/** initialize a new instance of a SpriteFont object */
	constructor() {

		this.fontName = "default";

		/** @type {Image} */
		this._spriteSheet = null;

		this.spriteTable = [];

		this.maxSpriteWidth = 0;
		this.maxSpriteHeight = 0;

		/** private */
		this._loaded = false;
	}

	static get default(){

		// generate the default font if it has not yet been created
		if(SpriteFont._default == null){
			SpriteFont._default = this.GenerateDefaultFont();
		}

		// return the default font
		return SpriteFont._default;
	}

	/** @type {SpriteFont} loads a spriteFont from a file path and returns it */
	static Load(/**@type {string}*/ path, /**@type {Function}*/ onFinishDelegate = null) {

		var r = new SpriteFont();
		r._onFinishDelegate = onFinishDelegate;

		var callback = function(data){
			r.LoadJSONData(data);
			r.LoadStatusCheck();
		};
		ContentPipeline.LoadJSONASync(path, callback);

		return r;
	}

	/** @type {SpriteFont} parses a JSON spritefont configuration into a spritefont object and returns the spritefont*/
	static FromJSONObject(data){
		var r = new SpriteFont();

		r.LoadJSONData(data);
		
		return r;
	}

	/** @type {SpriteFont} generates the default sprite font and returns it */
	static GenerateDefaultFont(){

		let r = new SpriteFont();

		// hope you don't have word wrap enabled
		// this is the image and sprite data and it's LOOONG
		let img = new Image();
		img.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAABICAYAAAA+hf0SAAAJYklEQVR4Xu1d6VYuKQzU939o59inYUJIqipAq5/X+2eOsmUpstLO+5v27+Pt7e0dTG3jbB47Da23Y9G8z999/ovoVOnK5inrlTmM/68YH+hESm3EfHx8fLy9v19TQ+G2cTKPMWfP8WcNY8E51/i1aKaT0c/4ZOuvgw1NmZwY/18xPvHylQBAVqIrsEnBKLITnQBNAgcA8K0/DeTuEjDavkKplTO+DQCToIwgo7HUAmS3/1awvYkN3OwG7wCgrw2AGynmO91EKIcTFuASwq3Q0Dc382wl4m7kYAGS2+pNraXdu4AJQCCGUQDCQKLsocyp3Obq3EcBwIhBFmDywUkskcUIu2ZYVUw2bwW81TihBbjDHWJCN+Mpj6csAKJl8u9BsDbcbgYAN85iACanHQAo4FP435XfMo8KAKyJZwdl4zQGAO5hML9BIDhE4IuZCAMBGleyAHVOpg9mAWD6jGSiAIAJRwUFjBNuLWfpJrvlQwywAALGIxtXLgkE0QLNk+tkaXo0rgBAYU4FAZt3qhC0Em1DH0/SSMZXV1agBAVcbH/GbzquAoAR8FvGdyqBOzJgCtzZG679A8Bjon2NjYdc+iYZBSKsH4DSG7VWj+r8TapZvT873wdR1fXNDSL+frrGrQw6/0O17OIurvkzP+ULMV7AaFwO8Lr2k3p/Qv+UhmX9AsY/GD+p/BV3wNaktYpPRckKEMDBSrHb44GVygpEVikd/Zul5Ij+o8pfyAaky9lK5T7bUgBwr8HNklYKzoo4YFwCoGEgtC5gfADCQwBgN1DJpGitwTCCZMDa4cM5pwAggQRV+KyWin0CpRTL6JNdWOAG2A1kZ7NxxYWxPZp4J1pPAUARAiISmWgbgK24kJT5e4BZoL7em08D2h0LoMhOCWIZCMJzTgBAZcCfBR+XFKpaigLVKhzz8WmxiLyYYi6A0gcsJAM4HD8NgHbY1KoF/Xrmo09YACpggb6dSqF0SUAAyCyAsn8IQiUNZD5W8VHMxyKUsvO76Qv8c+l27Kw/8N6AmXB7UZRL4+enLmDwceBRZXS7/VplDqs+skIQ65g9WchitKdKElyElWX1HCUGgRYgI/zv979cAlWk/XJx/HvsRb2AyxUmokDdMmT+h0Auaoma81CdPjuDBUnK+T0eWqDvxP6sz+J1Vfk5pW8KAhcCoVKQFuzP1rMgk40PQWJ2PntV3JEHeiXJHJamsgjej1d/hvxPaeA92zeF+qG7pdRbSJU0cRAgW787zt4jLkT7RwDQgBt8AOMzLF/LmORneQzrAOiWbAKAFX9opY+8qIncVwlATwJgce/p8kWPYo3OEACmYtzwC/CJFzLTkglXPi1D1geYYPn85PMx2YUsfl3EaiAVF3CJIXsVLXw5tQQAaELUdvIuADa6gRUTnJWCmZK6n0XfJu5agKSjKlmI6FO76/lHpEDnAipBBzPhuy6ArWfjjL6XBkCzDhX9hUGgC6ROAiBK40o3lAR53wUA+tkayzLsg43sA1RmAQQATC5ESQMlAOymSaqPJ2lcWr+w3ydG7w2YghT6dl5Mtf0F/iR9qG8q1EKQLwBFP0e32/IVPkq8J6CxC9h2o6RQw6qa7PydQhB68NroV/cPMxlvFYo/exl2WTGhObn//fjbJPAHgN+m0SI/1iypJpzV6lEfITvDm3hvBq3LydrFldq44tKq+w1u3OlBcXGsF8D0412NBIWeBahBHHpTHwQwbVu5kBTQUckSpkKHS3MfG2dBHAsiWZ2E6OdS/sKT8qvrVxHwSh7NSrE9QDEMWAtQoW9FwYPwWLEGPHu/dMRK5Ysfh6I0U6E/tQbfAQCYqpG/8sUEzABAlbQIAHQDlZu5M4elhdAVKC5AuoEbLmRwEwsAYDeAlUrZ+bvjO8q1ykP7sJhGtwCs24ZMHPFBshkDlTBaqlXoY7e88CRdAYcCAMWHq/tIwZ+NKNnGkgUQnlUzJjM62PknLMBkJYq+Gt5OMTir6OFY+h75TI+gQThBpC8pyFyV6hfIyv4yuNjtR2/z2VojuCGFFAEg81B4YUytgQKATpjdLWkvpkEaiREk5gPwyT6SvXXI2qXmAGihEtl4vi4WgFbSM+6N0J/sRcpOS+moPj1ZAveLrFCy+q7/4pE8SKUCFB60rhSSugETvpvwNKIaSKS0KaADjSx6w1uanwH0mC9RKPmH57BKIBPNyfUDQP8AwET/y8crLoD5mFNgUj9zUsynqr6dM5Uz2P5sXDljaY4aBELlF6JcRiRLhZ6gQzlTUVD60QyrjxyUH5PvNN4twAYRigBVwlb3Wl3no/Sw0ynIZqcOsEO7Ktd0XiVfzV69+GbPSju4ZQDoDzSgVzeKELNASqH/pAUIo3z25PzW4GWxNz9dG/TjlQXN2BU+gj/Rloz3W9ZguLBHT6UW1rLzWZomgUssICnNLH9bB96zUjeQ7cC/l58SuLFKXGncILnRXFovVOMmUNs2M2vXkvFqJ3NwMQu0h+vdPlvy+2oAMB9LK4nICu28ymWvgpmJZhaAWUf2IORzPKlmvhQAuqUydq7EQGBBWCBX2v/PAsTxIvJDzIcyH3xsnN2ixE9WALLlAm5G0y+vWRt60QJsxwDdV4NIVClVsjknxmGjJVFAy0BQUYxlASxQrD5qnQLB9lWQ+6/VDSvqhVmUEgPENuG1fouyiF1OmPJ39390/b8CgHbLr1jsAYkyC/HAkWe2fEIYZyj7nbukdRb2TkAArjXxMiB/EgBkol8UG/BBCQtglTS0pZpC6bqL8KcA4KX9qADInV7BbpoLybORY8+SkhUoSmcRvPXBkR9m9fhqFD3V24tf066st2JD1cjKWI/yWUfR8FeypL0d3LW/+GfQTq1vZswyVCjlRu1tD67SH1FSPy0btF/7X9gr1k+ZIxiieYoXGGtWbI/fJMB6ffG7AC+c9GflYahQCYyU0S2gsN5rQVWuOq8EhNMASNuV7WGjUK+vNnMGH4kUAAAg+1nU0Amsl2LCFcUqc0qKt/6+UgplFoC+BVjoiDH6TgCgn8HoY91EwcI9EQMsKb8FY7u1fIZOZX/pBipxBrMwgomupmsDf4kVeDILWFb+AADwd/i6Gbu1FD4KEQoZLMtomQK1IsLb/0goLTrOXtTYc6MsQHmthHisAktxH1vKjwDA6gJP1tS3mXlog5M8P1kJXGKfdZCym9TAs3ToCy5C7xFfkJ3/Sf4PIeK8DKx7anIAAAAASUVORK5CYII=";
		let jstr = `{"fontName" : "DefaultFont","maxSpriteWidth":8,"maxSpriteHeight":12,"characters":{"0":[0,12,6,12],"1":[8,12,5,12],"2":[16,12,6,12],"3":[24,12,6,12],"4":[32,12,6,12],"5":[40,12,6,12],"6":[48,12,6,12],"7":[56,12,6,12],"8":[64,12,6,12],"9":[72,12,6,12]," ":[0,0,6,12],"!":[8,0,3,12],"\\"":[16,0,5,12],"#":[24,0,7,12],"$":[32,0,7,12],"%":[40,0,8,12],"&":[48,0,7,12],"'":[56,0,3,12],"(":[64,0,5,12],")":[72,0,5,12],"*":[79,0,7,12],"+":[88,0,7,12],",":[96,0,4,12],"-":[104,0,7,12],".":[112,0,3,12],"/":[120,0,6,12],":":[80,12,3,12],";":[88,12,4,12],"<":[96,12,6,12],"=":[104,12,6,12],">":[112,12,6,12],"?":[120,12,7,12],"@":[0,24,7,12],"A":[8,24,7,12],"B":[16,24,6,12],"C":[24,24,6,12],"D":[32,24,6,12],"E":[40,24,6,12],"F":[48,24,6,12],"G":[56,24,6,12],"H":[64,24,6,12],"I":[72,24,6,12],"J":[80,24,6,12],"K":[88,24,6,12],"L":[96,24,6,12],"M":[104,24,7,12],"N":[112,24,7,12],"O":[120,24,7,12],"P":[0,36,6,12],"Q":[8,36,7,12],"R":[16,36,6,12],"S":[24,36,6,12],"T":[32,36,7,12],"U":[40,36,6,12],"V":[48,36,7,12],"W":[56,36,7,12],"X":[64,36,7,12],"Y":[72,36,7,12],"Z":[80,36,6,12],"[":[88,36,5,12],"\\\\":[96,36,6,12],"]":[104,36,5,12],"^":[112,36,7,12],"_":[120,36,6,12],"\`":[0,48,5,12],"a":[8,48,7,12],"b":[16,48,6,12],"c":[24,48,6,12],"d":[32,48,6,12],"e":[40,48,6,12],"f":[48,48,5,12],"g":[56,48,6,12],"h":[64,48,6,12],"i":[72,48,3,12],"j":[80,48,4,12],"k":[88,48,6,12],"l":[96,48,3,12],"m":[104,48,7,12],"n":[112,48,6,12],"o":[120,48,6,12],"p":[0,60,6,12],"q":[8,60,7,12],"r":[16,60,6,12],"s":[24,60,6,12],"t":[32,60,6,12],"u":[40,60,6,12],"v":[48,60,7,12],"w":[56,60,7,12],"x":[64,60,7,12],"y":[72,60,7,12],"z":[80,60,6,12],"{":[88,60,5,12],"|":[96,60,3,12],"}":[104,60,5,12],"~":[112,60,7,12]}}`;
		let jobj = JSON.parse(jstr);

		// set the data from the data stream and image data url
		r.LoadJSONData(jobj);
		r._spriteSheet = img;

		return r;
	}

	/** @type {Object} converts the spriteFont to a loadable JSON object */
	ToJSONObject(){
		var r = {};

		r.spriteSheet = this._spriteSheet.src;
		r.maxSpriteWidth = this.maxSpriteWidth;
		r.maxSpriteHeight = this.maxSpriteHeight;

		r.characters = {};
		for(let chc in this.spriteTable){
			
			let chr = String.fromCharCode(chc);
			r.characters[chr] = this.spriteTable[chc].toJSONArray();
		}

		return r;
	}

	/** loads data from a JSON object and replaces all this spriteFont's data */
	LoadJSONData(data){

		this.fontName = data.fontName;
		var spritesheet = this._spriteSheet;

		// if the data specifies a spritesheet, load it
		if(data.spriteSheet)
			this.LoadSpriteSheet(data.spriteSheet);
		else{
			this._spriteSheet = spritesheet;
			this._loaded = true;
		}

		this.maxSpriteWidth = data.maxSpriteWidth;
		this.maxSpriteHeight = data.maxSpriteHeight;

		for(let char in data.characters){
			let chc = char.charCodeAt(0);

			this.spriteTable[chc] = Rect.FromJSONArray(data.characters[char]);
		}
	}

	/** loads a spritesheet texture into the spritefont */
	LoadSpriteSheet(/**@type {string}*/ path) {

		this._spriteSheet = IO.LoadImageFile(path);
		Game.instance.LoadRequests([this._spriteSheet]);
	}

	LoadStatusCheck(){
		if(!this.loaded)
			return;

		if(!!this._onFinishDelegate){

			this._onFinishDelegate();
			this._onFinishDelegate = null;
		}
	}

	/** whether or not the spritesheet for this spritefont has loaded */
	get loaded() {
		return this._loaded;
	}

	get spriteSheet() {
		return this._spriteSheet;
	}

	/** @type {Rect} returns the sprite of the specified character */
	SpriteOf(/**@type {string}*/char){
		return this.spriteTable[char.charCodeAt(0)] ?
			this.spriteTable[char.charCodeAt(0)] : 
			new Rect(new Vec2(), new Vec2(this.maxSpriteWidth, this.maxSpriteHeight));
	}

	/** maps a character to the specified sprite rectangle */
	MapCharacter(/**@type {string}*/char, /**@type {Rect}*/ sprite) {

		var code = char.charCodeAt(0);
		this.spriteTable[code] = sprite;

		this.maxSpriteWidth = Math.max(this.maxSpriteWidth, sprite.width);
		this.maxSpriteHeight = Math.max(this.maxSpriteHeight, sprite.height);
	}

	/** @type {HTMLCanvasElement} renders a string using the sprite characters from the SpriteFont 
	 * sprite sheet onto a canvas and returns it */
	GenerateTextImage(/**@type {string}*/ str, size, spacing) {

		// create the canvas object that will contain the text image
		var tcanvas = document.createElement("canvas");
		tcanvas.width = str.length * this.maxSpriteWidth * size + (str.length - 1 * spacing * size);
		tcanvas.height = this.maxSpriteHeight * size;
		var ctx = tcanvas.getContext("2d");
		
		ctx.fillStyle = "#000";
		ctx.fillRect(0, 0, 100, 100);
		
		// determine the width of the canvas and set it before anything is rendered onto the canvas
		var cwidth = 0;
		for(let i = 0; i < str.length; i++)
			cwidth += this.SpriteOf(str[i]).width * size + spacing * size;
		tcanvas.width = cwidth - spacing;
		ctx.imageSmoothingEnabled = false;
		
		// render each character in the string onto the canvas
		cwidth = 0;
		for(let i = 0; i < str.length; i++){
			/**@type {Rect} */
			let sprite = this.SpriteOf(str[i]);
			/**@type {Rect} */
			let target = new Rect(new Vec2(cwidth, 0), sprite.size.Scaled(size))
			ctx.drawImage(this.spriteSheet, 
				sprite.left, sprite.top, sprite.width, sprite.height, 
				target.left, target.top, target.width, target.height);
			cwidth += sprite.width * size + spacing * size;
		}
		
		return tcanvas;
	}	
	
	GenerateTextImageFixedWidth(/**@type {string}*/ str, size, spacing) {

		// create the canvas object that will contain the text image
		var tcanvas = document.createElement("canvas");
		tcanvas.width = str.length * this.maxSpriteWidth * size + (str.length - 1 * spacing * size);
		tcanvas.height = this.maxSpriteHeight * size;
		var ctx = tcanvas.getContext("2d");
		
		ctx.fillStyle = "#000";
		ctx.fillRect(0, 0, 100, 100);
		
		// determine the width of the canvas and set it before anything is rendered onto the canvas
		var cwidth = str.length * this.maxSpriteWidth * size + spacing * size;
		tcanvas.width = cwidth - spacing;
		ctx.imageSmoothingEnabled = false;
		
		// render each character in the string onto the canvas
		cwidth = 0;
		for(let i = 0; i < str.length; i++){
			/**@type {Rect} */
			let sprite = this.SpriteOf(str[i]);
			/**@type {Rect} */
			let target = new Rect(new Vec2(cwidth, 0), sprite.size.Scaled(size))
			ctx.drawImage(this.spriteSheet, 
				sprite.left, sprite.top, sprite.width, sprite.height, 
				target.left, target.top, target.width, target.height);
			cwidth += this.maxSpriteWidth * size + spacing * size;
		}
		
		return tcanvas;
	}
}