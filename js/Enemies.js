///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Asteroid extends Character {

	constructor(variant){
		super();

		this.team = -1;
		this.isThreat = true;
		this.parentUID = -1;

		this.variant = 0;
		this.tier = 1;
		this.health = 1;
		this.damage = 1;

		this.flip = Math.random() < 0.5;
		this.rotation = 0;
		this.angularVelocity = (Math.random() - 0.5) * 2;

		this.setVariant(variant);
	}

	/** @type {SpriteSheet} */
	static get spriteSheet_asteroids(){
		if(Asteroid.spriteSheetAsset_asteroids == null)
			Asteroid.spriteSheetAsset_asteroids = 
				AssetPack.GetAsset("vanilla", "spriteSheets", "asteroids");
		return Asteroid.spriteSheetAsset_asteroids.globalInstance;
	}

	/**
	 * @type {Asteroid}
	 * @param {Number} sizetier;
	 */
	static FromTier(sizetier){
		
		let variants = [];
		switch(sizetier){
			case 1: variants = [9,10,11]; break;
			case 2: variants = [6,7,8]; break;
			case 3: variants = [4,5]; break;
			case 4: variants = [1,2,3]; break;
			case 5: variants = [0]; break;
		}
		let variant = variants[Math.floor(variants.length * Math.random())];

		return new Asteroid(variant);
	}

	get points(){

		if(this.tier < 3)
			return 10;
		if(this.tier < 4)
			return 30;
		if(this.tier < 5)
			return 50;
		return 80;
	}

	checkCollisions(){

		// iterate through each overlapping collider
		let cols = this.collider.FindOverlappingColliders();
		for(let i = cols.length - 1; i >= 0; i--){
			
			/** @type {Collider} */
			let col = cols[i];

			// don't collide if it is not a solid object
			if(!col.isSolid)
				continue;

			// asteroids that break off from the same parent asteroid don't collide
			if(this.parentUID >= 0){
				if(col.parentObject instanceof Asteroid){
					if(col.parentObject.parentUID == this.parentUID){
						continue;
					}
				}
			}
			
			// find the intersection point
			let intersect = Rect.IntersectRect(this.collider.boundingBox, col.boundingBox);

			// get the difference in position and velocity
			let dpos = col.centroid.Minus(this.position);
			let dvel = col.parentObject.velocity.Minus(this.velocity);

			// destroy the asteroid
			if(!this.isRemoved)
				this.BreakApart(dpos.normalized.Times(-10));
			
			// damage the object that collided with the asteroid
			col.parentObject.Damage(this.damage, dvel.Times(-this.mass), intersect.center);
		}
	}

	Damage(damage, impulse, position){

		this.ApplyImpulse(impulse);
		this.health -= damage;

		if(this.health <= 0){
			this.BreakApart(impulse.normalized.Times(10), this.impulse);
			this.container.AddScore(this.points);
		}
	}

	BreakApart(velocity = Vec2.zero, impulse = Vec2.zero){
		
		let sizes = [];
		switch(this.tier){
			case 1: sizes = []; break;
			case 2: sizes = [1, 1]; break;
			case 3: sizes = [2, 1, 1]; break;
			case 4: sizes = [3, 2, 2, 1, 1]; break;
			case 5: sizes = [4, 3, 3, 1, 1, 1, 1]; break;
		}

		let maxRadius = this.collider.shape.radius;

		let asteroids = [];
		for(let i = sizes.length - 1; i >= 0; i--){
			
			let randVec = Vec2.FromDirection(Math.random() * Math.PI * 2, 1);
			let vel = this.velocity.Plus(randVec.Times(10 + 2 * (6 - sizes[i]))).Plus(velocity);
			
			let asteroid = Asteroid.FromTier(sizes[i]);
			let aradius = asteroid.collider.shape.radius;
			let adist = maxRadius - aradius;

			asteroid.position = this.position.clone.Plus(randVec.Times(adist));
			asteroid.velocity = vel;
			asteroid.angularVelocity *= 2;
			asteroid.parentUID = this.uid;
			
			asteroid.ApplyImpulse(impulse);
			asteroid.Update(0.1);
			asteroids.push(asteroid);
		}

		this.container.AddGameObjects(asteroids);
		this.Remove();
	}

	physicsUpdate(dt){
		super.physicsUpdate(dt);
		this.rotation += this.angularVelocity * dt;

		if(this.container != null)
			this.checkCollisions();
	}

	setVariant(variant){

		this.variant = variant;

		let tier = 1;
		switch (variant){
			case 0: tier = 5; break;
			case 1: tier = 4; break;
			case 2: tier = 4; break;
			case 3: tier = 4; break;
			case 4: tier = 3; break;
			case 5: tier = 3; break;
			case 6: tier = 2; break;
			case 7: tier = 2; break;
			case 8: tier = 2; break;
			case 9: tier = 1; break;
			case 10: tier = 1; break;
			case 11: tier = 1; break;
		}
		switch(tier){
			case 1: 
				this.maxHealth = 1;
				this.damage = 1;
				this.mass = 120;
				this._collider = Collider.FromShape(new Circle(Vec2.zero, 1.5), this);
				break;
			case 2: 
				this.maxHealth = 4;
				this.damage = 3;
				this.mass = 280;
				this._collider = Collider.FromShape(new Circle(Vec2.zero, 3), this);
				break;
			case 3: 
				this.maxHealth = 15;
				this.damage = 10;
				this.mass = 680;
				this._collider = Collider.FromShape(new Circle(Vec2.zero, 6), this);
				break;
			case 4: 
				this.maxHealth = 25;
				this.damage = 25;
				this.mass = 1200;
				this._collider = Collider.FromShape(new Circle(Vec2.zero, 9), this);
				break;
			case 5: 
				this.maxHealth = 35;
				this.damage = 50;
				this.mass = 2800;
				this._collider = Collider.FromShape(new Circle(Vec2.zero, 12.5), this);
				break;
		}
		
		this.checkAllCollisions = false;
		this.health = this.maxHealth;
		this.tier = tier;
	}

	/**
	 * @param {Camera} camera 
	 */
	onDraw(camera){
		camera.DrawSpriteFromSheet(Asteroid.spriteSheet_asteroids, this.variant, this.position, this.rotation, this.flip);
	}
}

class Enemy extends Character {

	constructor(){
		super();

		this.team = 1;
		this.isThreat = true;
		
		/** @type {Character} */
		this.target = null;
		this.targetingRange = 200;
		this.aiTimer = 0.35;

		this.crashDamage = 15;

		this.cruiseSpeed = 10;
		this.rushSpeed = 20;
		this.acceleration = 10;

		this.debrisCount = 10;

		this.rotation = 0;
		this.angularVelocity = 0;
	}

	get targetDelta(){
		return this.target.position.Minus(this.position);
	}

	physicsUpdate(dt){
		super.physicsUpdate(dt);
		this.checkCollisions();
		this.rotation += this.angularVelocity * dt;
	}

	onAdded(container){
		
		let dpos = container.minimapOrigin.Minus(this.position);
		this.rotation = dpos.direction + (Math.random() - 0.5) * Math.PI;

		this.setInitialCruiseVelocity(this.rotation);
	}

	checkCollisions(){

		let ths = this;

		// iterate through each object in each partition that's in a same partition as this object
		this.container.objectPartitions.IterateThroughWorldRectObjects(this.bounds, function(obj){

			// for debugging
			ths.container.totalObjectIterations++;
			
			// don't check for collisions if it's not a solid object
			if(!(obj instanceof SolidObject))
				return;
			
			// don't check for collisions with other enemies on the same team
			if(Character.AreTeamsAllied(obj.team, ths.team))
				return;
			
			// if their hitbox overlaps this our hitbox, we have a collision
			if(obj.collider.shape.OverlapsShape(ths.collider.shape)){

				// invoke the virtual collision method
				ths.onCollision(obj);
			}
		});
	}

	/**
	 * @virtual called when the enemy collides with an object
	 * @param {SolidObject} obj 
	 * @param {RaycastHit} rayHit 
	 */
	onCollision(obj, rayHit){

		// find the intersection point
		let intersect = Rect.IntersectRect(this.collider.shape, obj.collider.shape);

		// get the difference in position and velocity
		let dpos = obj.position.Minus(this.position);
		let dvel = obj.velocity.Minus(this.velocity);

		// destroy the enemy
		this.Kill();

		// damage the object that collided with the enemy
		obj.Damage(this.crashDamage, dvel.Times(-this.mass), intersect.center);
	}

	/**
	 * Kills and destroys the enemy
	 */
	Kill(){

		// play the explosion sound and add the explosion effect
		this.container.QueueSimpleSound(Camera.sfx_EnemyExplode, this.position);
		this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.5)));

		// spawn the destruction debris
		this.spawnDebris(this.debrisCount, this.velocity, 20);

		// remove the enemy from the world
		this.Remove();
	}

	setInitialCruiseVelocity(direction){

		this.velocity = Vec2.FromDirection(
			this.rotation, 
			this.cruiseSpeed + ((Math.random() - 0.5) * this.cruiseSpeed / 2)
			);
	}
	
	/** @type {Number} */
	findOptimalThrustDirection(targetPos, speed){

		let dpos = targetPos.Minus(this.position);
		let targetVel = dpos.normalized.Times(speed);
		let dvel = targetVel.Minus(this.velocity);

		return dvel.direction;
	}

	ai_Update(dt){

		if(this.target == null){
			this.ai_wander(dt);
		}
		else{

			if(this.target.isRemoved){
				this.target = null;
				return;
			}

			this.ai_seek(dt);
		}

		this.aiTimer -= dt;
	}

	ai_lookForTarget(){
		
		// iterate through each object in the world that could possibly be within range
		let ths = this;
		this.container.objectPartitions.IterateThroughWorldRectObjects(
			Rect.FromRadius(this.targetingRange, this.position),
			function(obj){
				
				// if the object is not on a hostile team, ignore it
				if(!(obj instanceof SolidObject) || !Character.AreTeamsHostile(ths.team, obj.team))
					return;
				
				// if the object is within targeting range
				let dpos = obj.position.Minus(ths.position);
				if(dpos.magnitude <= ths.targetingRange){

					// target the object
					ths.target = obj;
				}
			});
	}

	ai_findNearestTarget(){

		// define two variables to track which object is the closest
		let dist = Number.POSITIVE_INFINITY;
		let targ = null;

		// iterate through each object in the world that could possibly be within range
		let ths = this;
		this.container.objectPartitions.IterateThroughWorldRectObjects(
			Rect.FromRadius(this.targetingRange, this.position),
			function(obj){
				
				// if the object is not on a hostile team, ignore it
				if(!(obj instanceof SolidObject) || !Character.AreTeamsHostile(ths.team, obj.team))
					return;
				
				// if the object's distance is closer than all the other objects that've been 
				// checked so far
				let dpos = obj.position.Minus(ths.position);
				if(dpos.magnitude <= dist){

					// flag this object as the closest and store its distance to check 
					// against later
					targ = obj;
					dist = dpos.magnitude;
				}
			});

		// set the enemy's target to the nearest one found
		this.target = targ;
	}

	ai_wander(dt){
		
		if(this.aiTimer <= 0){
			this.ai_lookForTarget();
			this.aiTimer = Math.random() * 0.2 + 0.2;
		}
	}

	ai_seek(dt){

		let tdir = this.findOptimalThrustDirection(this.target.position, this.targetDelta.magnitude);
		let drot = AngleDifference(this.rotation, tdir);

		if(!this.isAiming && Math.abs(drot) < Math.PI / 8){
			this.angularVelocity *= Math.pow(0.9, dt * 60);
			this.ai_moveForward(true, dt);
		}
		else{
			this.ai_rotateToward(tdir, dt);
		}

		if(this.targetDelta.magnitude > this.targetingRange + 100){
			this.target = null;
		}
	}

	ai_seekPos(pos, dt){
		
		let delta = pos.Minus(this.position);
		let tdir = this.findOptimalThrustDirection(pos, delta.magnitude);
		let drot = AngleDifference(this.rotation, tdir);

		if(!this.isAiming && Math.abs(drot) < Math.PI / 8){
			this.angularVelocity *= Math.pow(0.9, dt * 60);
			this.ai_moveForward(true, dt);
		}
		else{
			this.ai_rotateToward(tdir, dt);
		}
	}

	ai_seekLockedTarget(dt){
		
		let targdir = this.targetDelta.direction;
		let angDif = AngleDifference(this.velocity.direction, targdir.normalized);
		let diffact = AngleDifferenceFactor(this.rotation, this.velocity.direction);

		this.ai_rotateToward(targdir, dt);

		if(this.velocity.magnitude * diffact < this.rushSpeed){
			this.ai_moveForward(true, dt);
		}

		if(Math.abs(angDif) > Math.PI / 4){
			this.targetLocked = false;
		}
	}

	ai_moveForward(rush, dt){

		let speed = rush ? this.rushSpeed : this.cruiseSpeed;

		if(this.velocity.magnitude > speed * 0.5){
			let targdir = this.targetDelta.direction;
			if(Math.abs(AngleDifference(this.rotation, targdir)) <= Math.PI / 8){
				if(Math.abs(AngleDifference(this.velocity.direction, targdir)) <= Math.PI / 8){
					this.targetLocked = true;
				}
			}
		}

		this.velocity.Translate(Vec2.FromDirection(this.rotation, this.acceleration * dt));
		this.isThrusting = true;
	}

	ai_moveBackward(dt){
		this.velocity.Translate(Vec2.FromDirection(this.rotation, this.acceleration * -dt));
	}

	ai_rotateToward(rotation, dt){

		let drot = AngleDifference(this.rotation, rotation);
		this.angularVelocity *= Math.pow(0.9, dt * 60);
		this.angularVelocity += Math.sign(drot);
	}

	spawnDebris(count = this.debrisCount, velAddition = new Vec2(), spread = 15, hitpoint = new Vec2()){

		for(let i = count; i > 0; i--){

			let dpos = Vec2.RandomDirection(Math.random() * this.collider.shape.boundingBox.width / 2);
			let dvel = velAddition.Plus(dpos.normalized.Times(spread + Math.random() * spread));

			let deb = new Debris();
			deb.position = this.position.Plus(dpos);
			deb.velocity = this.velocity.Plus(dvel);
			
			this.container.AddGameObject(deb);
		}
	}
}

class Kamikaze extends Enemy{

	constructor(){
		super();

		this._collider = Collider.FromShape(new Circle(Vec2.zero, 7.5), this);
		this.mass = 1400;

		this.debrisCount = 12;

		this.rotation = 0;
		this.angularVelocity = 0;
		
		this.cruiseSpeed = 20;
		this.rushSpeed = 40;
		this.acceleration = 35;
		
		this.maxHealth = 10;
		this.health = 10;
		this.isThrusting = false;
	}

	static get sprite_ship(){

		if(Kamikaze.spriteAsset_ship == null)
			Kamikaze.spriteAsset_ship = AssetPack.GetAsset("vanilla","sprites","enemy_kamikaze");
		return Kamikaze.spriteAsset_ship.globalInstance;
	}

	Damage(damage, impulse, position){

		this.health -= damage;
		if(this.health <= 0){
			
			this.container.AddScore(100);
			this.spawnDebris(this.debrisCount, impulse.Times(0.0035), 20);
			this.Explode(false);
		}

		this.ApplyImpulse(impulse);
		this.ai_findNearestTarget();
	}

	Explode(debris = true){
		
		this.container.QueueSimpleSound(Camera.sfx_EnemyExplode, this.position);

		if(debris)
			this.spawnDebris(this.debrisCount, Vec2.zero, 35);

		this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.5)));
		
		let resource = Resource.ScrapMetal();
		resource.position = this.position;
		this.container.AddGameObject(resource);

		this.Remove();
	}

	onUpdate(dt){
		super.onUpdate(dt);
		this.isThrusting = false;
		this.ai_Update(dt);
	}

	/** @param {Camera} camera */
	onDraw(camera){
		
		camera.DrawSprite(Kamikaze.sprite_ship, this.position, this.rotation);
		
		if(this.isThrusting){
			let tOff1 = new Vec2(-8, -4);
			let tOff2 = new Vec2(-8, 4);
			let frame = Math.floor(this.container.timeSimulated * 1000 % Drone.spriteSheet_thrustFlame.sprites.length);
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(tOff1.Rotated(this.rotation)), this.rotation - Math.PI / 2, false);
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(tOff2.Rotated(this.rotation)), this.rotation - Math.PI / 2, false);
		}
	}
}

class Fighter extends Enemy{

	constructor(){
		super();

		this._collider = Collider.FromShape(new Circle(Vec2.zero, 7.5), this);
		this.mass = 1400;

		this.crashDamage = 12;
		this.rotation = 0;
		this.angularVelocity = 0;
		
		this.cruiseSpeed = 20;
		this.rushSpeed = 50;
		this.acceleration = 50;
		
		this.maxHealth = 20;
		this.health = 20;
		this.targetingRange = 250;
		this.firingRange = 200;
		this.closeRange = 40;
		this.retreatDistance = 200;
		this.targetLocked = false;

		this.burstCount = 2;
		this.burstsFired = 0;
		this.fireDelay = 1;
		this.fireWait = 0;
		this.barrel = new Vec2(15, 7);
		this.scrambleTime = 1;
		this.scrambleDir = 0;
		
		this.inFireMode = false;
		this.inRetreatMode = false;
		this.inScrambleMode = false;
		this.isThrusting = false;
	}

	static get sprite_ship(){

		if(Fighter.spriteAsset_ship == null)
			Fighter.spriteAsset_ship = AssetPack.GetAsset("vanilla","sprites","enemy_fighter");
		return Fighter.spriteAsset_ship.globalInstance;
	}

	ai_startScramble(){

		this.inScrambleMode = true;
		this.scrambleTime = Math.random() * 2 + 1;
		this.inRetreatMode = false;
		this.inFireMode = false;
		this.scrambleDir = this.rotation + (Math.random() - 0.5) * Math.PI;
	}

	ai_shoot(dt){
		if(this.fireWait <= 0){
			this.fireWeapon();
			this.burstsFired += 1;
		}
	}

	ai_fireMode(dt){

		let tdir = this.target.position.Minus(this.position).direction;
		let drot = AngleDifference(this.rotation, tdir);

		this.ai_rotateToward(tdir, dt);

		if(Math.abs(drot) < Math.PI / 6){
			this.ai_shoot();
		}

		this.velocity = this.velocity.Times(Math.pow(0.97, dt * 60));

		if(this.burstsFired > this.burstCount){
			this.burstsFired = 0;
			this.ai_startScramble();
			return;
		}

		let dist = this.targetDelta.magnitude;
		if(dist > this.retreatDistance){
			this.inScrambleMode = true;
		}else if (dist < this.closeRange){
			this.ai_startScramble();
		}
	}

	ai_retreatMode(dt){

		let tdir = this.position.Minus(this.target.position).direction;
		//let drot = AngleDifference(this.rotation, tdir);

		this.ai_rotateToward(tdir, dt);

		if(this.velocity.magnitude < this.rushSpeed){
			this.ai_moveForward(true, dt);
		}
		else{
			this.velocity = this.velocity.Times(Math.pow(0.95, dt * 60));
		}

		if(this.target == null || this.targetDelta.magnitude >= this.retreatDistance){
			this.inRetreatMode = false;
		}
	}
	
	ai_retreatMode(dt){

		let tdir = this.position.Minus(this.target.position).direction;
		//let drot = AngleDifference(this.rotation, tdir);

		this.ai_rotateToward(tdir, dt);

		if(this.velocity.magnitude < this.rushSpeed){
			this.ai_moveForward(true, dt);
		}
		else{
			this.velocity = this.velocity.Times(Math.pow(0.95, dt * 60));
		}

		if(this.target == null || this.targetDelta.magnitude >= this.retreatDistance){
			this.inRetreatMode = false;
		}
	}

	ai_scrambleMode(dt){

		let tpos = this.target.position.Plus(this.targetDelta.normalized.Times(-this.firingRange - 50));
		let targDist = this.targetDelta.magnitude;
		
		if(this.scrambleTime > 0){
			this.scrambleTime -= dt;
			this.tpos = this.targetDelta;
			tpos = this.position.Plus(Vec2.FromDirection(this.scrambleDir, 100));
		}

		else if(targDist > this.closeRange + 50 && targDist < this.firingRange){
			this.inRetreatMode = false;
			this.inScrambleMode = false;
		}

		this.ai_seekPos(tpos, dt);
	}

	ai_seek(dt){

		if(!this.inRetreatMode && this.targetDelta.magnitude < this.firingRange){
			this.inFireMode = true;
		}

		if(this.inRetreatMode){
			this.ai_retreatMode(dt);
			return;
		}

		if(this.inScrambleMode){
			this.ai_scrambleMode(dt);
			return;
		}

		if(this.inFireMode){
			this.ai_fireMode(dt);
			return;
		}

		super.ai_seek(dt);
	}

	ai_wander(dt){
		super.ai_wander(dt);
		this.inFireMode = false;
		this.inRetreatMode = false;
	}

	Damage(damage, impulse, position){

		this.health -= damage;
		if(this.health <= 0){
			
			this.container.AddScore(250);

			this.container.QueueSimpleSound(Camera.sfx_EnemyExplode, this.position);
			this.spawnDebris(this.debrisCount, impulse.Times(0.002));
			this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.5)));
			this.Remove();
		}

		this.ApplyImpulse(impulse);
		this.ai_findNearestTarget();
		this.inFireMode = false;
		this.inRetreatMode = true;
	}

	handleWeapon(dt){

		if(this.fireWait > 0)
			this.fireWait -= dt;
		else if(this.fireWait < 0)
			this.fireWait = 0;
	}

	fireWeapon(){

		let projSpeed = 100;
		let tpos = this.position.Plus(this.barrel.Rotated(this.rotation));
		
		let proj = new EnemyBullet();
		proj.FireFrom(tpos, this.rotation, projSpeed);
		proj.team = this.team;
		proj.damage = 5;
		this.container.AddGameObject(proj);

		this.container.QueueSimpleSound(Camera.sfx_EnemyWeaponFire, this.position);
		proj.hitSFX

		this.fireWait = this.fireDelay;
		this.barrel.y *= -1;
	}

	onUpdate(dt){
		super.onUpdate(dt);
		this.isThrusting = false;
		this.ai_Update(dt);
		this.handleWeapon(dt);
	}

	/** @param {Camera} camera */
	onDraw(camera){
		
		camera.DrawSprite(Fighter.sprite_ship, this.position, this.rotation);
		
		if(this.isThrusting){
			let tOff1 = new Vec2(-9, 0);
			let frame = Math.floor(this.container.timeSimulated * 1000 % Drone.spriteSheet_thrustFlame.sprites.length);
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(tOff1.Rotated(this.rotation)), this.rotation - Math.PI / 2, false);
		}
	}
}