///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * @abstract all inventory type classes will inherit from this base collectible container
 * class. Meant to act as a foundation for an object that all characters who can carry 
 * collectible objects will contain.
 */
class CollectibleContainer{
	constructor(){

		// an empty object to store resource types by their resource key
		/** @type {Object} */
		this.resources = {};
	}

	/** @type {Array<Resource>} */
	get allResources(){
		let r = [];

		for(let i in this.resources){
			r.push(this.resources[i]);
		}

		return r;
	}

	/** @abstract @type {Array<Item>} */
	get allItems(){ return []; }

	/** @type {Array<Collectible>} */
	get allCollectibles(){ 

		let r = this.allResources;
		let r2 = this.allItems;
		for(let i = r2.length - 1; i>= 0; i--){
			r.push(r2[i]);
		}
		
		return r;
	}

	/**
	 * @type {Number} returns the number of the specified type of collectible within the container
	 * @param {Collectible} collectible 
	 */
	NumberOfCollectible(collectible){

		// if the collictible is a resource, us the specific Resource counting functionality
		if(collectible instanceof Resource)
			return this.NumberOfResource(collectible);

		// otherwise if the collictible is a item, us the specific Item counting functionality
		return this.NumberOfItem(collectible);
	}

	/**
	 * @type {Number} returns the number that represents the amount of the specified resource 
	 * inside of this container
	 * @param {Resource|String} resource can either be a resource('Resource' type) OR 
	 * resource key('String' type) to check the quantity of that exists in this container
	 */
	NumberOfResource(resource){
		let key = resource instanceof Resource ? resource.key : resource;

		// if the resource key does not exist, it contains 0 of that resource
		if(this.resources[key] == null)
			return 0;

		// otherwise return the amount of the respective resources contained within this container
		return this.resources[key].amount;
	}

	/**
	 * @type {Number} returns the number of the specified item that exist inside this container
	 * @param {Item} item the item to search for within the container
	 */
	NumberOfItem(item){

		// if it is a unique item type, return only 1 if the container has it, otherwise return 0
		if(item.oid < 0){
			return this.HasItem(item) ? 1 : 0;
		}

		// check each item within the container and return the amount that exist inside the 
		// container
		let items = this.allItems;
		for(let i = items.length - 1; i >= 0; i--){

			// if the items have the same oid, return the size of the item stack
			if(items[i].oid == item.oid)
				return items[i].amount;
		}
	}

	/**
	 * @type {Boolean} checks to see if the specified resource and amount of it exist inside this 
	 * container
	 * @param {Resource} resource 
	 */
	HasResource(resource){

		// the container always contains at least 0 of any resource
		if(resource.amount <= 0)
			return true;

		// return false if the resource key does not exist in the container
		if(this.resources[resource.key] == null)
			return false;

		// return true if there is a greater than or equal to amount of the specified resource
		return this.resources[resource.key].amount >= resource.amount;
	}

	/** 
	 * @virtual @type {Boolean} checks to see if an item and the specified amount of it exists 
	 * inside the container 
	 * @param {Item} item
	 */
	HasItem(item){ 
		return this.NumberOfItem(item) >= item.amount;
	}

	/** 
	 * @virtual @type {Item} gets the item at the specified index or null if none 
	*/
	GetItemAt(index){
		return this.allItems[index];
	}

	/**
	 * @type {Boolean} checks to see if the specified collectible and amount exist within the 
	 * container
	 * @param {Collectible} collectible 
	 */
	HasCollectible(collectible){
		return (
			collectible instanceof Resource ?
				this.HasResource(collectible) : 
				this.HasItem(collectible)
		);
	}

	/**
	 * adds a collectible item to the container
	 * @param {Collectible} collectible 
	 */
	Add(collectible){

		// if the collectible is a resource, add it to the resource container
		if(collectible instanceof Resource){
			return this.AddResource(collectible);
		}

		// if it is not a resource, it is an item
		else{
			return this.AddItem(collectible);
		}
	}

	/**
	 * @virtual @type {Resource} returns the resource and amount of them that were added to the 
	 * inventory, if the returned resource amount is less than the amount that was added, that means
	 * some of the resources could not be added to the inventory
	 * @param {Resource} resource 
	 */
	AddResource(resource){

			// if the resources container does not already contain the resource type
			if(this.resources[resource.key] == null){
				this.resources[resource.key] = resource.clone;
			}

			// if the resource container already contains the resource type
			else{
				this.resources[resource.key].amount += resource.amount;
			}

			// return an empty collictible to denote all that were added
			return Collectible.empty;
	}

	/** 
	 * @abstract @type {Item} returns the items and amount of them that were added to the 
	 * inventory, if the returned item amount is less than the amount that was added, that means
	 * some of the items could not be added to the inventory
	 */
	AddItem(item){ return item; }
}

/** 
 * a simple inventory that cannot be organized or managed, just is an array of stored collectibles
 */
class ArrayInventory extends CollectibleContainer {
	constructor(){
		super();

		/** @type {Array<Item>} */
		this.itemArray = [];
	}

	/** @override */
	get allItems(){
		return this.itemArray;
	}

	/** @override */
	HasItem(item){

		// if it's a unique item, check to see if it contains the exact instance specified
		if(item.oid < 0)
			return this.itemArray.includes(item);

		// if it's not a unique item, check the oid of each item in the item array
		for(let i = this.itemArray.length - 1; i >= 0; i--){

			// if they have the same oid, check to see if it has at least the specified amount and 
			// return true or not based on the result of that check
			if(this.itemArray[i].oid == item.oid)
				if(this.itemArray[i].amount >= item.amount)
					return true;
		}

		// if all checks fail, the container does not have the specified item
		return false;
	}

	/**
	 * @override
	 * @param {Item} item 
	 */
	AddItem(item){

		// if it is a unique item, just add it to the array and do nothing else
		if(item.oid < 0){
			this.itemArray.push(item);
			return;
		}

		// iterate through all the items to check if there is already an item of the specified type
		for(let i = this.itemArray.length - 1; i >= 0; i--){

			// just add to the item stack if it already has a type of the same item
			if(this.itemArray[i].oid == item.oid)
				this.itemArray[i].amount += item.amount;
		}

		// return an empty collectible to denote that all items were placed in inventory
		return Collectible.empty();
	}
}

/**
 * an inventory that has specific slots that hold items so the user can organize the items in
 * their inventory
 */
class SlottedInventory extends CollectibleContainer {
	constructor(){ 
		super();

		/** @type {Array<Item>} */
		this.slots = [];

		/** @type {Number} */
		this.maxSlots = 24;
	}

	/** @override @type {Array<Item>} */
	get allItems(){

		// define an array that will hold all the items to be returned
		let r = [];

		// iterate through each inventory slot
		for(let i in this.slots){

			// get a reference to the slot
			let slot = this.slots[i];
			
			// add the item in the slot to the return array if there is at least one of them
			if(slot.amount > 0)
				r.push(slots);
		}

		return r;
	}

	/** 
	 * @type {Number} returns the max stack size of the specified item 
	 * @param {Collectible} item
	 */
	FindMaxStackSize(item){
		return Number.POSITIVE_INFINITY;
	}

	/** @override */
	GetItemAt(index){
		return this.slots[index];
	}

	/** @override */
	AddItem(item){

		let numLeft = item.amount;
		let maxStack = this.FindMaxStackSize(item);

		// as long as there are any items left to be placed in the inventory, loop through and try 
		// to find a valid slot to place them into
		while(numLeft > 0){
			let index = this.FindValidItemSlotIndex(item);
			
			// no valid slots left, break the loop
			if(index < 0){
				break;
			}
			
			// get the max amount of items that can be placed in the slot
			let maxPlacement = maxStack;
			let slot = this.slots[index];

			// if there is nothing in the slot, put it there
			if(slot == null){
				
				// create an empty item stack of the same item type in the slot
				slot = item.clone;
				slot.amount = 0;
				this.slots[index] = slot;
			}

			// if there is an item stack in the same slot, deduct the max number of items that can
			// be placed in the slot by the amoun that is already there
			else{
				maxPlacement -= slot.amount;
			}

			// add the items to the inventory
			let amtAdded = Math.min(maxPlacement, numLeft);
			slot.amount += amtAdded;
			numLeft -= amtAdded;
		}

		// assign the return variable to the item stack initially added
		let r = item;

		// if any of the stack was put into the inventory, create a new item instance of the same
		// item type and set 
		if(numLeft < r.amount){
			r = item.clone;
			r.amount = numLeft;
		}

		// return the item type and amount that were placed in the inventory
		return r;
	}

	/**
	 * @type {Number} finds the index of the first available slot for the specified item type
	 * @param {Item} item the item to find the slot index for
	 * @param {Boolean} overwrite whether or not empty slots that are reserved for specific items
	 * 	should be counted as valid slots or not
	 */
	FindValidItemSlotIndex(item, overwrite = true){
		
		// iterate through each slot that already has an item
		for(let i in this.slots){

			// ensure slot is not null
			if(this.slots[i] == null)
				continue;
			
			if(this.slots[i].oid == item.oid){

				// get the item in the slot and find its max stack size
				let slot = this.slots[i];
				let maxStack = this.FindMaxStackSize(slot);

				// if the slot stack is full, skip this slot
				if(slot.amount >= maxStack)
					continue;
				
				// otherwise return the current index of the slot
				return parseInt(i);
			}
		}

		// store a backup var to overwrite empty slots that are reseverved for specific items
		let backup = -1;

		// iterate through each slot again, including empty slots
		for(let i = 0; i < this.maxSlots; i++){

			// if there's nothing in the slot, return that slot
			if (this.slots[i] == null)
				return i;

			// if the slot is empty but reserved, store the index in the backup var
			else if (this.slots[i].amount == 0 && backup < 0)
				backup = i;
		}

		// if overwrite is enabled, return the backup slot, otherwise return -1 to indicate no 
		// available slots
		return overwrite ? backup : -1;
	}
}