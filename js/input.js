///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Input{

	static Initialize(){

		/**@type {Array<Boolean>} */
		Input._keysDown = [];

		Input.devTools = false;

		/**@type {Array<Boolean>} */
		Input._mouseButtonsDown = [];

		/**@type {Vec2} */
		Input._mouseDelta = new Vec2();

		// listen for mouse movement
		window.addEventListener("mousemove", function(e){
			Input.onPointerMoved(new Vec2(e.movementX, e.movementY));
		});

		// listen for keypress events
		window.addEventListener("keydown", function(e){
			
			if([32,40,38,34,33,13,9].indexOf(e.keyCode) >= 0)
				e.preventDefault();

			if(e.keyCode == 120){
				Input.devTools = !Input.devTools;
				console.log("devTools " + Input.devTools);
			}

			if(Input.devTools && e.keyCode == 16){
				Game.instance.masterTimescale = 10;
			}

			Input._keysDown[e.keyCode] = true;
			//console.log(e.key + ": " + e.keyCode);
		});
		window.addEventListener("keyup", function(e){
			Input._keysDown[e.keyCode] = false;
			
			if(Input.devTools && e.keyCode == 16){
				Game.instance.masterTimescale = 1;
			}
		});

		// listen for mouse button events
		window.addEventListener("mousedown", function(e){
			e.preventDefault();
			Input._mouseButtonsDown[e.button] = true;
		});
		window.addEventListener("mouseup", function(e){
			e.preventDefault();
			Input._mouseButtonsDown[e.button] = false;
		});

		// listen for any active gamepads
		window.addEventListener("gamepadconnected", function(e){
			console.log("Gamepad detected in slot " + e.gamepad.index);
		});
		window.addEventListener("gamepaddisconnected", function(e){
			console.log("Disconnected gamepad in slot " + e.gamepad.index);
		});
		
		window.onload = function () { window.focus(); };
		window.onclick = function () { window.focus(); };
	}

	static Step(){
		Input._mouseDelta.x = 0;
		Input._mouseDelta.y = 0;
	}

	/** @type {Array<Boolea>}} */
	static get keysDown(){
		return Input._keysDown;
	}

	/**@type {Array<Boolean>} */
	static get mouseButtonsDown(){
		return Input._mouseButtonsDown;
	}

	/** @type {Boolean} checks to see if the game is currently locking the pointer */
	static get isPointerLocked(){
		
		// return false if no element is currently locked
		if(document.pointerLockElement == null){
			return false;
		}

		// return true if the element that is locking the pointer is the game canvas
		return document.pointerLockElement == Game.instance.canvas;
	}

	/** @type {Vec2} */
	static get mouseDelta(){
		return Input._mouseDelta.clone;
	}

	/**
	 * called when the pointer is moved
	 * @param {Vec2} delta 
	 */
	static onPointerMoved(delta){ 
		this._mouseDelta.Translate(delta);
	}

	/**
	 * Locks the pointer to the canvas if possible, must be called from a user input event
	 */
	static LockPointer(){
		Game.instance.canvas.requestPointerLock();
	}
}

Input.Initialize();
Input.Step();