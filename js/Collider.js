///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * a wrapper for the collisionshape so that we can more easily relate the collision shape and it's
 * parent game object
 */
class Collider{

	constructor(){

		/** @type {GameObject} */
		this.parentObject = null;
		
		/** @type {CollisionShape} */
		this.shape = null;
		
		/** @type {Rect} */
		this.partitionRange = new Rect();

		/** @type {Vec2} */
		this.offset = Vec2.zero;

		/** @type {Collider} */
		this.isSolid = true;
	}

	/**
	 * Creates a collider from the specified collision shape and game object parent
	 * @param {CollisionShape} shape 
	 * @param {GameObject} parent 
	 */
	static FromShape(shape, parent){

		// create the collider and set it's properties
		let r = new Collider();
		r.shape = shape;
		r.parentObject = parent;

		return r;
	}

	/** @type {GameObjectContainer} */
	get container(){
		return this.parentObject.container;
	}

	/** @type {Rect} */
	get boundingBox(){
		return this.shape.boundingBox;
	}

	/** @type {Vec2} */
	get centroid(){
		return this.shape.centroid;
	}

	get uid(){
		return this.parentObject.uid;
	}

	/**
	 * attach the collider to the specified game object
	 * @param {GameObject} object 
	 */
	AttachToObject(object){

		if(object.container != null)
			throw (
				"colliders must be attached to a game object before the game object is added to " +
				"it's container"
			);

		object.implements_collider = true;
		object.collider = this;
	}

	/**
	 * centers the collision shape on to the parent object, accounting for the offset set in 
	 * this collider
	 */
	CenterOnParent(){
		this.shape.centroid = this.parentObject.position.Plus(this.offset);
	}

	/**
	 * @type {Array<Collider>} returns an array of colliders that are overlapping with this one in
	 * in the same game object container
	 */
	FindOverlappingColliders(){

		// we need a reference to self for anonymous function callback
		let ths = this;
		let r = [];

		/**
		 * function that is called when we're iterating through the colliders in the game object
		 * container's collider partitions
		 * @param {Collider} col the collider that is currently being iterated through
		 */
		let func = function(col){
			if(col.uid == ths.uid)
				return;
			if(ths.shape.OverlapsShape(col.shape))
				r.push(col);
		}

		// iterate through each possible collider and call the previously defined function
		this.container.collisionPartitions.IterateThroughObjects(this.partitionRange, func, false);

		// return the array of overlapping colliders
		return r;
	}
}

/**
 * a wrapper for raycastresult so that we can have a reference to the collider that was hit when
 * calling a raycast from a game object container
 */
class ColliderRaycastResult{

	constructor(){

		/** @type {RaycastResult} */
		this.result = null;

		/** @type {Collider} */
		this.collider = null;
	}

	/**
	 * @returns @type {ColliderRaycastResult} creates a collider raycast result from a collider 
	 * and a raycast result object
	 * @param {Collider} collider the collider that was hit
	 * @param {RaycastResult} result the result from the raycast
	 */
	static FromResult(collider, result){

		let r = new ColliderRaycastResult();
		r.result = result;
		r.collider = collider;

		return r;
	}
}