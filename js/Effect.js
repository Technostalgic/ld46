///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Effect extends GameObject{

	constructor(){
		super();

		this.velocity = new Vec2();

		/** @type {SpriteSheet} */
		this.spriteSheet = null;
		/** @type {SpriteAnimation} */
		this.spriteAnim = null;

		this.renderOrder = 100;
		this.rotation = 0;
		this.isFlipped = false;
	}

	static Load(){

		Effect.spriteSheet_smallHit = null;
		Effect.spriteSheet_smallExplosion = null;
		Effect.spriteSheet_flash = null;

		Effect.graphic_smallHit = IO.LoadImageFile("./gfx/SmallHit.png", function(){
			Effect.spriteSheet_smallHit = new SpriteSheet(Effect.graphic_smallHit);
			let sprsize = new Vec2(Effect.graphic_smallHit.width / 7, Effect.graphic_smallHit.height);
			Effect.spriteSheet_smallHit.GenerateSprites(
				sprsize, new Vec2(sprsize.x - 1, sprsize.y / 2));
		});
		Effect.graphic_smallExplosion = IO.LoadImageFile("./gfx/SmallExplosion.png", function(){
			Effect.spriteSheet_smallExplosion = new SpriteSheet(Effect.graphic_smallExplosion);
			Effect.spriteSheet_smallExplosion.GenerateSprites(
				new Vec2(Effect.graphic_smallExplosion.width / 7, Effect.graphic_smallExplosion.height));
		});
		Effect.graphic_flash = IO.LoadImageFile("./gfx/Flash.png", function(){
			Effect.spriteSheet_flash = new SpriteSheet(Effect.graphic_flash);
			Effect.spriteSheet_flash.GenerateSprites(
				new Vec2(Effect.graphic_flash.width / 10, Effect.graphic_flash.height));
		});

		Game.LoadRequests([Effect.graphic_smallHit, Effect.graphic_smallExplosion, Effect.graphic_flash]);
	}

	/** @type {AnimatedEffect} */
	static SmallHit(position, velocity, rotation){

		let r = new AnimatedEffect();
		r.spriteSheet = Effect.spriteSheet_smallHit;
		r.spriteAnim = new SpriteAnimation([0, 1, 2, 3, 4, 5, 6], 30);
		
		r.position = position.clone;
		r.velocity = velocity.clone;
		r.rotation = rotation;
		r.isFlipped = false;

		if(Math.random() < 0.5){
			r.rotation += Math.PI;
			r.isFlipped = true;
		}

		return r;
	}
	
	/** @type {AnimatedEffect} */
	static SmallExplosion(position, velocity){

		let r = new AnimatedEffect();
		r.spriteSheet = Effect.spriteSheet_smallExplosion;
		r.spriteAnim = new SpriteAnimation([0, 1, 2, 3, 4, 5, 6], 30);
		
		r.position = position.clone;
		r.velocity = velocity.clone;
		r.rotation = Math.random() * Math.PI * 2;
		r.isFlipped = Math.random() < 0.5;

		return r;
	}
	
	/** @type {AnimatedEffect} */
	static SmallFlash(position, velocity, randRotation = true){

		let r = new AnimatedEffect();
		r.spriteSheet = Effect.spriteSheet_flash;
		r.spriteAnim = new SpriteAnimation([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 60);
		
		r.position = position.clone;
		r.velocity = velocity.clone;
		r.rotation = randRotation ? Math.random() * Math.PI * 2 : 0;
		r.isFlipped = false;

		if(Math.random() < 0.5){
			r.rotation += Math.PI;
			r.isFlipped = true;
		}

		return r;
	}
	
	onUpdate(dt){
		this.position.Translate(this.velocity.Times(dt));
	}
}

class CircleEffect extends Effect{

	constructor(){
		super();

		this.geometry = new Circle();
		this.color = new Color(255, 255, 255, 1);
	}

	/** @type {Number} */
	get radius(){
		return this.geometry.radius;
	}
	set radius(value){
		this.geometry.radius = value;
	}

	/** @param {Camera} camera */
	onDraw(camera){
		this.geometry.center = this.position;
		this.geometry.radius = this.radius;
		this.geometry.DrawFill(camera, this.color);
	}
}

class AnimatedEffect extends Effect{

	constructor(){
		super();
		
		this.velocity = new Vec2();

		/** @type {SpriteSheet} */
		this.spriteSheet = null;
		/** @type {SpriteAnimation} */
		this.spriteAnim = null;
	}

	onUpdate(dt){

		super.onUpdate(dt);
		this.spriteAnim.Play(dt);

		if(this.spriteAnim.isComplete){
			this.Remove();
		}
	}

	/**@param {Camera} camera */
	onDraw(camera){
		camera.DrawSpriteFromSheet(this.spriteSheet, this.spriteAnim.frameIndex, this.position, this.rotation, this.isFlipped);
	}
}