///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///


/**
 * a data structure that represents an individual element inside a UI window, such as text, a 
 * button, image, or something else like that
 */
class UIElement extends UIElementContainer{

	constructor(){
		super();
		
		/** @type {Boolean} */
		this.isSelectable = false;

		/** @type {Boolean} */
		this._isSelected = false;
	}

	/**
	 * @type {Function} returns an action function that closes the specified ui window
	 * @param {UIWindow} uiWindow 
	 */
	static Action_CloseWindow(uiWindow){

		let func = function(){
			uiWindow.uiContainer.CloseWindow(uiWindow);
		};

		return func;
	}

	/** @type {UIContainer} */
	get uiContainer(){ 
		let r = this.parentContainer;

		// climb the container heirarcy until the uiwindow is reached
		while(!(r instanceof UIWindow)){
			r = r._parent;
			if(r == null)
				return null;
		}

		return r.uiContainer;
	}

	/** @type {UIWindow} */
	get parentWindow(){

		if(this._parent == null)
			return null;

		let r = this._parent;

		while(!(r instanceof UIWindow)){
			r = r._parent;

			if(r == null)
				return null;
		}

		return r;
	}

	/** @type {UIWindow} */
	get parentContainer(){ return this._parent; }

	/** @type {Boolean} */
	get isSelected(){ return this._isSelected; }

	/** @type {Rect} gets the bounds relative to the parentwindow */
	get relativeBounds(){

		let b = this.bounds;

		let window = this.parentWindow;
		b.position = b.position.Minus(window.bounds.topLeft);
		
		return b;
	}

	/** @override */
	SendAction(action, target = null){

		if(this.isSelectable){
			switch(action){
				case UIActionType.Select:
					this._isSelected = true;
					break;
				case UIActionType.Deselect:
					this._isSelected = false;
					break;
			}
		}

		super.SendAction(action, target);
	}

	/**
	 * @virtual draws the element
	 * @param {Camera} cam 
	 */
	Draw(cam){ }
}

/**
 * an object used to automatically organize a group of elements within a window or another 
 * element container
 */
class UIElementLayoutGroup extends UIElement{
	constructor(){
		super();

		/** 
		 * @type {Vec2} the layout group alignment anchor, x value is used for horizontal alignment
		 * while y value is used for vertical alignment, values of zero are aligned to the 
		 * beginning, values of 0.5 are centered, and values of 1 are aligned to the end of the 
		 * group boundaries, only applied to the outermost alignment rect
		 */
		this.layoutAnchorOuter = new Vec2(0.5);
		
		/** 
		 * @type {Vec2} the layout group alignment anchor that's applied to all the alignment 
		 * rects inside the outermost alignment rect
		 */
		this.layoutAnchorInner = Vec2.zero;

		/** @type {Vec2} the spacing between each element in the layout group */
		this.layoutSpacing = Vec2.zero;

		/** @type {Boolean} */
		this._isDirty = true;
	}

	/** 
	 * forces the layout group to re-organize the child elements when they are next drawn 
	 */
	MarkDirty(){
		this._isDirty = true;
	}

	/**
	 * @type {Array<Vec2>} does the same thing as this.GetAlignmentRects(), but only returns the sizes
	 * instead of the full rects
	 */
	GetAlignmentSizes(){

		// define the array of rects to be returned
		let r = [];

		// get the array of direct child elements and the length of the array
		let elems = this.GetDirectChildElements();
		let elemCount = elems.length;

		// define an array of elem bounds so they only need to be calculated when iterating 
		// through the elements the first time
		/** @type {Array<Rect>} */
		let layoutBounds = this.bounds;

		// define a variable to store the cumulative width and height *insert funny cum joke here*
		let cumSize = Vec2.zero;
		let lineSize = Vec2.zero;
		let pushLastLine = false;
		
		// iterate through each element
		for(let i = 0; i < elemCount; i++){

			// calculate and store the element bounds
			let elemBounds = elems[i].bounds;

			// on the first iteration, set the initial height and width to the first element bounds
			if(i == 0){
				lineSize.x = elemBounds.width;
				lineSize.y = elemBounds.height
				continue;
			}

			// if the element fits inside the current layout group line
			let inc = elemBounds.width + this.layoutSpacing.x;
			if (lineSize.x + inc <= layoutBounds.width){

				// if iteration stops here, we still need to append the last line to cumulative 
				// size and the last line rect to the return array
				pushLastLine = true;

				// increment the line width and update the height
				lineSize.x += inc;
				lineSize.y = Math.max(elemBounds.height, lineSize.y);
			}

			// if the element does not fit on the current line
			else {

				// don't push the last line if iteration stops here
				pushLastLine = false;

				// increment the cumulative size by the necessary amount at the end of each line
				cumSize.x = Math.max(lineSize.x, cumSize.x);
				cumSize.y += lineSize.y + this.layoutSpacing.y;

				// push the lineRect to the return array
				r.push(lineSize.clone);

				// reset the line size
				lineSize = elemBounds.size.clone;
			}
		}

		// if we need to push the last line, do it
		if(pushLastLine){

			// add the last line to the cumulative size
			cumSize.x = Math.max(lineSize.x, cumSize.x);
			cumSize.y += lineSize.y + this.layoutSpacing.y;

			// push the final line rect to the return array
			r.push(lineSize);
		}

		// remove the spacing below the last line
		if(elemCount > 0){
			cumSize.y -= this.layoutSpacing.y;
		}

		// insert the cumulative rect at the beginning of the array
		r.splice(0, 0, cumSize);

		// return the array
		return r;
	}

	/**
	 * @type {Array<Rect>} returns an array of rects with the first rect being the total area of 
	 * space that all the elements cumulativeley take up, and each rect after that being an 
	 * individual "line" of elements within the larger rect (think of this like the way that 
	 * text wrapping in text editors works, where each individual word in a text editor is an 
	 * element in the UIElementContainer)
	 * @param {Array<Vec2>} sizes the sizes of the alignment rects, can be calculated with 
	 * 	this.GetAlignmentSizes()
	*/
	GetAlignmentRects(sizes){

		// create an array to fill with layout rects to return
		let r = [];

		// calculate the cumulative element rect and push it into the return array
		let selfBounds = this.bounds;
		let sizeDif = selfBounds.size.Minus(sizes[0]);
		let outerOff = 
			selfBounds.topLeft.Plus(
				sizeDif.Scaled(this.layoutAnchorOuter)
			);
		let outerRect = new Rect(outerOff, sizes[0]);
		r.push(outerRect);

		// set a variable to increment that determines the top of where the rect calculated from
		// the current line will be
		let lineTop = outerRect.top;

		// iterate through each size
		for(let i = 1; i < sizes.length; i++){

			// get a reference to the size of current line
			let lineSize = sizes[i];

			// calculate the x offset from the left edge of the outer rect
			let xOff = (outerRect.width - lineSize.x) * this.layoutAnchorInner.x;

			// create the rect from the line size and offset
			let off = new Vec2(outerRect.left + xOff, lineTop);
			let rect = new Rect(off, lineSize.clone);

			// push the line rect to the return array
			r.push(rect);

			// increment the line top rect placement
			lineTop += lineSize.y + this.layoutSpacing.y;
		}

		// return the result
		return r;
	}

	/** @virtual */
	OrganizeChildElements(){

		// calculate alignment rects
		let alignmentRects = this.GetAlignmentRects(this.GetAlignmentSizes());

		if(alignmentRects.length <= 1)
			return;

		// create alignment rect iterator
		let rectInd = 1;

		// get info from current alignment rect
		let top = alignmentRects[rectInd].top;
		let left = alignmentRects[rectInd].left;
		let right = alignmentRects[rectInd].right;

		// get bounds of layout group
		let layoutBounds = this.bounds;

		// iterate through each direct child
		let elems = this.GetDirectChildElements();
		let elemCount = elems.length;
		for(let i = 0; i < elemCount; i++){
			
			/**@type {UIElement} */
			let elem = elems[i];
			elem.positionOffset = Vec2.zero;

			// get the old elem bounds
			let elemBounds = elem.bounds;

			// calculate the offset from the elements current position to it's target position
			let yOff = (alignmentRects[rectInd].height - elemBounds.height) * this.layoutAnchorInner.y;
			let off = (new Vec2(left, top + yOff)).Minus(elemBounds.topLeft);
			
			// apply offset through modifying the element's position anchor
			elem.parentAnchor.x += off.x / layoutBounds.width;
			elem.parentAnchor.y += off.y / layoutBounds.height;

			// if it's not the last element in the layout group
			if(i != elemCount.length - 1){

				// set left to the new elem bounds left plus spacing
				left = elem.bounds.right + this.layoutSpacing.x;

				// if end of line reached
				if(left > right){

					// increment rect index and update placement values
					rectInd++;
					if(rectInd < alignmentRects.length){
						top = alignmentRects[rectInd].top;
						left = alignmentRects[rectInd].left;
						right = alignmentRects[rectInd].right;
					}
				}
			}
		}
	}

	/** @override */
	Draw(cam){ 

		// // debug code that draws outer alignment rect of layout group
		// let rect = this.relativeBounds;
		// rect.position = rect.position.Minus(cam.offset);
		// cam.StrokeRect(rect, "#999", 1);

		// organize children if necessary
		if(this._isDirty){
			this.OrganizeChildElements(); 
			this._isDirty = false;
		}
	}
}

/**
 * a UI element that displays a graphic
 */
class UIGraphic extends UIElement{

	constructor(){
		super();

		/** @type {SpriteSheet} */
		this._sprite = null;
	}

	/** @type {UIGraphic} @param {CanvasImageSource} graphic */
	static FromGraphic(graphic){
		let sprite = SpriteSheet.FromImage(graphic);
		return UIGraphic.FromSprite(sprite);
	}

	/** @type {UIGraphic} @param {SpriteSheet} sprite */
	static FromSprite(sprite){
		let r = new UIGraphic();
		r._sprite = sprite;
		r.sizeAnchor = Vec2.zero;
		r.sizeOffset = new Vec2(
			sprite.currentFrame.bounds.width, 
			sprite.currentFrame.bounds.height);
		return r;
	}

	/** @override @param {Camera} cam */
	Draw(cam){

		// offset bounds by camera offset
		let rect = this.relativeBounds;
		rect.position = rect.position.Minus(cam.offset);

		//draw srpite at rect
		cam.DrawSprite(
			this._sprite, 
			rect.topLeft,
			Vec2.zero);
	}
}

/**
 * a UI element that displays text from a specified font
 */
class UIText extends UIElement{

	constructor(){
		super();

		/** @type {String} */
		this._text = null;

		/** @type {CanvasImageSource} */
		this._textGraphic = null;

		/** @type {SpriteFont} */
		this.font = null;

		/** 
		 * @type {Boolean} whether or not the sizeOffset adjusts to fit the text whenever it is 
		 * modified 
		 */
		this.autoSize = true;
		this.sizeAnchor = Vec2.zero;
	}

	/**
	 * @type {UIText} creates a UI Text element from the specified string and spritefont
	 * @param {String} str the text that the element will display
	 * @param {SpriteFont} font the spritefont that should be used to generate the image, leave null for 
	 * default font
	 */
	static FromString(str, font = null){
		let r = new UIText();
		r._text = str;
		r.font = font || SpriteFont.default;
		return r;
	}

	/** @type {String} the text displayed by the element */
	get text(){
		return this._text;
	}
	set text(value){
		this._text = value;
		this._textGraphic = null;
	}

	/** @type {Vec2} the size of the generated tex graphic */
	get textRectSize(){

		// ensure graphic is generated
		if(this._textGraphic == null)
			this.GenerateTextGraphic();

		// return size of graphic
		return new Vec2(this._textGraphic.width, this._textGraphic.height);
	}

	/** generates the text graphic from the _text field and the font */
	GenerateTextGraphic(){
		this._textGraphic = this.font.GenerateTextImage(this._text, 1, -1);

		// resize if necessary
		if(this.autoSize)
			this.sizeOffset = new Vec2(this._textGraphic.width, this._textGraphic.height);
	}

	/** @override @param {Camera} cam */
	Draw(cam){

		// if the text image has not been generated yet, generate it
		if(this._textGraphic == null){
			this.GenerateTextGraphic();
		}

		// offset bounds by camera center offset
		let rect = this.relativeBounds;
		rect.position = rect.position.Minus(cam.offset);

		// draw the generated text image
		cam.DrawImage(
			this._textGraphic, 
			rect.topLeft,
			Vec2.zero
		);
	}
}

/**
 * a UI element that a player can interact with by selecting
 */
class UIButton extends UIElement{

	constructor(){
		super();

		this.isSelectable = true;

		/** @type {Sprite} */
		this._sprite = null;

		/** @type {Color} */
		this.outlineColor = Color.Grey;
		this.selectedOutlineColor = Color.White;

		/** @type {Function} */
		this.submitAction = null;
	}

	/** 
	 * @type {UIButton} 
	 * @param {CanvasImageSource} graphic
	 */
	static FromGraphic(graphic){

		let r = new UIButton();

		if(graphic != null)
			r._sprite = Sprite.FromGraphic(graphic);

		return r;
	}	
	
	/** 
	* @type {UIButton} 
	* @param {Sprite} graphic
	*/
	static FromSprite(sprite){
		let r = new UIButton();
		r._sprite = sprite;
		return r;
	}

	/** 
	 * @type {UIButton} 
	 * @param {String} text 
	 * @param {Number} size
	 * @param {Number} spacing
	 * @param {SpriteFont} font
	 */
	static FromText(text, size = 1, spacing = -1, font = null){
		
		// set the default font if not specified
		if(font == null)
			font = SpriteFont.default;
		
		// initialize a button object
		let r = new UIButton();

		// generate the text graphic and set it to the button
		let graphic = font.GenerateTextImage(text, 1, -1);
		r._sprite = Sprite.FromGraphic(graphic);
		
		return r;
	}

	/** @type {CanvasImageSource} */
	get graphic(){
		return this._sprite;
	}

	/**
	 * sets the size offset to match the button's graphic with the specified padding
	 * @param {Vec2} padding the amount of space between the graphic and button's boundaries
	 */
	SizeToGraphic(padding = Vec2.zero){
		this.sizeOffset = new Vec2(
			this._sprite.width + padding.x * 2,
			this._sprite.height + padding.y * 2
		);
	}

	/**
	 * @override
	 * @param {UIActionType} action 
	 * @param {UIElementContainer} target should be left null
	 */
	SendAction(action, target = null){

		switch(action){

			// only override submit action to do the specified button action if it has one
			case UIActionType.Submit:
				if(this.submitAction != null)
					this.submitAction();
				else
					super.SendAction(action, target);
				break;
			
			// otherwise perform the super class' SendAction method
			default:
				super.SendAction(action, target);
				break;
		}
	}

	/**
	 * @virtual draws the element at the specified position
	 * @param {Camera} cam 
	 */
	Draw(cam){

		// calculate the bounds of the button relative to the window, including camera offset
		let rect = this.relativeBounds;
		rect.position = rect.position.Minus(cam.offset);

		// draw the text or graphic of the button
		if(this._sprite != null)
			cam.DrawSprite(
				this._sprite,
				rect.center
			);
		
		// get the color that the outline is supposed to be
		let col = this.isSelected ? this.selectedOutlineColor : this.outlineColor;

		//draw the border
		cam.StrokeRect(rect, col.ToRGBA(), 1);
	}
}

// ------------------------------------------------------------------------------------------------

class UIInventorySlotGroup extends UIElementLayoutGroup{

	constructor(){
		super();

		/** @type {SlottedInventory} */
		this.inventory = null;

		/** @type {Boolean} */
		this._slotsDirty = true;

		/** @type {Array<UIFloatingElement>} */
		this._slotPopups = [];
	}

	/**
	 * @type {UIButton} creates a UI Button from the item / collectible specified
	 * @param {Number} index the index of the item in the inventory, set null to create empty slot
	 * @param {CollectibleContainer} container the container that the slot is inside of
	 */
	static CreateSlotElement(index, container){

		let itm = container.GetItemAt(index);

		// get the item icon if applicable
		let icon = null;
		if(itm != null)
			icon = itm.icon;
		let r = UIButton.FromSprite(icon);

		r.itmIndex = index;
		r.item = itm;
		r.selfAnchor = new Vec2(0.5);
		r.sizeAnchor = new Vec2(0);
		r.sizeOffset = new Vec2(20);

		if(itm != null){
			r.itemCount = itm.amount;
			if(itm != null && itm.amount > 1){
				let numText = UIText.FromString(itm.amount.toString());
				numText.selfAnchor = Vec2.one;
				numText.parentAnchor = Vec2.one;
				r.AddElement(numText);
			}
		}

		return r;
	}

	/**
	 * @type {UIInventorySlotGroup} creates an inventory slot group from the specified inventory
	 * @param {SlottedInventory} inv the slotted inventory to create the slot elements from
	 */
	static FromInventory(inv){
		let r = new UIInventorySlotGroup();

		r.inventory = inv;
		r.GenerateSlotElements();

		return r;
	}

	/** flags the inventory layout group to let it know that the slots need to be regenerated */
	MarkSlotsDirty(){
		this._slotsDirty = true;
	}

	/** regenerate the slot button child elements within the layout group */
	GenerateSlotElements(){

		// iterate through each slot in the inventory
		for(let i = 0; i < this.inventory.maxSlots; i++){

			// get reference to the item in the slot and the old UI slot element
			let oldSlot = this.GetDirectChildAt(i);
			let slotItm = this.inventory.slots[i];

			// make sure there is a slot UI element at the specified index
			if(oldSlot != null){

				// if the old UI slot existed
				if(oldSlot.item != null){

					// if there is something in the inventory slot and the old UI slot
					if(slotItm != null && oldSlot.item != null){
						
						// determine if the item in the old slot and the item in the inventory are
						// the same item or not
						let isEqual = false;
						if(slotItm.oid >= 0)
							isEqual =
								slotItm.oid == oldSlot.item.oid && 
								slotItm.amount == oldSlot.itemCount;
						else
							isEqual = slotItm == oldSlot.item;
						
						// if they are the same item, we do not need to recreate the slot
						if(isEqual)
							continue;
					}
				}

				// if there is nothing in the old slot
				else{

					// skip if slot does NOT need to be recreated
					if(slotItm == null){
						continue;
					}
				}
			}

			// create a new slot button element
			let newSlot = UIInventorySlotGroup.CreateSlotElement(i, this.inventory);

			// remove the old slot
			this.RemoveElementAt(i);

			// insert the new slot and mark the slot container dirty so that it repositions all 
			// the elements
			this.InsertElementsAt(i, newSlot);
			this.MarkDirty();
		}

		// mark the slot container clean to signify that all the slots are generated properly
		this._slotsDirty = false;
	}

	/**
	 * @type {Collectible} Grabs the specified stack size of an item from the slot and returns it
	 * @param {Number} index the index of the slot to grab from
	 * @param {Number} maxCount the max stack size of items to grab from the slot
	 */
	GrabItemFromSlot(index, maxCount = Number.POSITIVE_INFINITY){

		// if the slot is empty, do nothing
		let slotItm = this.inventory.slots[index];
		if(slotItm == null)
			return null;

		let r = slotItm;
		
		// if specified stack size is larger than the stack in the slot, grab all of them
		if(maxCount >= slotItm.amount){
			this.inventory.slots[index] = null;
			this.MarkSlotsDirty();
		}

		else{
			// create an item stack to return of the same type as in the slot, but with the specified
			// maxCount stack size
			r = this.inventory.slots[index].clone;
			r.amount = maxCount;

			// deduct the amount in the inventory that we took from the slot
			this.inventory.slots[index].amount -= maxCount;
		}
		
		// flag the slots to be regenerated and return the item stack and return the item 
		// stack that was grabbed
		this.MarkSlotsDirty();
		return r;
	}

	/**
	 * @type {Collectible} Adds an item of the specified stack size to an inventory slot, returns
	 * the items that were not added to the slot
	 * @param {Number} index the index of the slot to place it in
	 * @param {Collectible} item the item stack to place
	 * @param {Number} maxCount the max amount of items from the stack to place
	 */
	PlaceItemInSlot(index, item, maxCount = Number.POSITIVE_INFINITY){

		// ensure there is an item specified
		if(item == null)
			return item;
		
		let slotMaxStack = this.inventory.FindMaxStackSize(item);
		let toPlace = Math.min(slotMaxStack, maxCount, item.amount);
		let slotItem = this.inventory.slots[index];

		// if it's not empty, take into account the amount of items already in the slot
		if(slotItem != null){

			// if the items cannot be stacked, do nothing
			if(slotItem.oid < 0 || slotItem.oid != item.oid){
				return item;
			}

			toPlace = Math.min(toPlace, slotMaxStack - slotItem.amount);
		}

		// flag the slots to be regenerated
		this.MarkSlotsDirty();

		// if the whole stack can be placed
		if(item.amount <= toPlace){
			if(slotItem == null)
				this.inventory.slots[index] = item;
			else
				this.inventory.slots[index].amount += toPlace;
			return null;
		}

		// if only part of the stack can be placed
		else{

			// create empty item of same type if slot is empty
			if(slotItem == null){
				this.inventory.slots[index] = item.clone;
				this.inventory.slots[index].amount = 0;
			}
			this.inventory.slots[index].amount += toPlace;
			item.amount -= toPlace;
			return item;
		}

	}

	/**
	 * 
	 * @param {UIButton} target 
	 */
	CreateSlotPopup(target){

		/** @type {Collectible} */
		let itm = target.item;
		let nameStr = !!itm ? itm.name : "empty";
		let nameText = UIText.FromString(nameStr);
		
		// calculate the position and size of the name text element
		let nameRect = new Rect(Vec2.zero, nameText.textRectSize);
		if(this.uiContainer.cursorEnabled){
			nameRect.center = this.uiContainer.cursorPositionOffset.Plus(new Vec2(0, -25));
		}
		else{
			let tbound = target.bounds;
			nameRect.center = new Vec2(tbound.center.x, tbound.top + 15);
		}

		// create the name text Floating Element window
		target.popup = UIFloatingElement.FromElement(nameText, nameRect.Expanded(3), 0.5);
		target.popup.target = target;

		// open the window in the UIContainer
		this.uiContainer.AddWindowOnTop(target.popup);
		this._slotPopups.push(target.popup);
	}

	/** checks to make sure all the slot popups should still be there */
	CheckSlotPopups(){

		// iterate through each slot popup in the layout group
		for(let i = this._slotPopups.length - 1; i >= 0; i--){

			// check to see if the popup's target element, the item slot element that spawned 
			// it, is selected
			let popup = this._slotPopups[i];
			if(popup.target.isSelected){

				// extend the popup's lifetime
				popup.SetCurrentToMaxLife(0.5);

				// reposition the popup to follow cursor
				if(this.uiContainer.cursorEnabled){
					let rect = this._slotPopups[i].bounds;
					rect.center = this.uiContainer.cursorPositionOffset.Plus(new Vec2(0, -25));
					this._slotPopups[i].positionOffset = rect.topLeft;
				}
			}

			// otherwise, if the popup has been closed, remove it from the list
			else if (!popup.isOpen){
				this._slotPopups.splice(i, 1);
			}
		}
	}

	/** removes all the slot popups that were created by the layout group */
	RemoveAllSlotPopups(){
		
		// iterate through each slot popup in the layout group and remove it from the UI container
		for(let i = this._slotPopups.length - 1; i >= 0; i--){
			this.uiContainer.CloseWindow(this._slotPopups[i]);
			this._slotPopups.splice(i, 1);
		}
	}

	/** @override */
	SendAction(action, target = null){

		/** @type {UIFloatingElement} */
		let popup = target.popup;

		switch(action){
		
			case UIActionType.Select:

				// open the item name tooltip if it's selected
				if(popup == null || !popup.isOpen){
					this.CreateSlotPopup(target);
				}

				break;

			case UIActionType.Deselect:

				// close the item name tooltip if it's open
				if(popup != null && popup.isOpen){
					this.uiContainer.CloseWindow(popup);
				}

				break;

			case UIActionType.Submit:

				// if no stack in item reserve, grab the item stack
				if(this.uiContainer.itemReserve == null)
					this.uiContainer.itemReserve = this.GrabItemFromSlot(target.itmIndex);

				// otherwise place item from item reserve
				else
					this.uiContainer.itemReserve = this.PlaceItemInSlot(target.itmIndex, this.uiContainer.itemReserve);

				break;

			case UIActionType.AltSubmit:

				// if no stack in item reserve, grab half the item stack
				if(this.uiContainer.itemReserve == null){
					if(target.item != null){
						let count = Math.ceil(this.inventory.slots[target.itmIndex].amount / 2);
						this.uiContainer.itemReserve = this.GrabItemFromSlot(target.itmIndex, count);
					}
				}

				// otherwise place item from item reserve
				else
					this.uiContainer.itemReserve = this.PlaceItemInSlot(target.itmIndex, this.uiContainer.itemReserve, 1);

				break;

			default:
				super.SendAction(action, target);
				break;
		}
	}

	/** @override */
	Draw(cam){

		// regenerate slot elements if necessary
		if(this._slotsDirty)
			this.GenerateSlotElements();

		// keep popups active if necessary
		this.CheckSlotPopups();

		// call the super class Draw function which will organize the child elements if necessary
		super.Draw(cam);
	}
}