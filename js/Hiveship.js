///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

class Drone extends Character{

	constructor(){
		super();

		this.mass = 1200;
		this.thrustForce = 100000;

		this.team = 0;
		this.implements_collectibleContainer = true;

		/** @type {ArrayInventory} */
		this.collectibleContainer = new SlottedInventory;

		/** @type {Hiveship} */
		this.hiveship = null;

		/**@type {Rect} */
		this._collider = Collider.FromShape(new Rect(new Vec2(), new Vec2(10, 10)), this);

		/**@type {Object} */
		this.aimSensitivity = 0.01;
		this.input = { aimVector: new Vec2() };
		this.setEmptyInput();

		/** @type {InteractionTrigger} */
		this._interactionTarget = null;

		/**@type {Array<Weapon>} */
		this.weapons = [new BeamLaser(), new Cannon()];
		this.weaponIndex = 0;
		this.altWeaponIndex = 1;

		this.maxHealth = 25;
		this.health = 25;
		this.isFriendly = true;

	}

	Damage(damage, impulse, position){

		this.ApplyImpulse(impulse);
		this.health -= damage;

		if(this.health <= 0){
			this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.5)));
			this.Remove();
		}
	}

	/** @type {Sprite} */
	static get sprite_chasis(){

		// get the sprite asset or set it if it does not exist
		/** @type {Asset} */
		let asset = Sprite.spriteAsset_chasis;
		if(asset == null){
			asset = Sprite.spriteAsset_chasis = 
				AssetPack.GetAsset("vanilla", "sprites", "drone_chasis");
		}
	
		// return a global instance of the asset
		return asset.globalInstance;
	}

	/** @type {Sprite} */
	static get sprite_cannon(){
	
		// get the sprite asset or set it if it does not exist
		/** @type {Asset} */
		let asset = Sprite.spriteAsset_cannon;
		if(asset == null){
			asset = Sprite.spriteAsset_cannon = 
				AssetPack.GetAsset("vanilla", "sprites", "drone_cannon");
		}
	
		// return a global instance of the asset
		return asset.globalInstance;
	}

	/** @type {SpriteSheet} */
	static get spriteSheet_thrustFlame(){
	
		// get the sprite asset or set it if it does not exist
		/** @type {SpriteSheetAsset} */
		let asset = Sprite.spriteSheetAsset_thrustFlame;
		if(asset == null){
			asset = Sprite.spriteSheetAsset_thrustFlame = 
				AssetPack.GetAsset("vanilla", "spriteSheets", "thrustFlame");
		}

		// return a global instance of the asset
		return asset.globalInstance;
	}

	/** @type {Weapon} */
	get currentWeapon(){
		return this.weapons[this.weaponIndex];
	}
	/** @type {Weapon} */
	get altWeapon(){
		return this.weapons[this.altWeaponIndex];
	}

	get barrelPosition(){
		return this.position.Plus(Vec2.FromDirection(this.input.aimVector.direction, 6));
	}

	getTotalAmmoPercent(){

		let t = 0;
		let p = 0;
		for(let i = this.weapons.length - 1; i >= 0; i--){
			if(!Number.isFinite(this.weapons[i].maxAmmo))
				continue;

			t += 1;
			p += this.weapons[i].ammoPercent;
		}

		return p / t;
	}

	setEmptyInput(){
		this.input.isMoving = false;
		this.input.isMovingUp = false;
		this.input.isMovingDown = false;
		this.input.isMovingLeft = false;
		this.input.isMovingRight = false;
		this.input.interacting = false;
		this.input.interacting_initial = false;
		this.input.inventoryOpen = false;
		this.input.inventoryOpen_initial = false;
	}

	handleInput(){
		
		if(this.hiveship.uiContainer._windows.length > 0){
			this.setEmptyInput();
			return;
		}

		// w
		if(Input.keysDown[87])
			this.input.isMovingUp = true;
		else this.input.isMovingUp = false;

		// s
		if(Input.keysDown[83])
			this.input.isMovingDown = true;
		else this.input.isMovingDown = false;

		// a
		if(Input.keysDown[65])
			this.input.isMovingLeft = true;
		else this.input.isMovingLeft = false;

		// d
		if(Input.keysDown[68])
			this.input.isMovingRight = true;
		else this.input.isMovingRight = false;

		// whether or not it's moving at all
		this.input.isMoving = (this.input.isMovingRight ||
			this.input.isMovingLeft ||
			this.input.isMovingUp ||
			this.input.isMovingDown);

		// e
		if(Input.keysDown[69]){
			if(!this.input.interacting)
				this.input.interacting_initial = true;
			else this.input.interacting_initial = false;
			this.input.interacting = true;
		}
		else {
			this.input.interacting = false;
			this.input.interacting_initial = false;
		}
		
		// Tab
		if(Input.keysDown[9]){
			if(!this.input.inventoryOpen)
				this.input.inventoryOpen_initial = true;
			else this.input.inventoryOpen_initial = false;
			this.input.inventoryOpen = true;
		}
		else {
			this.input.inventoryOpen = false;
			this.input.inventoryOpen_initial = false;
		}

		// left click
		if(Input.mouseButtonsDown[0]){
			if(!this.input.isFiring)
				this.input.isFiring_initial = true;
			else this.input.isFiring_initial = false;
			this.input.isFiring = true;
		}
		else {
			this.input.isFiring_initial = false;
			this.input.isFiring = false;
		}
		
		// right click
		if(Input.mouseButtonsDown[2]){
			if(!this.input.isAltFiring)
				this.input.isAltFiring_initial = true;
			else this.input.isAltFiring_initial = false;
			this.input.isAltFiring = true;
		}
		else {
			this.input.isAltFiring_initial = false;
			this.input.isAltFiring = false;
		}

		this.input.aimVector.Translate(Input.mouseDelta.Times(this.aimSensitivity));
		if(this.input.aimVector.magnitude > 1)
			this.input.aimVector.Normalize();
	}

	handleActions(dt){

		// interact action is triggered
		if(this.input.interacting_initial){
			
			// trigger the target if there is one
			if(this._interactionTarget != null)
				this._interactionTarget.OnInteract(this);
		}

		// inventory open is triggered
		if(this.input.inventoryOpen_initial){
			this.OpenInventoryUI(this.collectibleContainer);
		}
	}

	handleWeapons(dt){

		let barrelPos = this.barrelPosition;
		let aim = this.input.aimVector.direction;
		let altWeaponValid =  this.altWeaponIndex != this.weaponIndex && !!this.altWeapon;

		if(!this.input.isAltFiring || !altWeaponValid)
			this.currentWeapon.Update(barrelPos, aim, this, dt);
		
		if(this.input.isFiring_initial){
			this.currentWeapon.Trigger_begin(barrelPos, aim, this);
		}
		if(this.input.isFiring){
			this.currentWeapon.Trigger(barrelPos, aim, this, dt)
		}
		

		if(altWeaponValid){
			if(!this.input.isFiring)
				this.altWeapon.Update(barrelPos, aim, this, dt);

			if(this.input.isAltFiring_initial){
				this.altWeapon.Trigger_begin(barrelPos, aim, this);
			}
			if(this.input.isAltFiring){
				this.altWeapon.Trigger(barrelPos, aim, this, dt)
			}
		}
	}

	handleMovement(dt){

		let thrustVector = new Vec2();
		if(this.input.isMovingUp){
			 thrustVector.Translate(new Vec2(0, -1));
		}
		if(this.input.isMovingDown){
			thrustVector.Translate(new Vec2(0, 1));
		}
		if(this.input.isMovingLeft){
			thrustVector.Translate(new Vec2(-1, 0));
		}
		if(this.input.isMovingRight){
			thrustVector.Translate(new Vec2(1, 0));
		}

		this.ApplyForce(thrustVector.Times(this.thrustForce), dt);
	}
	
	/**
	 * opens the drone's inventory UI
	 */
	OpenInventoryUI(){
		let invWindow = UIInventoryWindow.FromInventory(this.collectibleContainer);
		this.hiveship.uiContainer.AddWindowOnTop(invWindow);
		this.hiveship.uiContainer.cursorEnabled = true;
	}

	/**
	 * finds an overlapping interaction trigger that the player is able to interact with, and
	 * sets the interaction target to that trigger
	 */
	findInteractionTarget(){

		// define a variable to hold the chosen trigger target
		let trig = null;

		// iterate through each trigger the player is overlapping
		let colliders = this.collider.FindOverlappingColliders();
		for(let i = colliders.length - 1; i >= 0; i--){

			// if the player can interact with the trigger, select it and break the loop
			if(colliders[i].parentObject.implements_interaction){
				if(colliders[i].parentObject.CanBeIntractedFrom(this)){
					trig = colliders[i].parentObject;
					break;
				}
			}
		}

		// set the interaction target
		this._interactionTarget = trig;
	}

	drawThrust(camera){

		let upOff = new Vec2(-0.5, -4);
		let downOff = new Vec2(0.5, 4);
		let leftOff = new Vec2(-4, 0.5);
		let rightOff = new Vec2(4, -0.5);
		let frame = Math.floor(this.container.timeSimulated * 1000 % Drone.spriteSheet_thrustFlame.sprites.length);

		if(this.input.isMovingDown)
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(upOff), 0, false);
		if(this.input.isMovingUp)
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(downOff), Math.PI, false);
		if(this.input.isMovingRight)
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(leftOff), Math.PI / -2, false);
		if(this.input.isMovingLeft)
			camera.DrawSpriteFromSheet(Drone.spriteSheet_thrustFlame, frame, this.position.Plus(rightOff), Math.PI / 2, false);
	}

	onUpdate(dt){

		this.findInteractionTarget();
		this.handleInput();
		this.handleActions();
		this.handleMovement(dt);
		this.handleWeapons(dt);
	}

	/**
	 * @param {Camera} camera 
	 */
	onDraw(camera){

		camera.DrawSprite(Drone.sprite_chasis, this.position, 0, false);
		this.drawThrust(camera);
		camera.DrawSprite(Drone.sprite_cannon, this.position, this.input.aimVector.direction, false);

		let aimOff = this.input.aimVector.Times(10);
		aimOff = this.barrelPosition.Plus(aimOff);
		this.currentWeapon.DrawReticle(camera, aimOff, this.input.aimVector.direction);
	}
}

class Hiveship extends Character{
	
	constructor(){
		super();

		this.implements_interaction = true;

		this.team = 0;
		this.renderOrder = 4;

		this.mass = 18200500;
		this.velocity = new Vec2(10, 0);

		this.droneRange = 600;
		this.radarRange = 600;

		/**@type {CanvasRenderingContext2D} */
		this._radarContext = null;

		/** @type {Number} for debug */
		this.radarIterations = 0;

		/**@type {Collider} */
		this._collider = Collider.FromShape(new Rect(new Vec2(), new Vec2(90, 60)), this);
		
		this.centerHitbox();

		this.maxHealth = 100;
		this.health = this.maxHealth;
		this.isFriendly = true;

		/** @type {Drone} */
		this.drone = null;

		this.droneFabricationDelay = 5;
		this.droneFabricationTime = 0;

		/** @type {HiveshipUIContainer} */
		this.uiContainer = null;

		/** @type {UIWindow} */
		this.uiWindow = null;
	}

	static get sprite_ship(){

		if(Hiveship.spriteAsset_ship == null)
			Hiveship.spriteAsset_ship = AssetPack.GetAsset("vanilla", "sprites", "hiveShip");
		return Hiveship.spriteAsset_ship.globalInstance;
	}

	/**@type {Number} returns the distance between the hiveship and drone*/
	get droneDistance(){
		return this.drone.position.Minus(this.position).magnitude;
	}

	/**@type {Number} returns the strength of the hiveship's signal to the drone */
	get droneSignal(){
		return 1 - this.droneDistance / this.droneRange;
	}

	/**@type {HTMLCanvasElement} */
	get radarCanvas(){
		return this._radarContext.canvas;
	}

	centerHitbox(){
		this._collider.CenterOnParent();
	}

	doDroneRepair(dt){

		this.droneFabricationTime -= dt;
		if(this.droneFabricationTime <= 0){
			this.finishDroneRepair();
		}
	}

	finishDroneRepair(dt){

		let drone = new Drone();
		drone.hiveship = this;
		drone.position = this.position.clone;
		
		drone.weapons = this.drone.weapons;
		drone.velocity = this.velocity.clone;
		drone.weaponIndex = this.drone.weaponIndex;
		drone.altWeaponIndex = this.drone.altWeaponIndex;
		
		this.drone = drone;
		this.container.AddGameObject(drone);

		this.droneFabricationTime = 0;
	}

	/** @override @param {GameObjectContainer} container */
	onAdded(container){
		super.onAdded(container);

		if(this.uiContainer != null)
			this.uiContainer.CloseAllWindows();

		// get the resolution of the parent state if available, otherwise default to 320x320
		let res = container.parentState.camera != null ?
			container.parentState.camera.resolution : 
			new Vec2(320);

		// construct a UIContainer with the specified resolution
		this.uiContainer = HiveshipUIContainer.FromHiveship(this, res);
	}

	/** @override */
	onRemoved(){
		super.onRemoved();
		this.interactTrigger.Remove();
	}

	onUpdate(dt){
		super.onUpdate(dt);

		if(this.health <= 0){
			this.deathUpdate(dt);
			return;
		}

		if(!this.drone.isRemoved && this.drone.position.Minus(this.position).magnitude > this.container.minimapRange + 200){
			this.drone.Damage(1000, Vec2.zero, Vec2.zero);
		}

		if(this.droneFabricationTime > 0){
			this.doDroneRepair(dt);
		} else if(this.drone.isRemoved){
			this.droneFabricationTime = this.droneFabricationDelay;
		}
	}

	deathUpdate(dt){

		this.droneFabricationTime -= dt;
		if(this.droneFabricationTime <= 0){
			
			let pos = new Vec2(
				this._collider.shape.width * Math.random(), 
				this._collider.shape.height * Math.random()
			).Plus(this._collider.shape.position);

			this.container.AddGameObject(Effect.SmallExplosion(pos, Vec2.zero));
			this.droneFabricationTime = Math.random() * 0.25 + 0.05;
			this.container.QueueSimpleSound(Camera.sfx_HiveshipExplode, pos);
		}
	}

	Damage(damage, impulse, position){

		this.health -= damage;

		if(this.health <= 0){
			this.container.AddGameObject(Effect.SmallExplosion(this.position, this.velocity.Times(0.5)));
			if(!this.drone.isRemoved)
				this.drone.Damage(100000, Vec2.zero);
			this.droneFabricationTime = 0;
		}
	}

	/**
	 * returns true if it can be interacted from the specified interactor
	 * @param {GameObject} interactor 
	 */
	CanBeIntractedFrom(interactor){

		// only allow the hiveship's drone to interact with it
		return this.drone == interactor;
	}

	/**
	 * Called when an object interacts with the hiveship
	 * @param {Drone} interactor 
	 */
	OnInteract(interactor){

		// TODO  implement upgrade / shop interface
		this.uiWindow = this.uiContainer.AddWindowOnTop(UITitledWindow.FromTitle("Upgrade"), true);
		this.uiContainer.cursorEnabled = true;
	}

	/**
	 * @type {String} returns the text to prompt the user to interact
	 * @param {Character} interactor
	 */
	GetInteractionText(interactor){
		return "Manage Hiveship";
	}

	/**
	 * Initializes a radar canvas with the specified width/height
	 * @param {Number} size 
	 */
	InitializeRadarCanvas(size){

		// create a canvas element
		let canvas = document.createElement("canvas");

		// set the canvas's size to the specified resolution
		canvas.width = size;
		canvas.height = size;

		// set the reference to the canvas context
		this._radarContext = canvas.getContext("2d");
	}

	/**
	 * Renders everything within radar range to the specified canvas context
	 * @param {CanvasRenderingContext2D} ctx 
	 */
	DrawRadar(){

		let ctx = this._radarContext;

		// clear the whole rect
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		
		// render the radar circle
		ctx.fillStyle = "rgba(255,255,255,0.25)";
		ctx.strokeStyle = "rgba(255,255,255,0.5)";
		ctx.beginPath();
		ctx.arc(ctx.canvas.width / 2, ctx.canvas.height / 2, ctx.canvas.height / 2, 0, Math.PI * 2);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();

		// store a reference to self for anonymous function iteration
		let ths = this;

		// find the world rect where all possible objects within radar range exist
		let radarRect = Rect.FromRadius(this.radarRange, this.position);

		// iterate through each object that could be within radar range
		this.container.objectPartitions.IterateThroughWorldRectObjects(radarRect,
			function(obj){

				// for debug and performance benchmarking
				ths.radarIterations++;

				// write the object to the radar context
				if(obj.isThreat || obj.isFriendly)
					ths.WriteObjectToRadar(obj);
			});
	}

	/** 
	 * @param {Character} obj 
	 */
	WriteObjectToRadar(obj){
		
		let dpos = obj.position.Minus(this.position);

		if(dpos.magnitude > this.radarRange){
			if(!(obj instanceof Drone)){
				return;
			}
			else{
				dpos = dpos.normalized.Times(this.radarRange - 20);
			}
		}

		dpos = dpos.Times((this._radarContext.canvas.width / 2) / this.radarRange);
		dpos = dpos.Plus(new Vec2(this._radarContext.canvas.width / 2));
		dpos = dpos.rounded;

		let size = 1;
		if(obj.isThreat){

			size = 1;
			let alpha = 1;

			if(obj instanceof Asteroid){
				switch(obj.tier){
					case 1: alpha = 0.25; break;
					case 2: alpha = 0.5; break;
					case 4: size = 2; break;
					case 5: size = 2; break;
				}
			}
			if(obj instanceof Projectile){
				alpha = 0.25;
			}

			this._radarContext.fillStyle = Character.AreTeamsHostile(obj.team, this.team) ? 
				"rgba(255,48,0," + alpha.toString() +")" : 
				"rgba(255,192,0," + alpha.toString() +")";
		}
		if(obj.isFriendly){

			size = 1;
			if(obj instanceof Hiveship)
				size = 3

			this._radarContext.fillStyle = "#0AF";
		}
	
		dpos = dpos.Plus(new Vec2(size / -2)).rounded;
		this._radarContext.fillRect(dpos.x, dpos.y, size, size);
	}

	/**
	 * @param {Camera} camera 
	 */
	onDraw(camera){

		camera.DrawSprite(Hiveship.sprite_ship, this.position, 0, false);
		//camera.StrokeRect(this._hitbox, "#3F3", 1);
	}
}