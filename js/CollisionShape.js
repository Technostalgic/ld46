///
/// 	code by Isaiah Smith
/// 	
/// 	https://technostalgic.tech  
/// 	twitter @technostalgicGM
///

/**
 * @abstract
 */
class CollisionShape{

	/** @type {CollisionShape} */
	constructor(){ }

	/** @abstract @type {Vec2} the geometric center of the shape*/
	get centroid(){ }
	/** @abstract @param {Vec2} value */
	set centroid(value){ }

	/** @type {Rect} the bounding box that completely encapsulates the shape*/
	get boundingBox(){ }

	/**
	 * @type {RaycastResult}
	 * @param {CollisionShape} shape 
	 * @param {Ray} ray 
	 */
	static RayCast(shape, ray){	return shape.RayCast(ray); }
	/**
	 * @type {CirclecastResult}
	 * @param {CollisionShape} shape 
	 * @param {Ray} ray 
	 * @param {Number} radius 
	 */
	static CircleCast(shape, ray, radius){ return shape.CircleCast(ray, radius); }

	/**
	 * @abstract @type {Vec2} returns a location which is still inside the shape that's as close as
	 * possible to the specified point
	 * @param {Vec2} point 
	 */
	ClosestPoint(point){ throw "Not Implemented"; }
	/** 
	 * @abstract @type {Boolean} returns true if the point lies within the shape
	 * @param {Vec2} point
	*/
	OverlapsPoint(point){ throw "Not Implemented"; }
	/** 
	 * @abstract @type {Boolean} 
	 * @param {Rect} rect
	 */
	OverlapsRect(rect){ throw "Not Implemented"; }
	/**
	 * @abstract @type {Boolean}
	 * @param {Circle} circ 
	 */
	OverlapsCircle(circ){ throw "Not Implemented"; }
	/**
	 * @type {Boolean}
	 * @param {CollisionShape} shape 
	 */
	OverlapsShape(shape){ 
		if(shape instanceof Rect) return this.OverlapsRect(shape);
		if(shape instanceof Circle) return this.OverlapsCircle(shape);
		return false;
	}
	/**
	 * @abstract @type {Vec2} finds what the normal should be of the shape at any given point, 
	 * even for points that are not inside of or on the edge of the shape
	 * @param {Vec2} point
	 */
	FindImplicitNormal(point){ throw "Not Implemented"; }

	/**
	 * @abstract @type {RaycastResult} casts a ray to test for collision with the collision shape
	 * @param {Ray} ray the ray to test against
	*/
	RayCast(ray){ }
	/**
	 * @abstract @type {CircleCastResult} casts a ray with a non-zero width to test for collision 
	 * with the collision shape
	 * @param {Ray} ray the ray to test against
	 * @param {Number} radius the width of the ray to cast
	*/
	CircleCast(ray, radius){ }

	/**
	 * @abstract
	 * @param {Camera} ctx the context used to render the shape
	 * @param {Color} color the color that will be used when rendering the fill
	 */
	DrawFill(cam, color){ }
	/**
	 * @abstract
	 * @param {Camera} ctx the context used to render the shape
	 * @param {Color} color the color that will be used when rendering the outline
	 * @param {Number} lineWidth the width of the outline
	 */
	DrawOutline(cam, color, lineWidth = 1){ }
}

/**
 * a data structure that represents an axis-aligned bounding box
 */
class Rect extends CollisionShape{

	/**
	 * @type {Rect}
	 * @param {Vec2} position the position of the top left of the rectangle
	 * @param {Vec2} size the width and height of the rectangle
	 */
	constructor(position = new Vec2(), size = new Vec2()){
		super();

		/** @type {Vec2} */
		this.position = position;

		/** @type {Vec2} */
		this.size = size;
	}

	/**@type {Rect}*/
	static FromSides(left, top, right, bottom) {
		return new Rect(new Vec2(left, top), new Vec2(right - left, bottom - top));
	}

	/**
	 * @type {Rect} creates an axis aligned rectangle from two points
	 * @param {Vec2} pointA
	 * @param {Vec2} pointB
	*/
	static FromPoints(pointA, pointB){
	
			// define the min and max boundaries
			let minX = Math.min(pointA.x, pointB.x);
			let minY = Math.min(pointA.y, pointB.y);
			let maxX = Math.max(pointA.x, pointB.x);
			let maxY = Math.max(pointA.y, pointB.y);
	
			// return a rectangle from the calculated sides
			return Rect.FromSides(minX, minY, maxX, maxY);
	}

	/**
	 * @type {Rect} creates a rectangle that completely overlaps a circle with the specified 
	 * radius and center
	 * @param {Number} radius the radius of the circle
	 * @param {Vec2} center the position of the center of the circle
	 */
	static FromRadius(radius, center){
			
			// create a new rect
			let r = new Rect();
	
			// set it's bounds based on the circle position and radius
			r.left = center.x - radius;
			r.right = center.x + radius;
			r.top = center.y - radius;
			r.bottom = center.y + radius;
	
			return r;
	}
	
	/**@type {Rect} converts from anonymous object into rect type */
	static FromObjectData(data){
		let position = Vec2.FromObjectData(data.position);
		let size = Vec2.FromObjectData(data.size);
		return new Rect(position, size);
	}

	/**@type {Rect}*/
	static FromJSONArray(arr){
		let r = new Rect();

		r.position = new Vec2(arr[0],arr[1]);
		r.size = new Vec2(arr[2],arr[3]);

		return r;
	}

	/**
	 * @type {Rect} Creates a new rectangle that overlaps both the specified rectangles
	 * @param {Rect} rectA 
	 * @param {Rect} rectB 
	 */
	static OverlapRect(rectA, rectB){

		// find the top left of the overlap rect by getting the min x and y values of rect A & B
		let min = new Vec2(
			Math.min(rectA.left, rectB.left), // the left-most x value of both rectangles
			Math.min(rectA.top, rectB.top) // the highest y value of both rectangles
		);

		// find the bottom right of the overlap rect by getting the min x and y of rect A & B
		let max = new Vec2(
			Math.max(rectA.right, rectB.right), // the right-most x value of both rectangles
			Math.max(rectA.bottom, rectB.bottom) // the lowest y value of both rectangles
		);

		// construct and return a new rectangle from the two points we found
		return Rect.FromPoints(min, max);
	}

	/**
	 * @type {Rect} returns the area of two intersecting rectangles, if they are not intersecting
	 * it will return null
	 * @param {Rect} rectA 
	 * @param {Rect} rectB 
	 */
	static IntersectRect(rectA, rectB){

		// find the right-most left edge of the two rects and the lowest top edge
		let min = new Vec2(
			Math.max(rectA.left, rectB.left),
			Math.max(rectA.top, rectB.top)
		);

		// find the left-most right edge of the two rects and the highest bottom edge
		let max = new Vec2(
			Math.min(rectA.right, rectB.right),
			Math.min(rectA.bottom, rectB.bottom)
		);

		// determine the rectangle's size from the max and min points
		let size = max.Minus(min);

		// if the rectangle has negative or zero area, return null
		if(size.x <= 0 || size.y <= 0) return null;

		// return new rect constructed from the min (top left) and computed size
		return new Rect(min, size);
	}

	/**@type {Rect}*/
	get boundingBox(){ return this; }

	/**@type {Vec2}*/
	get topLeft(){ return new Vec2(this.position.x, this.position.y); }
	set topLeft(value){
		let dpos = value.Minus(this.topLeft);
		this.position.Translate(dpos);
	}
	/**@type {Vec2}*/
	get topRight(){ return new Vec2(this.right, this.position.y); }
	set topRight(value){
		let dpos = value.Minus(this.topRight);
		this.position.Translate(dpos);
	}
	/**@type {Vec2}*/
	get bottomLeft(){ return new Vec2(this.position.x, this.bottom); }
	set bottomLeft(value){
		let dpos = value.Minus(this.bottomLeft);
		this.position.Translate(dpos);
	}
	/**@type {Vec2}*/
	get bottomRight(){ return new Vec2(this.right, this.bottom); }
	set bottomRight(value){
		let dpos = value.Minus(this.bottomRight);
		this.position.Translate(dpos);
	}

	/** @type {Vec2} */
	get center(){
		return this.position.Plus(this.size.Times(0.5));
	}
	set center(value){
		this.position = value.Plus(this.size.Times(-0.5));
	}

	/** @type {Vec2} */
	get centroid(){ return this.center; }
	set centroid(value) { this.center = value; }

	/** @type {Vec2} */
	get left(){ return this.position.x; }
	set left(value) { this.size.x = this.right - value; this.position.x = value; }
	/** @type {Vec2} */
	get right(){ return this.position.x + this.size.x; }
	set right(value) { this.size.x = value - this.position.x; }
	/** @type {Vec2} */
	get top() { return this.position.y; }
	set top(value) { this.size.y = this.bottom - value; this.position.y = value; }
	/** @type {Vec2} */
	get bottom(){ return this.position.y + this.size.y; }
	set bottom(value) { this.size.y = value - this.position.y; }
	
	/**@type {Number}*/
	get width() { return this.size.x; }
	set width(value) { this.size.x = value; }
	/**@type {Number}*/
	get height() { return this.size.y; }
	set height(value) { this.size.x = value; }

	/**@type {Number}*/
	get area() { return this.size.x * this.size.y; }
	/**@type {Boolean}*/
	get isEmpty() { return this.area <= 0; }

	/** @type {Rect} a new instance of the rect with the same values */
	get clone(){ return new Rect(this.position.clone, this.size.clone); }

	/**
	 * @type {Boolean} checks to see if this rect has the same position and size as another
	 * @param {Rect} rect 
	 */
	IsEqual(rect){
		return (
			this.position.x == rect.position.x &&
			this.position.y == rect.position.y &&
			this.size.x == rect.size.x &&
			this.size.y == rect.size.y );

	}
	/**
	 * @override @type {Vec2} returns a location which is still inside the shape that's as close as
	 * possible to the specified point
	 * @param {Vec2} point 
	 */
	ClosestPoint(point){ 
		let x = Math.max(Math.min(point.x, this.right), this.left);
		let y = Math.max(Math.min(point.y, this.bottom), this.top);
		return new Vec2(x,y);
	}
	/**
	 * @override
	 * @param {Rect} rec 
	 */
	OverlapsRect(rec){
		return (
			this.left <= rec.right &&
			this.right >= rec.left &&
			this.top <= rec.bottom &&
			this.bottom >= rec.top );
	}
	/**
	 * @override
	 * @param {Circle} circ 
	 */
	OverlapsCircle(circ){

		// return true if the center if the circle is inside the rect
		let inside = this.OverlapsPoint(circ.center);
		if(inside)
			return true;

		// otherwise return true if the closest point within the rect is in within the circle's radius
		return this.ClosestPoint(circ.center).Minus(circ.center).magnitude <= circ.radius;
	}
	/** 
	 * @type {Boolean} 
	 * @param {Vec2} point
	*/
	OverlapsPoint(point){
		return (
			point.x <= this.right &&
			point.x >= this.left &&
			point.y <= this.bottom &&
			point.y >= this.top );
	}
	/**
	 * @type {Rect} expands the edges of the rectangle outwards from the center by the specified 
	 * size and returns the result
	 * @param {Number} size 
	 */
	Expanded(size){
		return new Rect(
			this.position.Minus(new Vec2(size)), 
			this.size.Plus(new Vec2(2 * size))
			);
	}
	/** 
	 * expands the size of the rect by the specified amount of pixels per side without changing the rect's center 
	 * @param {Number} horizontal
	 * @param {Number} vertical
	 * */
	AddPadding(horizontal, vertical = horizontal) {
		let ocent = this.center;
		this.size = this.size.Plus(new Vec2(horizontal, vertical).Scaled(2));
		this.center = ocent;
	}

	/**
	 * @type {RaycastResult} casts a ray against the specified rectangle and if hit, returns
	 * information about the raycast hit, otherwise returns null 
	 * (algorithm adapted from "zacharmarz" on gamedev.stackexchange.com)
	 * @param {Vec2} rayStart the origin of the ray
	 * @param {Vec2} rayEnd the vector direction that the ray is pointing in
	 */
	RayCast(ray) {

		// define the t values
		let t1 = (this.left - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t2 = (this.right - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t3 = (this.top - ray.rayStart.y) * ray.dirfrac.y || -1;
		let t4 = (this.bottom - ray.rayStart.y) * ray.dirfrac.y || -1;

		// find t min/max values
		let tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
		let tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));

		// if tmin > tmax, ray doesn't intersect AABB
		if (tmin > tmax) {
			return RaycastResult.Invalid;
		}

		// define and return the raycast hit information
		let r = new RaycastResult_rect(ray, tmin, tmax, t1, t2, t3, t4);
		return r;
	}
	/**
	 * @override @type {CirclecastResult}
	 * @param {Ray} ray 
	 * @param {Number} radius 
	 */
	CircleCast(ray, radius){
		
		// define the t values
		let t1 = (this.left - radius - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t2 = (this.right + radius - ray.rayStart.x) * ray.dirfrac.x || -1;
		let t3 = (this.top - radius - ray.rayStart.y) * ray.dirfrac.y || -1;
		let t4 = (this.bottom + radius - ray.rayStart.y) * ray.dirfrac.y || -1;

		// find t min/max values
		let tmin = Math.max(Math.min(t1, t2), Math.min(t3, t4));
		let tmax = Math.min(Math.max(t1, t2), Math.max(t3, t4));

		// if tmin > tmax, ray doesn't intersect AABB
		if (tmin > tmax) {
			return RaycastResult.Invalid;
		}

		// define and return the raycast hit information
		let r = new CirclecastResult_rect(ray, radius, tmin, tmax, t1, t2, t3, t4, this);
		return r;
	}

	/** @returns {Array<Number>} */
	ToJSONArray(){
		return [this.position.x, this.position.y, this.width, this.height];
	}

	/**
	 * @param {Camera} cam 
	 * @param {Color} color 
	 */
	DrawFill(cam, color = Color.White) {

		let tpos = cam.WorldToViewportPosition(this.position);
		let ctx = cam.renderContext;

		ctx.fillStyle = color.ToRGBA();
		ctx.fillRect(
			Math.round(tpos.x), Math.round(tpos.y), 
			Math.round(this.width * cam.zoom), Math.round(this.height * cam.zoom));
	}
	/**
	 * @param {Camera} cam 
	 * @param {String} strokeStyle 
	 * @param {Number} lineWidth 
	 */
	DrawOutline(cam, color = Color.White, lineWidth = 1){

		let tpos = cam.WorldToViewportPosition(this.position);
		let ctx = cam.renderContext;

		ctx.strokeStyle = color.ToRGBA();
		ctx.lineWidth = lineWidth;

		ctx.beginPath();
		ctx.moveTo(tpos.x, tpos.y);
		ctx.lineTo(tpos.x + this.width * cam.zoom, tpos.y);
		ctx.lineTo(tpos.x + this.width * cam.zoom, tpos.y + this.height * cam.zoom);
		ctx.lineTo(tpos.x, tpos.y + this.height * cam.zoom);
		ctx.closePath();
		ctx.stroke();
	}
}

/**
 * a data structure that represents a simple circle, that is a point in space and a radius
 */
class Circle extends CollisionShape{

	/**
	 * @type {Circle}
	 * @param {Vec2} center the position of the center of the circle
	 * @param {Number} radius the circle's radius
	 */
	constructor(center, radius){
		super();
		this.center = center;
		this.radius = radius;
	}

	/** @type {Vec2} */
	get centroid() { return this.center; }
	set centroid(value) { this.center = value; }
	
	/** @type {Rect} */
	get boundingBox(){
		return new Rect(
			this.center.Minus(new Vec2(this.radius)), 
			new Vec2(this.radius * 2) );
	}

	/**
	 * @override @type {Vec2} returns a location which is still inside the shape that's as close as
	 * possible to the specified point
	 * @param {Vec2} point 
	 */
	ClosestPoint(point){ 

		// if the point is inside the circle, return the a point at the same location
		let dif = point.Minus(this.center);
		if(dif.magnitude <= this.radius)
			return point.clone;

		return this.center.Plus(dif.normalized.Times(this.radius));
	}
	/** 
	 * @type {Boolean} returns true if the specified point lies within the circle
	 * @param {Vec2} point
	*/
	OverlapsPoint(point){
		return (this.center.Minus(point).magnitude <= this.radius);
	}
	/**
	 * @type {Boolean}
	 * @param {Rect} rect
	 */
	OverlapsRect(rect){
		return rect.OverlapsCircle(this);
	}
	/**
	 * @type {Boolean}
	 * @param {Circle} circ 
	 */
	OverlapsCircle(circ){
		return this.center.Minus(circ.center).magnitude <= this.radius + circ.radius;
	}

	/**
	 * @override @type {RaycastResult}
	 * @param {Ray} ray the ray being cast
	 */
	RayCast(ray){
		
		// offset the line to be relative to the circle
		let rayStart = ray.rayStart.Minus(this.center);
		let rayEnd = ray.rayEnd.Minus(this.center);
		
		// calculate the dot products between the origin and target vectors
		let aoa = rayStart.Dot(rayStart);
		let aob = rayStart.Dot(rayEnd);
		let bob = rayEnd.Dot(rayEnd);

		// calculate the ratios used to find the determinant and the t-values
		let qa = aoa - 2.0 * aob + bob;
		let qb = -2.0 * aoa + 2.0 * aob;
		let qc = aoa - this.radius * this.radius;

		// caclulate the determinant used to determine if there was an intersection or not
		let determinant = qb * qb - 4.0 * qa * qc;
		
		// if the determinant is negative, the ray completely misses
		if(determinant >= 0.0){
			 
			// compute the nearest of the two t values
			let t1 = (-qb - Math.sqrt(determinant)) / (2.0 * qa);

			// compute the furthest of the t values
			let t2 = (-qb + Math.sqrt(determinant)) / (2.0 * qa);

			return new RaycastResult_circle(ray, this.center, t1, t2);
		}
		else{
			return RaycastResult_circle.Invalid;
		}
	}

	/**
	 * @override @type {CircleCastResult}
	 * @param {Vec2} rayStart the start point of the ray
	 * @param {Vec2} rayEnd the end point of the ray
	 * @param {Number} radius the width of the ray to cast
	 */
	CircleCast(ray, radius){
		
		// offset the line to be relative to the circle
		let a = ray.rayStart.Minus(this.center);
		let b = ray.rayEnd.Minus(this.center);
		
		// add the ray radius to the circle radius to get the total radius
		let tRadius = this.radius + radius;
		
		// calculate the dot products between the origin and target vectors
		let aoa = a.Dot(a);
		let aob = a.Dot(b);
		let bob = b.Dot(b);

		// calculate the ratios used to find the determinant and the t-values
		let qa = aoa - 2.0 * aob + bob;
		let qb = -2.0 * aoa + 2.0 * aob;
		let qc = aoa - tRadius * tRadius;

		// caclulate the determinant used to determine if there was an intersection or not
		let determinant = qb * qb - 4.0 * qa * qc;
		
		// if the determinant is negative, the ray completely misses
		let r = null;
		if(determinant >= 0.0){
			 
			// compute the nearest of the two t values
			let t1 = (-qb - Math.sqrt(determinant)) / (2.0 * qa);

			// compute the furthest of the t values
			let t2 = (-qb + Math.sqrt(determinant)) / (2.0 * qa);

			return new CirclecastResult_circle(ray, radius, this.center, t1, t2);
		}
		else{
			return CirclecastResult.Invalid;
		}
	}
	
	/**
	 * @override
	 * @param {Camera} cam 
	 * @param {Color} color 
	 * @param {Number} lineWidth 
	 */
	DrawOutline(cam, color = Color.White, lineWidth = 1){

		let tpos = cam.WorldToViewportPosition(this.center);
		let ctx = cam.renderContext;

		ctx.strokeStyle = color.ToRGBA();
		ctx.lineWidth = lineWidth;
		ctx.beginPath();
		ctx.arc(
			tpos.x,
			tpos.y,
			this.radius,
			0, Math.PI * 2 );
		ctx.closePath();
		ctx.stroke();
	}

	/**
	 * @override
	 * @param {Camera} cam 
	 * @param {Color} color  
	 */
	DrawFill(cam, color = Color.White){
		
		let tpos = cam.WorldToViewportPosition(this.center);
		let ctx = cam.renderContext;
		
		ctx.fillStyle = color.ToRGBA();
		ctx.beginPath();
		ctx.arc(
			tpos.x,
			tpos.y,
			this.radius,
			0, Math.PI * 2 );
		ctx.closePath();
		ctx.fill();
	}
}